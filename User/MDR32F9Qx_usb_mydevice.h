#ifndef __MDR32F9Qx_USB_MYDEVICE_H
#define __MDR32F9Qx_USB_MYDEVICE_H

#include "MDR32F9Qx_usb_device.h"

#define USB_MYDEVICE_EP_SEND             USB_EP1 // точка send (отправляющая) должна быть IN в дескрипторе
#define USB_MYDEVICE_EP_RECEIVE          USB_EP2 // точка receive (принимающая) должна быть OUT в дескрипторе

/** @defgroup USB_MyDevice_Exported_Functions USB MyDevice Exported Functions
  * @{
  */

USB_Result USB_MyDevice_Init(uint8_t* ReceiveBuffer, uint32_t DataPortionLength, FlagStatus StartReceiving);

USB_Result USB_MyDevice_SetReceiveBuffer(uint8_t* ReceiveBuffer, uint32_t DataPortionLength);
USB_Result USB_MyDevice_ReceiveStart(void);
USB_Result USB_MyDevice_ReceiveStop(void);

USB_Result USB_MyDevice_SendData(uint8_t* Buffer, uint32_t Length);

/** @defgroup USB_MyDevice_Exported_Overwritten_Device_Handlers USB MyDevice Overwritten Device Handlers
  * @{
  */

USB_Result USB_MyDevice_Reset(void);
USB_Result USB_MyDevice_Receive_Reset(void);
USB_Result USB_MyDevice_GetDescriptor(uint16_t wVALUE, uint16_t wINDEX, uint16_t wLENGTH);
USB_Result USB_MyDevice_RecieveData(uint8_t* Buffer, uint32_t Length);
USB_Result USB_MyDevice_DataSent(void);
/** @} */ /* End of group USB_MyDevice_Exported_Overwritten_Device_Handlers */

#endif /* __MDR32F9Qx_USB_MYDEVICE_H */
