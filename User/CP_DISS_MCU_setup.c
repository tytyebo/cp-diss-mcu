#include "CP_DISS_MCU_Setup.h"

//#include "MDR1986VE1T.h"

//#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "MDR32F9Qx_usb_default_handlers.h"
#include "MDR32F9Qx_usb_handlers.h"
#include "MDR32F9Qx_usb.h"
#include "MDR32F9Qx_usb_device.h"
#include "MDR32F9Qx_usb_mydevice.h"

USB_Clock_TypeDef USB_Clock_InitStruct;
USB_DeviceBUSParam_TypeDef USB_DeviceBUSParam;
PORT_InitTypeDef PORT_InitStructure;

USB_RxBuffer_TypeDef USB_RxBuffer;
USB_TxBuffer_TypeDef USB_TxBuffer;

void MCU_Setup(void)
{
    MCU_HSE_Setup();
    MCU_USB_Setup();
    MCU_Port_Setup();
    MCU_ADC_Clock_Setup();
    NVIC_SetPriority(ADC_IRQn, 0);
    PORT_SetBits(MDR_PORTD, PORT_Pin_11);
}

void MCU_HSE_Setup(void)
{
    RST_CLK_DeInit();
    
    RST_CLK_HSEconfig(RST_CLK_HSE_Bypass);
    while (RST_CLK_HSEstatus() != SUCCESS) {};
    RST_CLK_CPU_PLLconfig(RST_CLK_CPU_PLLsrcHSEdiv1,RST_CLK_CPU_PLLmul10);//CPU_C1_SEL[1:0] and PLL multiplication factor
    RST_CLK_CPU_PLLcmd(ENABLE);	
    RST_CLK_CPUclkPrescaler(RST_CLK_CPUclkDIV1);//CPU_C3_SEL[3:0]
    RST_CLK_CPU_PLLuse(ENABLE);//CPU_C2_SEL[1:0]??
    RST_CLK_CPUclkSelection(RST_CLK_CPUclkCPU_C3);//Final MUX for CLK		
    RST_CLK_PCLKcmd((RST_CLK_PCLK_RST_CLK),ENABLE);
}
void MCU_USB_Setup(void) {
    RST_CLK_PCLKcmd(RST_CLK_PCLK_USB, ENABLE);

    USB_Clock_InitStruct.USB_USBC1_Source = USB_C1HSEdiv1;
    USB_Clock_InitStruct.USB_PLLUSBMUL = USB_PLLUSBMUL4;
   
    USB_DeviceBUSParam.MODE  = USB_SC_SCFSP_Full;
    USB_DeviceBUSParam.SPEED = USB_SC_SCFSR_12Mb;
    USB_DeviceBUSParam.PULL  = USB_HSCR_DP_PULLUP_Set;
   
    USB_DeviceInit(&USB_Clock_InitStruct, &USB_DeviceBUSParam);
   
    USB_SetSIM(0x17);
    USB_DevicePowerOn();
    NVIC_EnableIRQ(USB_IRQn);
   
    USB_MyDevice_Init((uint8_t *) &USB_RxBuffer, 2, SET);
    USB_DEVICE_HANDLE_RESET;
   
    USB_RxBuffer_Reset(&USB_RxBuffer);
}
void MCU_ADC_Clock_Setup(void) {
    RST_CLK_PCLKcmd(RST_CLK_PCLK_ADC, ENABLE);
    RST_CLK_ADCclkSelection(RST_CLK_ADCclkCPU_C1);
    RST_CLK_ADCclkPrescaler(RST_CLK_ADCclkDIV2);
    RST_CLK_ADCclkEnable(ENABLE);
}
void MCU_Port_Setup(void) {
    PORT_StructInit(&PORT_InitStructure);
    //Setup PORTA
    //Signals PA0, PA1 and PA2 is MODE signals. Those signals connected to GND net. PA2 dont have second function
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTA, ENABLE);
    PORT_DeInit(MDR_PORTA);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_0  |      //Signal A0 (First multiplexer's channel selection signal)
                                     PORT_Pin_1  |      //Signal A1 (Second multiplexer's channel selection signal)
                                     PORT_Pin_3  |      //Discret signal to MPR (CTRL2)
                                     PORT_Pin_4  |      //Discret signal to MPR (CTRL3)
                                     PORT_Pin_5  |      //Led's "USB" control
                                     PORT_Pin_12 |      //Led's "PC" control
                                     PORT_Pin_13 |      //Led's "FAIL" control
                                     PORT_Pin_14 |      //Led's "MEMORY" control
                                     PORT_Pin_15);      //Led's "SEA" control
    PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTA, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_6  |      //Input signal EN_HFS
                                     PORT_Pin_7  |      //Input signal EN_TEST
                                     PORT_Pin_8  |      //Input signal BEAM0
                                     PORT_Pin_9  |      //Input signal BEAM1
                                     PORT_Pin_11);      //State of wafer switch "AUTO/MANUAL"
    PORT_InitStructure.PORT_OE    = PORT_OE_IN;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTA, &PORT_InitStructure);
    //Setup PORTB
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTB, ENABLE);
    PORT_DeInit(MDR_PORTB);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_0  |      // Input ARINC (IN1+)
                                     PORT_Pin_1  |      // Input ARINC (IN1-)
                                     PORT_Pin_2  |      // Input ARINC (IN2+)
                                     PORT_Pin_3);       // Input ARINC (IN2-)
    PORT_InitStructure.PORT_FUNC = PORT_FUNC_ALTER;
    PORT_InitStructure.PORT_MODE    = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED   = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTB, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_4  |      // Signak from switch "CONTROL"
                                     PORT_Pin_5  |      // Signak from switch "OUTPUT RATE"
                                     PORT_Pin_6  |      // Signak from switch "AXIS Y"
                                     PORT_Pin_7  |      // Signak from switch "+27V LEFT"
                                     PORT_Pin_8  |      // Signak from switch "+27V RIGHT"
                                     PORT_Pin_9  |      // Signak from switch "TEST MODE"
                                     PORT_Pin_10 |      // Signak from switch "RESERVE 1"
                                     PORT_Pin_11 |      // Signak from switch "RESERVE 2"
                                     PORT_Pin_12 |      // Signak from switch "ECB" (extended control ban)
                                     PORT_Pin_13 |      // Signak from switch "SILENCE"
                                     PORT_Pin_14);      // Signak from switch "SEA"
    PORT_InitStructure.PORT_OE    = PORT_OE_IN;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTB, &PORT_InitStructure);
    //Setup PORTC
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTC, ENABLE);
    PORT_DeInit(MDR_PORTC);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_0  |      //Power Good signal 1
                                     PORT_Pin_1  |      //Power Good signal 2
                                     PORT_Pin_2  |      //Power Good signal 3
                                     PORT_Pin_3  |      //Power Good signal 4
                                     PORT_Pin_5  |      //Signal from button "DISS RESRT"
                                     PORT_Pin_6  |      //Signal from switch "DISS"
                                     PORT_Pin_7  |      //Sinhro signal for beams (DISS cabel)
                                     PORT_Pin_8);       //Sinhro signal for beams (SHFS cabel)
    PORT_InitStructure.PORT_OE    = PORT_OE_IN;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTC, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_9  |      //Led's "SILENCE" control
                                     PORT_Pin_10 |      //Led's "RESERVE 1" control
                                     PORT_Pin_11 |      //Led's "RESERVE 2" control
                                     PORT_Pin_12 |      //Led's "ETHERNET" control
                                     PORT_Pin_14 |      //Led's "ARINC RX" control
                                     PORT_Pin_15);      //Led's "ARINC TX" control
    PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTC, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = PORT_Pin_13;        //UART_TX
    PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_OVERRID;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTC, &PORT_InitStructure);
    //Setup PORTD
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTD, ENABLE);
    PORT_DeInit(MDR_PORTD);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_2  |      //Signal SLP2 for second TX channel ARINC
                                     PORT_Pin_4  |      //Signal WD_STR 
                                     PORT_Pin_5  |      //27V 1 control
                                     PORT_Pin_6  |      //27V 2 control
                                     PORT_Pin_11 |      //Reset signal for MPR 450
                                     PORT_Pin_12);      //Signal SLP1 for first TX channel ARINC
    PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTD, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_3);       //Indication of signal WD_PG
    PORT_InitStructure.PORT_OE    = PORT_OE_IN;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTD, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_13 |      //Output ARINC (OUT1+)
                                     PORT_Pin_14);      //Output ARINC (OUT1-)
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_ALTER;
    PORT_Init(MDR_PORTD, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_15);      //Output ARINC (OUT3+)
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_MAIN;
    PORT_Init(MDR_PORTD, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_7  |      //ADC0_REF+
                                     PORT_Pin_8  |      //ADC1_REF-
                                     PORT_Pin_9  |      //ADC channel 2
                                     PORT_Pin_10);      //ADC channel 3
    PORT_InitStructure.PORT_OE = PORT_OE_IN;
    PORT_InitStructure.PORT_MODE = PORT_MODE_ANALOG;
    PORT_Init(MDR_PORTD, &PORT_InitStructure);
    //Setup PORTE
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTE, ENABLE);
    PORT_DeInit(MDR_PORTE);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_0  |      //Indication of input RK 1
                                     PORT_Pin_2  |      //Indication of input RK 2
                                     PORT_Pin_3  |      //Indication of input RK 3
                                     PORT_Pin_4  |      //Indication of input RK 4
                                     PORT_Pin_5  |      //Indication of input RK 5
                                     PORT_Pin_6  |      //Indication of input RK 6
                                     PORT_Pin_7  |      //Indication of input RK 7
                                     PORT_Pin_8  |      //Indication of input RK 8
                                     PORT_Pin_9  |      //Indication of input RK 9
                                     PORT_Pin_10 |      //Indication of output RK 1
                                     PORT_Pin_11 |      //Indication of output RK 2
                                     PORT_Pin_12 |      //Indication of output RK 3
                                     PORT_Pin_13 |      //Indication of output RK 4
                                     PORT_Pin_14 );     //Indication 27V/15V
    PORT_InitStructure.PORT_OE    = PORT_OE_IN;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTE, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_15);      //Setup signals UPR_REF_1_2
    PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTE, &PORT_InitStructure);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_1);       //Output ARINC (OUT3-)
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_MAIN;
    PORT_Init(MDR_PORTE, &PORT_InitStructure);
    //Setup PORTF
    RST_CLK_PCLKcmd(RST_CLK_PCLK_PORTF, ENABLE);
    PORT_DeInit(MDR_PORTF);
    PORT_InitStructure.PORT_Pin   = (PORT_Pin_1  |      //Control of input RK 1
                                     PORT_Pin_2  |      //Control of input RK 2
                                     PORT_Pin_3  |      //Control of input RK 3
                                     PORT_Pin_4  |      //Control of input RK 4
                                     PORT_Pin_5  |      //Control of input RK 5
                                     PORT_Pin_6  |      //Control of input RK 6
                                     PORT_Pin_7  |      //Control of input RK 7
                                     PORT_Pin_8  |      //Control of input RK 8
                                     PORT_Pin_9  |      //Control of input RK 9
                                     PORT_Pin_10 |      //Control of output RK 1
                                     PORT_Pin_11 |      //Control of output RK 2
                                     PORT_Pin_12 |      //Control of output RK 3
                                     PORT_Pin_13 |      //Control of output RK 4
                                     PORT_Pin_14 |      //Control of first tipe RK
                                     PORT_Pin_15);      //Control of 5V/15V
    PORT_InitStructure.PORT_OE    = PORT_OE_OUT;
    PORT_InitStructure.PORT_FUNC  = PORT_FUNC_PORT;
    PORT_InitStructure.PORT_MODE  = PORT_MODE_DIGITAL;
    PORT_InitStructure.PORT_SPEED = PORT_SPEED_MAXFAST;
    PORT_Init(MDR_PORTF, &PORT_InitStructure);    
}

void USB_RxBuffer_Reset(USB_RxBuffer_TypeDef* USB_RxBuffer)
{
   USB_RxBuffer->Command = 0;
   USB_RxBuffer->ChkSum = 0;
   USB_RxBuffer->DataLen = 0;
   USB_RxBuffer->Filled = 0;
}

USB_Result USB_MyDevice_RecieveData(uint8_t* Buffer, uint32_t Length)
{ 
    USB_RxBuffer.DataLen = Length - 2;
    USB_RxBuffer.ChkSum = Buffer[Length - 1];
    if (USB_ChkSum((uint8_t *) &USB_RxBuffer, Length) == 0x00)
        USB_RxBuffer.Valid = 1;
    else
        USB_RxBuffer.Valid = 0;
    USB_RxBuffer.Filled = 1;
    return USB_SUCCESS;
}

USB_Result USB_SendTxBuffer(USB_TxBuffer_TypeDef* TxBuffer)
{
   uint32_t length = 2 + USB_TxBuffer.DataLen;
   if (USB_TxBuffer.Filled == 1)
   {
      USB_TxBuffer.ChkSum = USB_ChkSum((uint8_t *) &USB_TxBuffer, length);
      ((uint8_t *) TxBuffer)[length] = USB_TxBuffer.ChkSum;
         
      USB_MyDevice_SendData((uint8_t *) TxBuffer, length + 1);
      USB_TxBuffer.Filled = 0;
      return USB_SUCCESS;
   }
   else
      return USB_ERROR;
}

uint8_t USB_ChkSum(uint8_t* Data, uint32_t Length)
{
   uint32_t i;
   uint8_t check = 0;
   for (i = 0; i < Length; i++)
      {
         check = check + Data[i];
      }
   return ~check;
}

void MCU_USB_UpdateConnection(void)
{
   if ((USB_GetSLS() & 0x3) == 0)
      {
         NVIC_DisableIRQ(USB_IRQn);
         USB_DeviceInit(&USB_Clock_InitStruct, &USB_DeviceBUSParam);
         USB_SetSIM(0x17);
         USB_DevicePowerOn();
         NVIC_EnableIRQ(USB_IRQn);
         USB_DEVICE_HANDLE_RESET;
      }
}
void USB_memcpy(uint8_t* Source, uint8_t* Destination, uint32_t Length) //Требует оптимизации под 32 разрядные слова
{
   //Размер массива Destination должен быть больше или равен размеру массива Source
   uint32_t i;/*, len = Length / 4;*/
   //uint32_t* sour = (uint32_t*)Source;
   //uint32_t* dest = (uint32_t*)Destination;
   
   
   for (i = 0; i < Length; i++)
      {
         *(Destination++) = *(Source++);
      }
}
void USB_LongCmdEnable(void)
{
	NVIC_DisableIRQ(USB_IRQn);
	USB_MyDevice_SetReceiveBuffer((uint8_t *) &USB_RxBuffer, 1026);
	USB_MyDevice_Receive_Reset();
	NVIC_EnableIRQ(USB_IRQn);
}
void USB_ShortCmdEnable(void)
{
	NVIC_DisableIRQ(USB_IRQn);
	USB_MyDevice_SetReceiveBuffer((uint8_t *) &USB_RxBuffer, 2);
	USB_MyDevice_Receive_Reset();
	NVIC_EnableIRQ(USB_IRQn);
}
void USB_ForceShortCmdEnable(void)
{
	NVIC_DisableIRQ(USB_IRQn);
	USB_SetSEPxRXFC(USB_MYDEVICE_EP_RECEIVE, 1);
	USB_SetSEPxTXFDC(USB_MYDEVICE_EP_SEND, 1);
	USB_MyDevice_SetReceiveBuffer((uint8_t *) &USB_RxBuffer, 2);
	USB_MyDevice_Receive_Reset();
	NVIC_EnableIRQ(USB_IRQn);
}
USB_Result USB_MyDevice_DataSent(void)
{
	USB_TxBuffer.Filled = 0;
	return USB_SUCCESS;
}

