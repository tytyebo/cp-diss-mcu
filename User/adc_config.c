#include "adc_config.h"
uint16_t adc_mass[1024];
uint16_t adc_count;
uint8_t irq_adc_count;

void ADC_Config(uint32_t chanel)
{
    ADC_InitTypeDef sADC;
    ADCx_InitTypeDef sADCx;

    //ADC Configuration
    //Reset all ADC settings
    //ADC_DeInit();
    //ADC1_Cmd(DISABLE);
    sADC.ADC_StartDelay = 0; // задержка включения АЦП от начала работы = 0
    sADC.ADC_IntVRefTrimming = 0; //подстройка опорного напряжения
    ADC_Init (&sADC); //инициализировать АЦП с заданными параметрами
    //ADC Configuration
    ADCx_StructInit (&sADCx);
    sADCx.ADC_ClockSource      = ADC_CLOCK_SOURCE_ADC;
    sADCx.ADC_SamplingMode     = ADC_SAMPLING_MODE_CICLIC_CONV;
    sADCx.ADC_ChannelSwitching = ADC_CH_SWITCHING_Disable;
    sADCx.ADC_ChannelNumber    = chanel;
    sADCx.ADC_Channels         = 0;
    sADCx.ADC_LevelControl     = ADC_LEVEL_CONTROL_Disable;
    sADCx.ADC_LowLevel         = 0;
    sADCx.ADC_HighLevel        = 0;
    sADCx.ADC_VRefSource       = ADC_VREF_SOURCE_EXTERNAL;              //Внешний канал ~ 2,048, Внутренний ~ 3,3
    sADCx.ADC_IntVRefSource    = ADC_INT_VREF_SOURCE_INEXACT;           //Исследлвать влияние на точность
    sADCx.ADC_Prescaler        = ADC_CLK_div_None;
    sADCx.ADC_DelayGo          = 0x00;
    ADC1_Init (&sADCx);
    ADC1_Cmd(ENABLE);
    
    ADC1_ITConfig(ADCx_IT_END_OF_CONVERSION, ENABLE);
    NVIC_EnableIRQ(ADC_IRQn);
}
void ADC_Start(uint32_t chanel)
{
    adc_count = 0;
    ADC_Config(chanel);
    ADC1_Start();
}
void ADC_IRQHandler(void)
{
    adc_mass[adc_count] = ADC1_GetResult();
    adc_count++;
    if(adc_count >= 1024) {
        ADC1_Cmd(DISABLE);
        irq_adc_count++;
    }
}
