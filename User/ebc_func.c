#include "ebc_func.h"
uint8_t ext_int1 = 0;
extern uint8_t *udpData;	// адрес датаграммы UDP
extern uint16_t udpDataLen;	// длина датаграммы UDP
extern uint8_t status[];	// статус-сообщение (сейчас используется только младший бит - 
													//		состояние заполненности SRAM)
uint16_t rd_result[22];	// сюда считываем результат обработки (4 шт по 4 16-битных слова)
uint16_t sram_data[2048];
volatile int packet_num = 0;
uint8_t sram_complete;
uint32_t time_FPGA_CNT;
DataTimeStructTypeDef	time_FPGA;
uint8_t fl_telemetry_mode = 0;
extern uint16_t fpga_control_reg;
uint16_t fpga_control_reg_last;


void ExternalBusFPGAConfig(void)	// настройки внешней FPGA
{
	EBC_InitTypeDef EBC_InitStruct;
	EBC_MemRegionInitTypeDef EBC_MemRegionInitStruct;
	/* Enables the HSI clock for EBC control */
  RST_CLK_PCLKcmd(RST_CLK_PCLK_EBC,ENABLE);

  /* Initializes EBC_InitStruct by default values*/
  EBC_StructInit(&EBC_InitStruct);

  /* Specifies external bus mode RAM*/
  EBC_InitStruct.EBC_Mode = EBC_MODE_RAM;
	EBC_InitStruct.EBC_DataAlignment = EBC_EBC_DATA_ALIGNMENT_16;		// for PLIS
  /* Initializes the EBC peripheral registers */
  EBC_Init(&EBC_InitStruct);

  /* Inittializes the EBC memory region 0х50000000-0х5FFFFFFF */
  /* Struct init */
  EBC_MemRegionStructInit(&EBC_MemRegionInitStruct);
	EBC_MemRegionInitStruct.WS_Active	= 0x4;		// подобранны экспереминтально
	EBC_MemRegionInitStruct.WS_Hold		= 0x2;		// подобранны экспереминтально
	EBC_MemRegionInitStruct.WS_Setup	= 0x2;		// подобранны экспереминтально
  /* Initializes the EBC memory region  */
  EBC_MemRegionInit (&EBC_MemRegionInitStruct, EBC_MEM_REGION_50000000);
  /* Enables the specified EBC memory region settigs. */
  EBC_MemRegionCMD(EBC_MEM_REGION_50000000, ENABLE);
}

void ExternalBusSRAMConfig(void)	// настройки внешней SRAM
{
	EBC_InitTypeDef EBC_InitStruct;
	EBC_MemRegionInitTypeDef EBC_MemRegionInitStruct;
	/* Enables the HSI clock for EBC control */
  RST_CLK_PCLKcmd(RST_CLK_PCLK_EBC,ENABLE);

  /* Initializes EBC_InitStruct by default values*/
  EBC_StructInit(&EBC_InitStruct);

  /* Specifies external bus mode RAM*/
  EBC_InitStruct.EBC_Mode = EBC_MODE_RAM;
	EBC_InitStruct.EBC_DataAlignment = EBC_EBC_DATA_ALIGNMENT_8;	// for SRAM
  /* Initializes the EBC peripheral registers */
  EBC_Init(&EBC_InitStruct);

  /* Inittializes the EBC memory region 0х60000000-0х6FFFFFFF */
  /* Struct init */
  EBC_MemRegionStructInit(&EBC_MemRegionInitStruct);
	EBC_MemRegionInitStruct.WS_Active	= 0x4;		// подобранны экспереминтально
	EBC_MemRegionInitStruct.WS_Hold		= 0x2;		// подобранны экспереминтально
	EBC_MemRegionInitStruct.WS_Setup	= 0x2;		// подобранны экспереминтально
  /* Initializes the EBC memory region  */
  EBC_MemRegionInit (&EBC_MemRegionInitStruct, EBC_MEM_REGION_60000000);
  /* Enables the specified EBC memory region settigs. */
  EBC_MemRegionCMD(EBC_MEM_REGION_60000000, ENABLE);
}

void EXT_INT1_IRQHandler()	// прерывание от плис
{
	int i = 0;
	volatile int j;
	time_FPGA_CNT = MDR_TIMER2->CNT;
	time_FPGA = DataTime;
	ext_int1++;
	NVIC_ClearPendingIRQ(EXT_INT1_IRQn);
	NVIC_DisableIRQ(EXT_INT1_IRQn);	// выключение прерываний
	NVIC_ClearPendingIRQ(EXT_INT1_IRQn);
//	if(fl_telemetry_mode)
//	{
//		// ответ телеметрии
//		PortsConfig(); 						// Конфигурируем порты для захвата внешней шины Микроконтроллером
//		ExternalBusFPGAConfig();
//		for(i = 0; i < 1024; i++)
//		{
//			cpu_wr_reg(0x10, (uint16_t)i);
//			sram_data[i*2] = cpu_rd_reg(0x1E*2);
//			sram_data[i*2+1] = cpu_rd_reg(0x1D*2);
//		}
//		udpData = (uint8_t *) &sram_data[0];
//		udpDataLen = 1024;
//		udp_reply(udpData, udpDataLen);
//		DoNetworkStuff(MDR_ETHERNET1);
//		udpData = (uint8_t *) &sram_data[512];
//		udpDataLen = 1024;
//		udp_reply(udpData, udpDataLen);
//		DoNetworkStuff(MDR_ETHERNET1);
//		//for(j = 0; j < 50000;j++) {}
//		udpData = (uint8_t *) &sram_data[1024];
//		udpDataLen = 1024;
//		udp_reply(udpData, udpDataLen);
//		DoNetworkStuff(MDR_ETHERNET1);
//		udpData = (uint8_t *) &sram_data[1536];
//		udpDataLen = 1024;
//		udp_reply(udpData, udpDataLen);
//		DoNetworkStuff(MDR_ETHERNET1);	
//		//for(j = 0; j < 50000;j++) {}
//		tech_mode_read_result();
//		udp_reply(udpData, udpDataLen);
//		DoNetworkStuff(MDR_ETHERNET1);
////!!! TODO : 
//		// Сюда добавить отправку телеметрии от МГЕРА
//		udpData = (uint8_t *) &tel_array;
//		udpDataLen = sizeof(tel_array);
//		udp_reply(udpData, udpDataLen);
//		DoNetworkStuff(MDR_ETHERNET1);
//	}

}

// Проверка работы SRAM. 0 - успех! либо адрес ошибки
uint32_t TestSRAM()
{
	uint32_t*	ptr;
	uint32_t read;
	uint32_t address = ADDRESS_SRAM_START;
	uint32_t res = 0;
	// Запись
	uint32_t	data = 1;
	for(ptr=(uint32_t*)address;(uint32_t)ptr < ADDRESS_SRAM_END; ptr++)
	{
		if(data == 0)
			data = 1;
		*ptr = data;
		data = data << 1;
	}
	// Чтение
	data = 1;
	for(ptr=(uint32_t*)address; (uint32_t)ptr < ADDRESS_SRAM_END; ptr++)
	{
		if(data == 0)
			data = 1;
		read = *ptr;
		if(data != read)
			return (uint32_t)ptr;
		data = data << 1;
	}
	return res;
}

//***************************************************************************
//************************ Работа с ПЛИС ************************************
//---------------------------------------------------------------------------
// --- Функция записи в регистр ПЛИС по адресу
void cpu_wr_reg(uint16_t addr, uint16_t data)
{
	uint16_t*	ptr = (uint16_t*)(0x50000000 | addr);
	*ptr = data;
}

//---------------------------------------------------------------------------
// --- Функция чтения регистра ПЛИС по адресу
uint16_t cpu_rd_reg(uint16_t addr)
{
	uint16_t*	ptr = (uint16_t*)(0x50000000 | addr);
	uint16_t read = 0;
	read = *ptr;
	return read;
}

//---------------------------------------------------------------------------
// --- Опрос (поллинг) бита готовности результата 
// 		 цифровой обработки
//uint16_t t = 0;
uint16_t cpu_rd_result_done()
{
	uint16_t result_done = 0;
	uint16_t read = 0;
//	t = 0;
	while(result_done == 0) // polling result_done_bit
	//while(t < 20)
	{
		read = cpu_rd_reg(0x3*2);
		result_done = (read >> 1) & 0x1;
//		t++;
	}
	return read;
}

//---------------------------------------------------------------------------
// --- Инициализация АЦП
// REFDAC1, REFDAC2 - параметры чувствительности ЦАПов в каналах АЦП (1..1023)
// 1 - высокая чувствительность; 1023 - низкая
void cpu_wr_init_adc(uint16_t REFDAC1, uint16_t REFDAC2)
{
	int i = 0;
	PortsConfig(); // Конфигурируем порты для захвата внешней шины Микроконтроллером
	ExternalBusFPGAConfig(); // Конфигурируем внешнюю шину для обращения МК к ПЛИС (memory mapped)
	
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x0);	
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x2);			 	// Сброс модуля ADS8363_CTRL
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x8|0x2);		// Включить режим выходного сигнала, генерирующегося в ПЛИС (на АЦП - 25 кГц)
	// Запись команд инициализации АЦП в модуль INIT_ADC (согласно даташиту на ADS8363)
	// 1-й канал
	cpu_wr_reg(FPGA_INIT_ADC_CMD0_ADDR,0x1012);		// cmd0
	cpu_wr_reg(FPGA_INIT_ADC_CMD1_ADDR,REFDAC1);	// cmd1
	cpu_wr_reg(FPGA_INIT_ADC_CMD2_ADDR,0x0);		// cmd2
	cpu_wr_reg(FPGA_INIT_ADC_CMD3_ADDR,0x0);		// cmd3
	for(i = 0; i < 20; i++)
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x4|0x2);		// Сброс модуля INIT_ADC, включить режим выходного сигнала, 
															// генерирующегося на входе АЦП
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x0|0x2);
	// 2-й канал
	cpu_wr_reg(FPGA_INIT_ADC_CMD0_ADDR,0x1015);		// cmd0
	cpu_wr_reg(FPGA_INIT_ADC_CMD1_ADDR,REFDAC2);		// cmd1
	cpu_wr_reg(FPGA_INIT_ADC_CMD2_ADDR,0x0);		// cmd2
	cpu_wr_reg(FPGA_INIT_ADC_CMD2_ADDR,0x0);		// cmd3
	
	for(i = 0; i < 20; i++)
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x4|0x2);		// Сброс модуля INIT_ADC, включить режим выходного сигнала, 
															// генерирующегося на входе АЦП
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR,0x0|0x2);
}

//-------------------------------------------------------------------
// Старт вычитывания выхода АЦП в технологическом режиме
void tech_mode_start()
{
	//uint16_t fpga_control_reg;
	PortsConfig(); 	// Конфигурируем порты для захвата внешней шины Микроконтроллером
	ExternalBusFPGAConfig();	// Конфигурируем внешнюю шину для обращения МК к ПЛИС (memory mapped)
	//	--	3) FPGA_ChangeSourseForADC - тестовый сигнал генерируется в ПЛИС (на АЦП - 25 kHz)
	//				 FPGA_ADC_nPRST - сигнал reset_n контроллера АЦП
	fpga_control_reg = FPGA_ChangeSourseForADC | FPGA_ADC_nPRST | FPGA_BEAM_en | FPGA_EN_TEST | FPGA_EN_HFS;
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
	//cpu_wr_reg(0x1*2,0x8|0x2|0x0);
	//	--	4) FPGA_TestModeON - По переднему фронту этого сигнала активируется технологический режим
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg | FPGA_TestModeON);
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
	PORT_ResetBits(MDR_PORTD,PORT_Pin_7); //	Сообщаем ПЛИС, что внешняя шина пока занята
	PortsConfig_forFPGA();								//	Конфигурируем порты для захвата внешней шины со стороны ПЛИС
	PORT_SetBits(MDR_PORTD,PORT_Pin_7); 	//	Сообщаем ПЛИС, что внешняя шина для нее свободна
	tech_mode_read_status(); 							//  Читаем статус заполненности SRAM с отправлением ответа на ПК
}
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Чтение статуса заполненности SRAM с отправлением ответа на ПК
void tech_mode_read_status()
{
	uint8_t FM_1 = 0;	
	FM_1 = PORT_ReadInputDataBit(MDR_PORTD, PORT_Pin_4); 	// читаем пин FM[1]
	status[0] = FM_1;																			// отсылаем его состояние компу
	udpData = (uint8_t *) status; 
	udpDataLen = 4;
}
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Итерация чтения SRAM (по порциям 512 байт) 
//		с параметром cntSRAMPacket - номер порции
void tech_mode_read_memory(uint32_t cntSRAMPacket)
{
	uint32_t corr_addr; // адрес памяти в соответствии с физическими линиями ({SA[27:19],SA[9:0]})
	uint32_t * ttt;
	corr_addr = ((cntSRAMPacket & 0x1) << 9) | ((cntSRAMPacket & 0xfffe) << 18); // !!!
	ttt = (uint32_t*)(0x60000000 + corr_addr); // устанавливаем указатель на 0-й байт SRAM
	PORT_ResetBits(MDR_PORTD,PORT_Pin_7);	// Сообщаем ПЛИС, что внешняя шина занята
	
	/*PORT_ResetBits(MDR_PORTD,PORT_Pin_9); 	// debug
	PORT_SetBits(MDR_PORTD,PORT_Pin_9);
	PORT_ResetBits(MDR_PORTD,PORT_Pin_9);*/
	
	PortsConfig(); 	// Конфигурируем порты для захвата внешней шины Микроконтроллером
	ExternalBusSRAMConfig();	// Конфигурируем внешнюю шину для обращения МК к SRAM (memory mapped)
	udpData = (uint8_t *) ttt; 	// Готовим пакет для отправки (указатель на датаграмму)
	udpDataLen = 512;	// Готовим пакет для отправки (количество байт датаграммы)
}
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Ответ компу с результатами вычислений
void tech_mode_read_result()
{
	uint8_t i;
	uint16_t base_addr;
	PortsConfig(); 						// Конфигурируем порты для захвата внешней шины Микроконтроллером
	ExternalBusFPGAConfig(); // Конфигурируем внешнюю шину для обращения МК к ПЛИС (memory mapped)
	base_addr = 0x12; // базовый адрес результатов вычислений в ПЛИС
	
	for(i = 0; i < 4; i++) // перебор регистров с результатами в соответствии с номерами лучей
	{
		rd_result[4*i + 0] = cpu_rd_reg(base_addr + 8*i + 0);	// старшие разряды числителя
		rd_result[4*i + 1] = cpu_rd_reg(base_addr + 8*i + 2); // младшие разряды числителя
		rd_result[4*i + 2] = cpu_rd_reg(base_addr + 8*i + 4); // старшие разряды знаменателя
		rd_result[4*i + 3] = cpu_rd_reg(base_addr + 8*i + 6); // младшие разряды знаменателя
	}
	rd_result[16] = cpu_rd_reg(0x19*2);
	rd_result[17] = cpu_rd_reg(0x1A*2);
	rd_result[18] = cpu_rd_reg(0x1B*2);
	rd_result[19] = cpu_rd_reg(0x1C*2);
	rd_result[20] = cpu_rd_reg(0x6);
	rd_result[21] = cpu_rd_reg(0x2);
/*	
	rd_result[0] = cpu_rd_reg(FPGA_DSP_CHISL_H);	// старшие разряды числителя
	rd_result[1] = cpu_rd_reg(FPGA_DSP_CHISL_L);	// младшие разряды числителя
	rd_result[2] = cpu_rd_reg(FPGA_DSP_ZNAM_H);	// старшие разряды знаменателя
	rd_result[3] = cpu_rd_reg(FPGA_DSP_ZNAM_L); // младшие разряды знаменателя
*/
	// CHEAT $$$ : Если мы не в режиме телеметрии, то порты конфигурируем
	// TODO : Когда перейдем на общий режим телеметрии, это вообще нужно убрать
	if(fl_telemetry_mode == 0)
	{
		PortsConfig_forFPGA();								//	Переконфигурируем порты для доступа ПЛИС к SRAM
		PORT_SetBits(MDR_PORTD,PORT_Pin_7); 	//	Сообщаем об этом ПЛИС, выставляя сигнал MF[0] в 1
	}
	
	udpData = (uint8_t *) rd_result; // Готовим пакет для отправки (указатель на датаграмму)
	udpDataLen = 44; // Готовим пакет для отправки (количество байт датаграммы)

}
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Старт вычитывания выхода CIC в технологическом режиме
void cic_mode_start(uint16_t control)
{
	uint8_t stop_4;
	uint8_t stop_1;
	//uint16_t fpga_control_reg;
	uint16_t dsp_control_reg = 0; // Регистр управления модуля ЦОС ПЛИС (DSP.v)
	PORT_ResetBits(MDR_PORTD,PORT_Pin_7); // Сообщаем ПЛИС, что внешняя шина занята
	PortsConfig(); 								// 	-- 	1) Config ports to "MCU-FPGA"
	ExternalBusFPGAConfig();			// 	-- 	2) Config EBC to "MCU-FPGA"
	fpga_control_reg = 0; // Регистр управления модуля ПЛИС топ-уровня (diss_main.v)
	//uint16_t dsp_control_reg = 0; // Регистр управления модуля ЦОС ПЛИС (DSP.v)
	stop_4 = 0;					// Флаг стопа алгоритма ЦОС после 4-го пуска
	stop_1 = 0;					// Флаг стопа алгоритма ЦОС после 1-го пуска
	// Заполняем биты регистра управления fpga_control_reg в соответствии 
	// с переменной "control", пришедшей по Ethernet
	
	// Сброс ПЛИС
	if(control & 0x1) 
	{
		fpga_control_reg |= FPGA_RESET;
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
		fpga_control_reg &= ~FPGA_RESET;
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
	}
	
	if((control >> 7) & 0x1) fpga_control_reg |= FPGA_ChangeSourseForADC;
	if((control >> 6) & 0x1) {
		fpga_control_reg |= FPGA_BEAM_en;	// бит уплавления номером луча от МК
		stop_1 = 0x1;											
	}
	else																				// Если не МК переключает лучи, 
		stop_4 = 0x1;															// то запускаем разрешение на сигнал "stop_4"
	if((control >> 5) & 0x1) fpga_control_reg |= FPGA_EN_TEST;	// Включение выхода ПЛИС EN_TEST (передатчик)
	if((control >> 4) & 0x1) fpga_control_reg |= FPGA_EN_HFS;		// Включение выхода ПЛИС EN_HFS (мощность)
	switch((control >> 2) & 0x3)	// для случая управления номером луча от МК
	{
		case 0: fpga_control_reg |= FPGA_BEAM_0; break;	
		case 1: fpga_control_reg |= FPGA_BEAM_1; break;
		case 2: fpga_control_reg |= FPGA_BEAM_2; break;
		case 3: fpga_control_reg |= FPGA_BEAM_3; break;
	}
		
	// Заполняем биты регистра управления dsp_control_reg в соответствии 
	// с переменной "control", пришедшей по Ethernet
	/*switch(control & 0x3)		// выбор номера 
	{
		case 0: dsp_control_reg |= FPGA_BEAM_0; break;
		case 1: dsp_control_reg |= FPGA_BEAM_1; break;
		case 2: dsp_control_reg |= FPGA_BEAM_2; break;
		case 3: dsp_control_reg |= FPGA_BEAM_3; break;
	}*/
	
	// Не сбрасывая АЦП, сбрасываем бит режима "CIC2SRAM" 
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg 
																		| FPGA_ADC_nPRST 	// reset_n контроллера АЦП = 1
																		);
	// Не сбрасывая АЦП, включаем режим "CIC2SRAM", активируем запись в SRAM выхода CIC
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg
																		| FPGA_ADC_nPRST	// reset_n контроллера АЦП = 1
																		| FPGA_CIC2SRAM_mode // включен режим "CIC2SRAM" 
																		| FPGA_TestModeON	// по переднему фронту этого сигнала
																		);								// 		активируется запись выхода CIC в SRAM
	// Не сбрасывая АЦП и не выключая режим "CIC2SRAM", сбрасываем FPGA_TestModeON,
	//																								(так как нужен только передний фронт)
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg
																		| FPGA_ADC_nPRST	// reset_n контроллера АЦП = 1
																		| FPGA_CIC2SRAM_mode // включен режим "CIC2SRAM" 
																		);
	//cpu_wr_reg(0x7*2,0x2);	// Команда "старт алторитма"
	cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR,FPGA_DSP_CTRLREG_RESET);	// Обнуляем регистр управления DSP
	if((control >> 8) & 0x1) dsp_control_reg |= FPGA_DSP_CTRLREG_80MS_EN;
	if(stop_4) // Если включен флаг стопа алгоритма ЦОС после 4-го пуска, стартуем с битом разрешения стопа после 4-го пуска
		dsp_control_reg |= FPGA_DSP_CTRLREG_STOP4_EN | FPGA_DSP_CTRLREG_START;
		//cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR, FPGA_DSP_CTRLREG_STOP4_EN | FPGA_DSP_CTRLREG_START);	
	else	// Иначе просто стартуем
		dsp_control_reg |= FPGA_DSP_CTRLREG_START;
		//cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR, FPGA_DSP_CTRLREG_START);
		cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR, dsp_control_reg);
	dsp_control_reg &= ~FPGA_DSP_CTRLREG_START;
	dsp_control_reg |= FPGA_DSP_CTRLREG_STOP;
	if(stop_1) // Если включен флаг стопа алгоритма ЦОС после 1-го пуска, включаем бит стопа алгоритма ЦОС
		cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR, dsp_control_reg);	// Команда "стоп алторитма"
	PORT_ResetBits(MDR_PORTD,PORT_Pin_7); //	--	5) BUS "FPGA - SRAM" is Busy
	PortsConfig_forFPGA();								//	Переконфигурируем порты для доступа ПЛИС к SRAM
	PORT_SetBits(MDR_PORTD,PORT_Pin_7); 	//	Сообщаем об этом ПЛИС, выставляя сигнал MF[0] в 1
 	tech_mode_read_status(); 							// 	Читаем статус заполненности SRAM с отправлением ответа на ПК
}
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Выход из технологического режима
void tech_mode_end()
{
	//uint16_t fpga_control_reg;
	PORT_ResetBits(MDR_PORTD,PORT_Pin_7); // Сообщаем ПЛИС, что внешняя шина занята
	PortsConfig(); 								// 	-- 	1) Config ports to "MCU-FPGA"
	ExternalBusFPGAConfig();			// 	-- 	2) Config EBC to "MCU-FPGA"
	// Сброс ПЛИС
	fpga_control_reg |= FPGA_RESET;
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
	fpga_control_reg &= ~FPGA_RESET;
	cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
	// Стоп алгоритма
	cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR, FPGA_DSP_CTRLREG_STOP);	// Команда "стоп алторитма"
	cpu_wr_reg(FPGA_DSP_CTRLREG_ADDR, FPGA_DSP_CTRLREG_RESET);	// Обнуляем регистр управления DSP
	// $ TODO :
	// нужно вернуться в исходное состояние для АЦП, EN_TEST, EN_HFS, сконфиругировать шину и тд.
	status[1] = 0xa5;	
	udpData = (uint8_t *) status; 
	udpDataLen = 4;
}
//-------------------------------------------------------------------

//-------------------------------------------------------------------
// Старт режима телеметрии
void telemetry_mode_start(uint16_t control)
{
	// test signal params
	uint16_t df = 65536-500;
	uint8_t sig = 12;
	uint8_t noise = 14;
	uint16_t noise_sig;
	noise_sig = noise << 4 | sig;
	//--
	
	fpga_control_reg_last = fpga_control_reg;
	PORT_ResetBits(MDR_PORTD,PORT_Pin_7); // Сообщаем ПЛИС, что внешняя шина занята
	PortsConfig(); 								// 	-- 	1) Конфигурация портов "MCU-FPGA"
	ExternalBusFPGAConfig();			// 	-- 	2) Конфигурация шины EBC "MCU-FPGA"
	
	fpga_control_reg &= (~FPGA_BEAM_en & ~FPGA_BEAM_3) ; // Регистр управления модуля ПЛИС топ-уровня (diss_main.v)
	// Сброс ПЛИС
	/*if(control & 0x1) 
	{
		fpga_control_reg |= FPGA_RESET;
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
		fpga_control_reg &= ~FPGA_RESET;
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg);
	}*/
	
	//fpga_control_reg |= FPGA_ChangeSourseForADC;
	if((control >> 7) & 0x1) 
	{
		fpga_control_reg |= FPGA_ChangeSourseForADC;
		fpga_control_reg |= FPGA_TestSignalMode;
		cpu_wr_reg(FPGA_TESTSIGNALPARAMS_ADDR, noise_sig);
		cpu_wr_reg(FPGA_TESTSIGNALDF_ADDR, (uint16_t)df);
	}
	if((control >> 6) & 0x1) {
		fpga_control_reg |= FPGA_BEAM_en;	// бит уплавления номером луча от МК
	}

	//if((control >> 5) & 0x1) fpga_control_reg |= FPGA_EN_TEST;	// Включение выхода ПЛИС EN_TEST (передатчик)
	//if((control >> 4) & 0x1) fpga_control_reg |= FPGA_EN_HFS;		// Включение выхода ПЛИС EN_HFS (мощность)
	switch((control >> 2) & 0x3)	// для случая управления номером луча от МК
	{
		case 0: fpga_control_reg |= FPGA_BEAM_0; break;	
		case 1: fpga_control_reg |= FPGA_BEAM_1; break;
		case 2: fpga_control_reg |= FPGA_BEAM_2; break;
		case 3: fpga_control_reg |= FPGA_BEAM_3; break;
	}
		
	fl_telemetry_mode = ((control >> 9) & 0x1);
	if(fl_telemetry_mode)
	{
		// Не сбрасывая АЦП, сбрасываем бит режима "CIC2SRAM" 
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg 
																			| FPGA_ADC_nPRST 	// reset_n контроллера АЦП = 1
																			);
	}
	else
		cpu_wr_reg(FPGA_CONTROL_REG_ADDR, fpga_control_reg_last);
	//PORT_ResetBits(MDR_PORTD,PORT_Pin_7); // Сообщаем ПЛИС, что внешняя шина занята
	//
}

//-------------------------------------------------------------------

//-------------------------------------------------------------------
/*// Чтение SRAM в режиме телеметрии
void telemetry_mode_read_memory(uint32_t cntSRAMPacket)
{

}
//-------------------------------------------------------------------*/
