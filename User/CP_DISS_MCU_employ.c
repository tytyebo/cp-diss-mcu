#include "CP_DISS_MCU_employ.h"
#include "CP_DISS_MCU_setup.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"

uint32_t BEAM_time[12], Sinh_Beam_DISS_time[24], Sinh_Beam_MPR_time[24];
uint8_t BEAM_state[12];
uint8_t bi = 0, si1 = 0, si2 = 0, old_state_DISS =0, old_state_MPR = 0, auto_flag = 0;

void Sinhro_BEAM(void);

BitAction powerGood = Bit_RESET;

//Power
void Set_UPR_5V_15V() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_15);
}
void Reset_UPR_5V_15V() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_15);
}
void Set_UPR_27V_1() {
    PORT_SetBits(MDR_PORTD, PORT_Pin_5);
}
void Reset_UPR_27V_1() {
    PORT_ResetBits(MDR_PORTD, PORT_Pin_5);
}
void Set_UPR_27V_2() {
    PORT_SetBits(MDR_PORTD, PORT_Pin_6);
}
void Reset_UPR_27V_2() {
    PORT_ResetBits(MDR_PORTD, PORT_Pin_6);
}
void Set_UPR_RK_1() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_14);                 //Включение питания Дисс
}
void Reset_UPR_RK_1() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_14);                 //Включение питания Дисс
}
void Set_WD_STR() {
    PORT_SetBits(MDR_PORTD, PORT_Pin_4);
}
void Reset_WD_STR() {
    PORT_ResetBits(MDR_PORTD, PORT_Pin_4);
}

//функция управления питанием
uint8_t Set_Pwr(uint8_t data) {
    if ((data & 0x01) == 0)
        Reset_UPR_5V_15V();
    else
        Set_UPR_5V_15V();
    if((data & 0x02) == 0)
        Reset_UPR_27V_1();
    else
        Set_UPR_27V_1();
    if((data & 0x04) == 0)
        Reset_UPR_27V_2();
    else
        Set_UPR_27V_2();
    if((data & 0x10) == 0)
        Reset_UPR_RK_1();
    else
        Set_UPR_RK_1();                 //Управление РК 1-го типа
    if((data & 0x08) == 0)
        return 0x00;
    else
        return 0x01;
}
//Фунцкия чтения сигналов управления питанием
uint8_t Get_PWR(uint8_t param) {
    return PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_15)            //UPR_5V_15V
        | (PORT_ReadInputDataBit(MDR_PORTD, PORT_Pin_5)<<1)         //UPR_27V_1
        | (PORT_ReadInputDataBit(MDR_PORTD, PORT_Pin_6)<<2)         //UPR_27V_2
        | (param << 3)                                              //WS_STR
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_14)<<4);       //UPR_RK_1
}
uint8_t Get_Reset_Button() {
    return PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_5);
}
//Функция переключения сигнала WS_STR
uint8_t Change_WS_STR(uint8_t param, uint8_t pre) {
    uint8_t post;
    //PORT_SetBits(MDR_PORTD, PORT_Pin_11);
    if (PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_5))               //if buuton reaet is pressed
        PORT_ResetBits(MDR_PORTD, PORT_Pin_11);
    else
        PORT_SetBits(MDR_PORTD, PORT_Pin_11);
    if (param) {
        if (pre)
        {
            Reset_WD_STR();
            post = 0;
        }
        else 
        {
            Set_WD_STR();
            post = 1;
        }
    }
    else
        post = pre;
    return post;        
}
uint8_t Get_PG_1_MCU() {
    return PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_0);
}
uint8_t Get_PG_2_MCU() {
    return PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_1);
}
uint8_t Get_PG_3_MCU() {
    return PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_2);
}
uint8_t Get_PG_4_MCU() {
    return PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_3);
}
uint8_t Get_WD_PG() {
    return PORT_ReadInputDataBit(MDR_PORTD, PORT_Pin_3);
}
uint8_t Get_IND_27V_15V() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_14);
}
//Функция чтения Power Goods
uint8_t Get_PG() {
        return Get_PG_1_MCU()
            | (Get_PG_2_MCU()<<1)
            | (Get_PG_3_MCU()<<2) 
            | (Get_PG_4_MCU()<<3) 
            | (Get_WD_PG()<<4) 
            | (Get_IND_27V_15V()<<5);
}
//Input RK
void Set_UPR_RK_B_1() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_10);
}
void Reset_UPR_RK_B_1() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_10);
}
void Set_UPR_RK_B_2() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_11);
}
void Reset_UPR_RK_B_2() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_11);
}
void Set_UPR_RK_B_3() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_12);
}
void Reset_UPR_RK_B_3() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_12);
}
void Set_UPR_RK_B_4() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_13);
}
void Reset_UPR_RK_B_4() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_13);
}
uint8_t Get_IND_RK_B_1() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_10);
}
uint8_t Get_IND_RK_B_2() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_11);
}
uint8_t Get_IND_RK_B_3() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_12);
}
uint8_t Get_IND_RK_B_4() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_13);
}
//Функция считывания индикаторов 4-х входных для Пульта РК
uint8_t Get_IND_4RK() {
    return Get_IND_RK_B_1()
        | (Get_IND_RK_B_2()<<1)
        | (Get_IND_RK_B_3()<<2)
        | (Get_IND_RK_B_4()<<3);
}
//Функция установки UPR_4RK
void Set_4RK(uint8_t data) {
    if ((data & 0x01) == 0)
        Reset_UPR_RK_B_1();
    else
        Set_UPR_RK_B_1();
    if((data & 0x02) == 0)
        Reset_UPR_RK_B_2();
    else
        Set_UPR_RK_B_2();
    if((data & 0x04) == 0)
        Reset_UPR_RK_B_3();
    else
        Set_UPR_RK_B_3();
    if((data & 0x08) == 0)
        Reset_UPR_RK_B_4();
    else
        Set_UPR_RK_B_4();
}
uint8_t Get_UPR_4RK() {
		return PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_10)
            | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_11)<<1)
            | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_12)<<2)
            | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_13) << 3);
}
//Output RK
void Set_UPR_RK_A_1() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_1);
}
void Reset_UPR_RK_A_1() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_1);
}
void Set_UPR_RK_A_2() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_2);
}
void Reset_UPR_RK_A_2() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_2);
}
void Set_UPR_RK_A_3() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_3);
}
void Reset_UPR_RK_A_3() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_3);
}
void Set_UPR_RK_A_4() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_4);
}
void Reset_UPR_RK_A_4() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_4);
}
void Set_UPR_RK_A_5() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_5);
}
void Reset_UPR_RK_A_5() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_5);
}
void Set_UPR_RK_A_6() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_6);
}
void Reset_UPR_RK_A_6() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_6);
}
void Set_UPR_RK_A_7() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_7);
}
void Reset_UPR_RK_A_7() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_7);
}
void Set_UPR_RK_A_8() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_8);
}
void Reset_UPR_RK_A_8() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_8);
}
void Set_UPR_RK_A_9() {
    PORT_SetBits(MDR_PORTF, PORT_Pin_9);
}
void Reset_UPR_RK_A_9() {
    PORT_ResetBits(MDR_PORTF, PORT_Pin_9);
}
uint8_t Get_IND_RK_A_1() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_0);
}
uint8_t Get_IND_RK_A_2() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_2);
}
uint8_t Get_IND_RK_A_3() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_3);
}
uint8_t Get_IND_RK_A_4() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_4);
}
uint8_t Get_IND_RK_A_5() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_5);
}
uint8_t Get_IND_RK_A_6() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_6);
}
uint8_t Get_IND_RK_A_7() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_7);
}
uint8_t Get_IND_RK_A_8() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_8);
}
uint8_t Get_IND_RK_A_9() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_9);
}
//Функция считывания индикаторов 9-х выходных для Пульта РК
uint16_t Get_IND_9RK() {
    uint16_t qq;
    qq = Get_IND_RK_A_1()
        | (Get_IND_RK_A_2()<<1)
        | (Get_IND_RK_A_3()<<2)
        | (Get_IND_RK_A_4()<<3)
        | (Get_IND_RK_A_5()<<4)
        | (Get_IND_RK_A_6()<<5)
        | (Get_IND_RK_A_7()<<6)
        | (Get_IND_RK_A_8()<<7)
        | (Get_IND_RK_A_9()<<8);
    return qq;
}
//Функция установки UPR_9RK
void Set_9RK(uint16_t data) {
    if ((data & 0x0001) == 0)
        Reset_UPR_RK_A_1();
    else
        Set_UPR_RK_A_1();
    if((data & 0x0002) == 0)
        Reset_UPR_RK_A_2();
    else
        Set_UPR_RK_A_2();
    if((data & 0x0004) == 0)
        Reset_UPR_RK_A_3();
    else
        Set_UPR_RK_A_3();
    if((data & 0x0008) == 0)
        Reset_UPR_RK_A_4();
    else
        Set_UPR_RK_A_4();
    if((data & 0x0010) == 0)
        Reset_UPR_RK_A_5();
    else
        Set_UPR_RK_A_5();
    if((data & 0x0020) == 0)
        Reset_UPR_RK_A_6();
    else
        Set_UPR_RK_A_6();
    if((data & 0x0040) == 0)
        Reset_UPR_RK_A_7();
    else
        Set_UPR_RK_A_7();
    if((data & 0x0080) == 0)
        Reset_UPR_RK_A_8();
    else
        Set_UPR_RK_A_8();
    if((data & 0x0100) == 0)
        Reset_UPR_RK_A_9();
    else
        Set_UPR_RK_A_9();
}
void Set_9RK2(uint8_t data1) {
    if ((data1 & 0x01) == 0)
        Reset_UPR_RK_A_1();
    else
        Set_UPR_RK_A_1();
    if((data1 & 0x02) == 0)
        Reset_UPR_RK_A_2();
    else
        Set_UPR_RK_A_2();
    if((data1 & 0x04) == 0)
        Reset_UPR_RK_A_3();
    else
        Set_UPR_RK_A_3();
    if((data1 & 0x08) == 0)
        Reset_UPR_RK_A_4();
    else
        Set_UPR_RK_A_4();
    if((data1 & 0x10) == 0)
        Reset_UPR_RK_A_5();
    else
        Set_UPR_RK_A_5();
    if((data1 & 0x20) == 0)
        Reset_UPR_RK_A_6();
    else
        Set_UPR_RK_A_6();
    if((data1 & 0x40) == 0)
        Reset_UPR_RK_A_7();
    else
        Set_UPR_RK_A_7();
    if((data1 & 0x80) == 0)
        Reset_UPR_RK_A_8();
    else
        Set_UPR_RK_A_8();
}
uint16_t Get_UPR_9RK() {
    return PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_1)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_2)<<1)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_3)<<2)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_4)<<3)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_5)<<4)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_6)<<5)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_7)<<6)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_8)<<7)
        | (PORT_ReadInputDataBit(MDR_PORTF, PORT_Pin_9)<<8);
}
void Set_UPR_REF_12(uint8_t data) {
    if(data)
        PORT_SetBits(MDR_PORTE, PORT_Pin_15);
    else
        PORT_ResetBits(MDR_PORTE, PORT_Pin_15);
}
uint8_t Get_UPR_REF_12() {
    return PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_15);
}
// LVTTL
void Set_CTRL2() {
    PORT_SetBits(MDR_PORTA, PORT_Pin_4);
}
void Reset_CTRL2() {
    PORT_ResetBits(MDR_PORTA, PORT_Pin_4);
}
void Set_CTRL3() {
    PORT_SetBits(MDR_PORTA, PORT_Pin_3);
}
void Reset_CTRL3() {
    PORT_ResetBits(MDR_PORTA, PORT_Pin_3);
}
void Set_3LVTTL(uint8_t data1) {
    if(auto_flag != 1){
        if((data1 & 0x02) == 0)
            Reset_CTRL2();
        else
            Set_CTRL2();
    }
    if((data1 & 0x04) == 0)
        Reset_CTRL3();
    else
        Set_CTRL3();
}
uint8_t Get_3LVTTL() {
    return (PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_4)<<1)
         | (PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_3)<<2);
}
uint8_t Get_EN_HFS() {
    return PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_6);
}
uint8_t Get_EN_TEST() {
    return PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_7);
}
uint8_t Get_EN_TTL() {
    return Get_EN_HFS() | (Get_EN_TEST()<<1);
}
void auto_ctrl(uint8_t control, uint8_t hfs) {
    if(control) {
        auto_flag = 1;
        if(hfs)
            Set_CTRL2();
        else
            Reset_CTRL2();
    }
    else
        auto_flag = 0;
}
uint8_t Get_BEAM0() {
    return PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_8);
}
uint8_t Get_BEAM1() {
    return PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_9);
}
uint8_t Monitor_BEAM(uint8_t bstate) {
    uint8_t i;
    uint8_t temp[4];
    temp[0] = (Get_BEAM0() | (Get_BEAM1() << 1));
    temp[1] = (Get_BEAM0() | (Get_BEAM1() << 1));
    temp[2] = (Get_BEAM0() | (Get_BEAM1() << 1));
    temp[3] = (Get_BEAM0() | (Get_BEAM1() << 1));
    if(bi == 12)
        bi = 0;
    for (i = 1; i < 4; i++) {
        if (temp[i - 1] == temp[i]) {
            if(bstate != temp[i]) {
                BEAM_time[bi] = MDR_TIMER2->CNT;
                BEAM_state[bi] = temp[i];
                bi++;
            }        
            break;
        }
    }
    Sinhro_BEAM();
    return temp[i];
}
void Sinhro_BEAM(void) {
    uint8_t new_state;
    new_state = PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_8);
    if(si1 == 24)
        si1 = 0;
    if(new_state != old_state_MPR) {
        Sinh_Beam_MPR_time[si1] = MDR_TIMER2->CNT;
        old_state_MPR = new_state;
        si1++;
    }
    new_state = PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_7);
    if(si2 == 24)
        si2 = 0;
    if(new_state != old_state_DISS) {
        Sinh_Beam_DISS_time[si2] = MDR_TIMER2->CNT;
        old_state_DISS = new_state;
        si2++;
    }
}
uint8_t AUTO_MANUAL_POS() {
    return PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_11);
}
//LEDs
void Set_LED(uint8_t PC_RK) {
    //Включение диодов отвественных за РК
    if(PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_10))
        PORT_SetBits(MDR_PORTA, PORT_Pin_13);
    else 
        PORT_ResetBits(MDR_PORTA, PORT_Pin_13);
    if(PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_11))
        PORT_SetBits(MDR_PORTA, PORT_Pin_14);
    else 
        PORT_ResetBits(MDR_PORTA, PORT_Pin_14);
    if(PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_12))
        PORT_SetBits(MDR_PORTA, PORT_Pin_15);
    else 
        PORT_ResetBits(MDR_PORTA, PORT_Pin_15);
    if(PORT_ReadInputDataBit(MDR_PORTE, PORT_Pin_13))
        PORT_SetBits(MDR_PORTC, PORT_Pin_9);
    else 
        PORT_ResetBits(MDR_PORTC, PORT_Pin_9);
    if (PC_RK)
        PORT_SetBits(MDR_PORTA, PORT_Pin_12);
    else
        PORT_ResetBits(MDR_PORTA, PORT_Pin_12);
}
//7 RK
void Set_UP_SW(uint8_t enable) {
    if(!enable)
    {
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_13))
            Set_UPR_RK_A_1();
        else
            Reset_UPR_RK_A_1();
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_14))
            Set_UPR_RK_A_2();
        else
            Reset_UPR_RK_A_2();
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_12))
            Set_UPR_RK_A_3();
        else
            Reset_UPR_RK_A_3();
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_6))
            Set_UPR_RK_A_4();
        else
            Reset_UPR_RK_A_4();
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_4))
            Set_UPR_RK_A_6();
        else
            Reset_UPR_RK_A_6();
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_5))
            Set_UPR_RK_A_5();
        else
            Reset_UPR_RK_A_5();
        if(PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_9))
            Set_UPR_RK_A_7();
        else
            Reset_UPR_RK_A_7();
        Set_Pwr((PORT_ReadInputDataBit(MDR_PORTC, PORT_Pin_6)<<4) | (PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_7)<<1) | (PORT_ReadInputDataBit(MDR_PORTB, PORT_Pin_8)<<2));
    }
}
void Set_Led_Eth() {
    PORT_SetBits(MDR_PORTC, PORT_Pin_12);
}
void Reset_Led_Eth() {
    PORT_ResetBits(MDR_PORTC, PORT_Pin_12);
}
void Set_Led_Ari() {
    PORT_SetBits(MDR_PORTC, PORT_Pin_14);
}
void Reset_Led_Ari() {
    PORT_ResetBits(MDR_PORTC, PORT_Pin_14);
}
void Set_Led_Aro() {
    PORT_SetBits(MDR_PORTC, PORT_Pin_15);
}
void Reset_Led_Aro() {
    PORT_ResetBits(MDR_PORTC, PORT_Pin_15);
}
void Set_Led_USB() {
    PORT_SetBits(MDR_PORTA, PORT_Pin_5);
}
void Reset_Led_USB() {
    PORT_ResetBits(MDR_PORTA, PORT_Pin_5);
}
//функция управления питанием
void Set_Mult(uint8_t data) {
    if ((data & 0x01) == 0)
        PORT_ResetBits(MDR_PORTA, PORT_Pin_0);
    else
        PORT_SetBits(MDR_PORTA, PORT_Pin_0);
    if((data & 0x02) == 0)
        PORT_ResetBits(MDR_PORTA, PORT_Pin_1);
    else
        PORT_SetBits(MDR_PORTA, PORT_Pin_1);
}
//Фунцкия чтения сигналов управления питанием
uint8_t Get_Mult() {
    return PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_0)             //A0
        | (PORT_ReadInputDataBit(MDR_PORTA, PORT_Pin_1)<<1);        //A1
}
