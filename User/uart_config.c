#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_uart.h"
#include "MDR32F9Qx_port.h"
#include "MDR32F9Qx_rst_clk.h"
#include "uart_config.h"

static UART_InitTypeDef UART_InitStructure;
uint8_t uart_mass[28];
uint8_t uart_ctrl = 0;
uint8_t uart_send_is_running = 0;

void UART_Config ( void )
{
    UART_DeInit(MDR_UART2);
    /* Enables the CPU_CLK clock on UART2 */
    RST_CLK_PCLKcmd(RST_CLK_PCLK_UART2, ENABLE);

    /* Set the HCLK division factor = 1 for UART1,UART2*/
    UART_BRGInit(MDR_UART2, UART_HCLKdiv1);

    /* Initialize UART_InitStructure */
    UART_InitStructure.UART_BaudRate = 115200;
    UART_InitStructure.UART_WordLength = UART_WordLength8b;
    UART_InitStructure.UART_StopBits = UART_StopBits2;
    UART_InitStructure.UART_Parity = UART_Parity_No;
    UART_InitStructure.UART_FIFOMode = UART_FIFO_OFF;
    UART_InitStructure.UART_HardwareFlowControl = UART_HardwareFlowControl_TXE;

    /* Configure UART2 parameters*/
    UART_Init(MDR_UART2, &UART_InitStructure);

    /* Configure DMA for UART2*/
    UART_DMAConfig(MDR_UART2, UART_IT_FIFO_LVL_12words,
            UART_IT_FIFO_LVL_12words );
    UART_DMACmd(MDR_UART2, UART_DMA_TXE | UART_DMA_ONERR, ENABLE);

    /* Enables UART2 peripheral */
    UART_Cmd(MDR_UART2, ENABLE);
}
void Uart_Send(uint8_t control, uint8_t test, uint8_t beams) {
    uint8_t i;
    if(!uart_send_is_running){
        if ((test || (control & 0x10)) && (control & 0x01)) {
            if (control & 0x04)
                uart_mass[3] = beams;
            if (control & 0x02) {
                if (control & 0x08){
                    uart_mass[26] = 0;
                    for(i = 0; i < 26; i++)
                        uart_mass[26] += uart_mass[i];
                }
                uart_send_is_running = 1;
                TIMER_Cmd(MDR_TIMER4, ENABLE);
            }
            else {
                if (control & 0x08) {
                    uart_mass[16] = 0;
                    for(i = 0; i < 16; i++)
                        uart_mass[16] += uart_mass[i];
                }
                uart_send_is_running = 1;
                TIMER_Cmd(MDR_TIMER4, ENABLE);
            }
        }
    }
}
/*void Uart_Send(uint8_t control, uint8_t test, uint8_t beams) {
    uint8_t i;
    if (test && (control & 0x01)) {
        if (control & 0x04)
            uart_mass[3] = beams;
        if (control & 0x02) {
            if (control & 0x08)
                //uart_mass[26] = 0;
            for(i = 0; i < 28; i++) {
                while (UART_GetFlagStatus(MDR_UART2, UART_FLAG_TXFE ) != SET) {
                }
                UART_SendData(MDR_UART2, uart_mass[i]);
                if(control & 0x08 && i < 26)
                    uart_mass[26] += uart_mass[i];
            }
        }
        else {
            if (control & 0x08)
                uart_mass[16] = 0;
            for(i = 0; i < 18; i++) {
                while (UART_GetFlagStatus(MDR_UART2, UART_FLAG_TXFE ) != SET) {
                }
                UART_SendData(MDR_UART2, uart_mass[i]);
                if(control & 0x08 && i < 16)
                    uart_mass[16] += uart_mass[i];
            }
        }
    }
}*/
