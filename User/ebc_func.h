#ifndef __EBC_FUNC_H__
	#define __EBC_FUNC_H__
	#include "MDR32F9Qx_config.h"           // Milandr::Device:Startup
	#include "MDR32F9Qx_rst_clk.h"          // Milandr::Drivers:RST_CLK
	#include "MDR32F9Qx_ebc.h"              // Milandr::Drivers:EBC
	#include "port_config.h"
	#include "timer_config.h"
	#include "arinc_config.h"
	
	#define ADDRESS_SRAM_START 								(0x60000000)
	#define ADDRESS_SRAM_END									(0x60080000)
	
	// FPGA REGISTERs MAP
	#define FPGA_CONTROL_REG_ADDR 						(0x2)				// Адрес общего регистра управления ПЛИС
	#define FPGA_ADC_CTRLREG_ADDR							(0x4)				// Адрес регистра управления АЦП
	#define FPGA_INIT_ADC_CMD0_ADDR						(0x6)				// Адрес 0-й команды инициализации АЦП
	#define FPGA_INIT_ADC_CMD1_ADDR						(0x8)				// Адрес 1-й команды инициализации АЦП
	#define FPGA_INIT_ADC_CMD2_ADDR						(0xA)				// Адрес 2-й команды инициализации АЦП
	#define FPGA_INIT_ADC_CMD3_ADDR						(0xC)				// Адрес 3-й команды инициализации АЦП
	#define FPGA_DSP_CTRLREG_ADDR							(0xE)				// Адрес регистра управления схемой ЦОС (DSP.v)
	#define FPGA_TEL_RDADDRESS_ADDR						(0x10)				//
	#define FPGA_THRES_IN_ADDR								(0x12)				//
	#define FPGA_TESTSIGNALPARAMS_ADDR				(0x14)				//
	#define FPGA_TESTSIGNALDF_ADDR						(0x16)				//
	
	// 	FPGA_CONTROL_REG bits
	#define FPGA_TestModeON 									(0x1)				// Включение технологического режима (используется передний фронт)
	#define FPGA_ADC_nPRST 										(0x2)				// сигнал reset_n контроллера АЦП (когда используем АЦП, всегда держим включенным)
	#define FPGA_INIT_ADC_nPRST 							(0x4)				// Включение инициализации АЦП (используется передний фронт)
	#define FPGA_ChangeSourseForADC						(0x8)				// Если выставлен этот бит, на вход схемы ЦОС - последовательность от "мифника"
																												//		если не выставлен - от АЦП
	#define FPGA_CIC2SRAM_mode 								(0x10)			// Режим записи в SRAM со стороны ПЛИС выхода CIC (используется в техн. режиме)
	#define FPGA_RESV1 												(0x20)			// Подать "1" на выход ПЛИС RESV1 (если бит не выставлен, на RESV1 "0")
	#define FPGA_RESV2 												(0x40)			// Подать "1" на выход ПЛИС RESV2 (если бит не выставлен, на RESV2 "0")
	#define FPGA_EN_HFS 											(0x80)			// Подать "1" на выход ПЛИС EN_HFS (если бит не выставлен, на EN_HFS "0") 
	#define FPGA_EN_TEST 											(0x100)			// Подать "1" на выход ПЛИС EN_TEST (если бит не выставлен, на EN_TEST "0") 
	#define FPGA_BEAM_en 											(0x200)			// Включение мультиплексора управления номером луча
																												//			если бит выставлен - управление сигналами {BEAM1, BEAM0} - от МК
																												//			если бит не выставлен - управление сигналами {BEAM1, BEAM0} - от схемы ЦОС ПЛИС (DSP.v)
	#define FPGA_BEAM_0 											(0x0)				// {BEAM1, BEAM0} = 2'b00;
	#define FPGA_BEAM_1 											(0x400)				// {BEAM1, BEAM0} = 2'b01;
	#define FPGA_BEAM_2 											(0x800)				// {BEAM1, BEAM0} = 2'b10;
	#define FPGA_BEAM_3 											(0xC00)				// {BEAM1, BEAM0} = 2'b11;
	#define FPGA_RESET 											(0x1000)			// Сброс ПЛИС
	#define FPGA_TestSignalMode							(0x2000)			// Режим тестовогог сигнала
	// 	FPGA_DSP_CTRLREG bits
	#define FPGA_DSP_CTRLREG_RESET						(0x0)				// Обнулить регистр управления схемы ЦОС
	#define FPGA_DSP_CTRLREG_START						(0x1)				// Старт схемы ЦОС (используется передний фронт)
	#define FPGA_DSP_CTRLREG_STOP							(0x2)				// Стоп схемы ЦОС (используется передний фронт)
	#define FPGA_DSP_CTRLREG_BEAMCHANGE				(0x4)				// Разрежение начального задания номера луча (если не включено - стартуем с луча 0)
	#define FPGA_DSP_CTRLREG_BEAMNUMINPUT_0		(0x0)				// Номер стартового луча - 0 (работает при включенном FPGA_DSP_CTRLREG_BEAMCHANGE)
	#define FPGA_DSP_CTRLREG_BEAMNUMINPUT_1		(0x8)				// Номер стартового луча - 1 (работает при включенном FPGA_DSP_CTRLREG_BEAMCHANGE)
	#define FPGA_DSP_CTRLREG_BEAMNUMINPUT_2		(0x10)				// Номер стартового луча - 2 (работает при включенном FPGA_DSP_CTRLREG_BEAMCHANGE)
	#define FPGA_DSP_CTRLREG_BEAMNUMINPUT_3		(0x18)				// Номер стартового луча - 3 (работает при включенном FPGA_DSP_CTRLREG_BEAMCHANGE)
	#define FPGA_DSP_CTRLREG_STOP4_EN					(0x20)			// Разрешение на стоп после 4-го пуска алгоритма (используется в технологическом режиме)
	#define FPGA_DSP_CTRLREG_80MS_EN					(0x40)			// Режим накопления сигнала в течение 80 мс (Режим "Висение")
	
	extern uint8_t ext_int1;
	extern uint32_t time_FPGA_CNT;
	extern DataTimeStructTypeDef	time_FPGA;
	
	void ExternalBusFPGAConfig(void);											// настройки внешней FPGA
	void ExternalBusSRAMConfig(void);											// настройки внешней SRAM
	void EXT_INT1_IRQHandler(void);	
	uint32_t TestSRAM(void);															// Проверка работы SRAM. 0 - успех! либо адрес ошибки
	
	void cpu_wr_reg(uint16_t addr, uint16_t data);				// Функция записи в регистр ПЛИС по адресу
	uint16_t cpu_rd_reg(uint16_t addr);										// Функция чтения регистра ПЛИС по адресу
	uint16_t cpu_rd_result_done(void);										// Опрос (поллинг) бита готовности результата цифровой обработки
//	void cpu_wr_init_adc(void);													// Инициализация АЦП
	void cpu_wr_init_adc(uint16_t REFDAC1, uint16_t REFDAC2);				// Инициализация АЦП
//	uint16_t cpu_rd_fifo_word(void);										// Чтение FIFO от АЦП (в технологическом режиме)

	
	void tech_mode_read_memory(uint32_t cntSRAMPacket);		// Итерация чтения SRAM (по порциям 512 байт) 
	void tech_mode_read_status(void);											// Чтение статуса заполненности SRAM с отправлением ответа на ПК
	void tech_mode_start(void);														// Старт вычитывания выхода АЦП в технологическом режиме
	void cic_mode_start(uint16_t control);									// Старт вычитывания выхода CIC в технологическом режиме
	void tech_mode_read_result(void);											// Ответ компу с результатами вычислений
	void tech_mode_end(void);																	// Выход из технологического режима
	void telemetry_mode_start(uint16_t control);						// Старт режима телеметрии
	//void telemetry_mode_read_memory(uint32_t cntSRAMPacket);		// Чтение SRAM в режиме телеметрии
	
#endif 	//__EBC_FUNC_H__
