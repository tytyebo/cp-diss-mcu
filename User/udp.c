//#include "udp.h"
#include "string.h"
#include <stdint.h>
#include "MDR1986VE1T.h"
#include "tcpip.h"
//#include "ebc_func.h"

extern uint16_t RecdFrameMAC[3];     	// 48 bit MAC
extern uint16_t RecdFrameIP[2];        	// 32 bit IP
extern uint16_t RecdIPFrameLength;         	// 16 bit IP packet length
extern uint8_t TransmitControl;
extern uint8_t TxFrame2[ETH_CTRL_FIELD + ETH_HEADER_SIZE + MAX_ETH_TX_DATA_SIZE];
extern uint16_t MyIP[];
extern uint16_t ToIP[];
uint8_t eth_data[1025];
uint16_t leneth;
uint8_t ethcount = 0;
extern pEthernetUDP_Frame EthernetUDP_Frame;
//extern uint8_t fl_telemetry_mode;

uint8_t *udpData;
uint16_t udpDataLen = 0;
uint8_t status[] = {0x0, 0x0, 0x0, 0x0};

//----------------------------------------------------------------------------
// UDP FRAME
// process UDP packet

uint32_t PreCheckSum (uint32_t PreValue, void* Buf, uint16_t Size)
{
	uint16_t Num = Size/2;
	uint16_t i;
  uint32_t cs = PreValue;
  for(i=0; i< 2*Num; i+=2 ) {
    cs += (((uint8_t*)Buf)[i] ) + ((((uint8_t*)Buf)[i+1])<< 8);
  }
	if ( Size > 2*Num ) {
		cs+= ((uint8_t*)Buf)[i];
	}
  return cs;
}

uint16_t CheckSum (uint32_t PreValue, void* Buf, uint16_t Size)
{
	uint16_t Num = Size/2;
	uint16_t i;
  uint32_t cs = PreValue;
  for(i=0; i< 2*Num; i+=2 ) {
    cs+= (((uint8_t*)Buf)[i])  + ((((uint8_t*)Buf)[i+1])<< 8);
  }
	if ( Size > 2*Num ) {
		cs+= ((uint8_t*)Buf)[i];
	}
  cs=(cs>>16)+(cs&0xFFFF);
  return (uint16_t)(~cs);
}

void udp_reply(uint8_t * ptr_UdpData, uint16_t UDPDataCount)
{
	// Prepare UDP-packet for reply
	uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame2[4];
	
	UDP_Pseudo_Header tmpUDPPseudoHeader;	
	
	// Ethernet
	/* Set destanation MAC address */
	*(uint32_t *) &ptr_OutFrame[0] = (((((uint8_t) (RecdFrameMAC[1] >> 8) & 0xFF) << 24)
			                       | (((uint8_t) RecdFrameMAC[1] & 0xFF) << 16)
			                       | (((uint8_t) (RecdFrameMAC[0] >> 8) & 0xFF) << 8)
			                       | ((uint8_t) RecdFrameMAC[0] & 0xFF)));

	*(uint16_t *) &ptr_OutFrame[4] = ((((uint8_t) (RecdFrameMAC[2] >> 8) & 0xFF) << 8)
							       | ((uint8_t) RecdFrameMAC[2] & 0xFF));
	
	/* Set our MAC address */
	ptr_OutFrame[6] = MAC_5;
	ptr_OutFrame[7] = MAC_4;
	ptr_OutFrame[8] = MAC_3;
	ptr_OutFrame[9] = MAC_2;
	ptr_OutFrame[10] = MAC_1;
	ptr_OutFrame[11] = MAC_0;	
	*(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]			= SWAPB(FRAME_IP);
	// IP
	*(uint16_t *)&ptr_OutFrame[IP_VER_IHL_TOS_OFS] 		= SWAPB(IP_VER_IHL);
	WriteWBE(&ptr_OutFrame[IP_TOTAL_LENGTH_OFS], IP_HEADER_SIZE + UDP_HEADER_SIZE + UDPDataCount);
	*(uint16_t *)&ptr_OutFrame[IP_IDENT_OFS] 			= 0;//EthernetIP_Frame->Identification;//PROT_ICMP;
	*(uint16_t *)&ptr_OutFrame[IP_FLAGS_FRAG_OFS] 		= 0x00;
	*(uint16_t *)&ptr_OutFrame[IP_TTL_PROT_OFS] 		= SWAPB((128 << 8) | PROT_UDP);//SWAPB((DEFAULT_TTL << 8) | PROT_UDP);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;
	memcpy(&ptr_OutFrame[IP_SOURCE_OFS], MyIP, 4);
	memcpy(&ptr_OutFrame[IP_DESTINATION_OFS], RecdFrameIP, 4);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;//CalcChecksum(&ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE, 0);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] = CheckSum (0, &ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE);

	// UDP	
    *(uint16_t *)&ptr_OutFrame[UDP_SRC_PORT]	=	EthernetUDP_Frame->DEST_port;
    *(uint16_t *)&ptr_OutFrame[UDP_DST_PORT]	=	EthernetUDP_Frame->SRC_port;

	*(uint16_t *)&ptr_OutFrame[UDP_LEN_DGRM]	= SWAPB(UDPDataCount + UDP_HEADER_SIZE);
	*(uint16_t *)&ptr_OutFrame[UDP_CKSUM]		= 0x0;
			
	memcpy(&ptr_OutFrame[UDP_DATA], ptr_UdpData, UDPDataCount);
	
	memcpy ( (void*) &(tmpUDPPseudoHeader.SrcIP), MyIP, 4);
	memcpy ( (void*) &(tmpUDPPseudoHeader.DstIP), RecdFrameIP, 4);
	tmpUDPPseudoHeader.Zero = 0x00;
	tmpUDPPseudoHeader.Protocol = PROT_UDP;
	tmpUDPPseudoHeader.UDPLength = SWAPB(UDPDataCount + UDP_HEADER_SIZE);
	
	*(uint16_t *)&ptr_OutFrame[UDP_CKSUM] = 0;
	
	/* Set the length of the send frame */
	*(uint32_t*)&TxFrame2[0] = ETH_HEADER_SIZE + IP_HEADER_SIZE + UDPDataCount + UDP_HEADER_SIZE;
		
	TransmitControl |= SEND_FRAME2;
}

void udp_echo()
{
	udpDataLen = SWAPB(EthernetUDP_Frame->len) - UDP_HEADER_SIZE;
	udpData = (uint8_t *) EthernetUDP_Frame->data;
}
void udp_echo2()
{
    int ii;
    leneth = SWAPB(EthernetUDP_Frame->len) - UDP_HEADER_SIZE;
    for(ii = 0; ii < leneth ; ii++)
    {
        eth_data [ii] = EthernetUDP_Frame->data[ii];        
    }
    ethcount++;
}
//-----------------------------------------------------------------------
//----------------- UDP technilogic mode
void udp_tech_mode()
{
	uint8_t *data;
	uint8_t state;
//	uint16_t control;
//	uint16_t len;
//	uint32_t cntSRAMPacket;
//	uint32_t i = 0;
//	uint16_t REFDAC1, REFDAC2;
		
	data = (uint8_t *)EthernetUDP_Frame->data;
//	len = SWAPB(EthernetUDP_Frame->len) - UDP_HEADER_SIZE;
 		
	state = data[0];
//	cntSRAMPacket = data[1] | (data[2] << 8);
//	
//	
//	REFDAC1 = data[1] | ((data[2] & 0x3) << 8);
//	REFDAC2 = ((data[2] & 0xfc) >> 2) | data[3] << 6;
//	
//	//uint8_t beam_num_input = data[1];
//	control = data[1] | (data[2] << 8);
	
	switch  (state)
	{
        /*
		case 0x31:
			tech_mode_start();
			udp_reply(udpData, udpDataLen);
			break;
		case 0x32:
			tech_mode_read_status();
			udp_reply(udpData, udpDataLen);
			break;
		case 0x33:
			tech_mode_read_memory(cntSRAMPacket); 
			udp_reply(udpData, udpDataLen);
			break;
		case 0x34:
			cpu_wr_init_adc(REFDAC1, REFDAC2);
			udp_reply(udpData, udpDataLen);
			break;
		case 0x35:
			cic_mode_start(control);
			udp_reply(udpData, udpDataLen);
			break;		
		case 0x36:
			tech_mode_read_result();
			udp_reply(udpData, udpDataLen);
			break;
		case 0x37:
			tech_mode_end();	
			udp_reply(udpData, udpDataLen);
			break;
		case 0x38:
			telemetry_mode_start(control);
			//udp_reply(udpData, udpDataLen);
			break;
		case 0x39:
			telemetry_mode_read_memory(cntSRAMPacket);
			//udp_reply(udpData, udpDataLen);
			break;*/
		default:
			udp_echo2();
            //udp_echo();
			//udp_reply(udpData, udpDataLen);
			//tech_mode_read_status();
			break;
	}

}

void udp_proceed()
{
	udp_echo2();
}

// reply to UDP packet
// len is UDP data payload length

void udp_send(uint8_t * ptr_UdpData, uint16_t UDPDataCount)
{
	// Prepare UDP-packet for send
	uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame1[4];
	
	UDP_Pseudo_Header tmpUDPPseudoHeader;	
	
	// Ethernet
	/* Set destination MAC address */
	ptr_OutFrame[0] = MAC2_5;
	ptr_OutFrame[1] = MAC2_4;
	ptr_OutFrame[2] = MAC2_3;
	ptr_OutFrame[3] = MAC2_2;
	ptr_OutFrame[4] = MAC2_1;
	ptr_OutFrame[5] = MAC2_0;
	/* Set our MAC address */
	ptr_OutFrame[6] = MAC_5;
	ptr_OutFrame[7] = MAC_4;
	ptr_OutFrame[8] = MAC_3;
	ptr_OutFrame[9] = MAC_2;
	ptr_OutFrame[10] = MAC_1;
	ptr_OutFrame[11] = MAC_0;
	
	
	*(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]			= SWAPB(FRAME_IP);
	// IP
	*(uint16_t *)&ptr_OutFrame[IP_VER_IHL_TOS_OFS] 		= SWAPB(IP_VER_IHL);
	WriteWBE(&ptr_OutFrame[IP_TOTAL_LENGTH_OFS], IP_HEADER_SIZE + UDP_HEADER_SIZE + UDPDataCount);
	*(uint16_t *)&ptr_OutFrame[IP_IDENT_OFS] 			= 0;//EthernetIP_Frame->Identification;//PROT_ICMP;
	*(uint16_t *)&ptr_OutFrame[IP_FLAGS_FRAG_OFS] 		= 0x00;
	*(uint16_t *)&ptr_OutFrame[IP_TTL_PROT_OFS] 		= SWAPB((128 << 8) | PROT_UDP);//SWAPB((DEFAULT_TTL << 8) | PROT_UDP);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;
	memcpy(&ptr_OutFrame[IP_SOURCE_OFS], MyIP, 4);
	memcpy(&ptr_OutFrame[IP_DESTINATION_OFS], ToIP, 4);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;//CalcChecksum(&ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE, 0);

	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] = CheckSum (0, &ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE);

	// UDP
    *(uint16_t *)&ptr_OutFrame[UDP_SRC_PORT]	=	0x8EB1;
    *(uint16_t *)&ptr_OutFrame[UDP_DST_PORT]	=	0x8EB1;

	*(uint16_t *)&ptr_OutFrame[UDP_LEN_DGRM]	= SWAPB(UDPDataCount + UDP_HEADER_SIZE);
	*(uint16_t *)&ptr_OutFrame[UDP_CKSUM]		= 0x0;
			
	memcpy(&ptr_OutFrame[UDP_DATA], ptr_UdpData, UDPDataCount);
	
	memcpy ( (void*) &(tmpUDPPseudoHeader.SrcIP), MyIP, 4);
	memcpy ( (void*) &(tmpUDPPseudoHeader.DstIP), ToIP, 4);
	tmpUDPPseudoHeader.Zero = 0x00;
	tmpUDPPseudoHeader.Protocol = PROT_UDP;
	tmpUDPPseudoHeader.UDPLength = SWAPB(UDPDataCount + UDP_HEADER_SIZE);
	
	*(uint16_t *)&ptr_OutFrame[UDP_CKSUM] = 0;
	
	/* Set the length of the send frame */
	*(uint32_t*)&TxFrame2[0] = ETH_HEADER_SIZE + IP_HEADER_SIZE + UDPDataCount + UDP_HEADER_SIZE;
		
	TransmitControl |= SEND_FRAME2;
}
