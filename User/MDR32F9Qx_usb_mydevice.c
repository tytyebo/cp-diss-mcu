
/* Includes ------------------------------------------------------------------*/
/* Includes ------------------------------------------------------------------*/
#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_usb_handlers.h"
#include "MDR32F9Qx_usb_mydevice.h"

/** @defgroup USB_MyDevice_Private_Types USB MyDevice Private Types
  * @{
  */
  
typedef struct {
    uint8_t* MyDevice_ReceiveBuffer;
    uint32_t MyDevice_DataPortionLength;
    volatile USB_Result MyDevice_SendDataStatus,
                        MyDevice_ReceiveDataStatus;
}USB_MyDeviceContext_TypeDef;


/** @} */ /* End of group USB_MyDevice_Private_Types */

/** @defgroup USB_MyDevice_Private_Constants USB MyDevice Private Constants
  * @{
  */

#ifdef USB_REMOTE_WAKEUP_SUPPORTED
#define USB_REMOTE_WAKEUP                    0x20
#else
#define USB_REMOTE_WAKEUP                    0
#endif /* USB_REMOTE_WAKEUP_SUPPORTED */
#ifdef USB_SELF_POWERED_SUPPORTED
#define USB_SELF_POWERED                     0x40
#else
#define USB_SELF_POWERED                     1
#endif /* USB_SELF_POWERED_SUPPORTED */
#define USB_POWER_ATTIBUTES                  0x80 | USB_REMOTE_WAKEUP | USB_SELF_POWERED

/** @} */ /* End of group USB_MyDevice_Private_Constants */

/** @defgroup USB_MyDevice_Private_Variables USB MyDevice Private Variables
  * @{
  */

/**
  * @brief MyDevice internal context
  */

/*static*/ USB_MyDeviceContext_TypeDef USB_MyDeviceContext;

/**
  * @brief Standard Device Descriptor
  */


static uint8_t Usb_MyDevice_Device_Descriptor[0x12] =
{
    0x12,            /* bLength                  */
    0x01,            /* bDescriptorType (Device) */
    0x10, 0x01,      /* bcdUSB                   */
    0xFF,            /* bDeviceClass (Vendor)    */
    0x00,            /* bDeviceSubClass          */
    0x00,            /* bDeviceProtocol          */
    MAX_PACKET_SIZE, /* bMaxPacketSize0          */
    0x28, 0x02,      /* idVendor                 */
    0x59, 0x03,      /* idProduct                */
    0x00, 0x00,      /* bcdDevice                */
    0x00,            /* iManufacturer            */
    0x00,            /* iProduct                 */
    0x00,            /* iSerialNumber            */
    0x01             /* bNumConfigurations       */
};

/**
  * @brief Standard Configuration Descriptor
  */

#define USB_MYDEVICE_CONFIG_DESCR_LENGTH   0x32

static uint8_t Usb_MyDevice_Configuration_Descriptor[USB_MYDEVICE_CONFIG_DESCR_LENGTH] =
{
    /* Configuration Descriptor */
    0x09,                                   /* bLength                         */
    0x02,                                   /* bDescriptorType (Configuration) */
    USB_MYDEVICE_CONFIG_DESCR_LENGTH, 0x00, /* wTotalLength                    */
    0x01,                                   /* bNumInterfaces                  */
    0x01,                                   /* bConfigurationValue             */
    0x00,                                   /* iConfiguration                  */
    USB_POWER_ATTIBUTES,                    /* bmAttributes                    */
    0x32,                                   /* bMaxPower (100 mA)              */

    /* Data class interface Descriptor */
    0x09,       /* bLength (Endpoint Descriptor size)        */
    0x04,       /* bDescriptorType (Interface)               */
    0x00,       /* bInterfaceNumber                          */
    0x00,       /* bAlternateSetting                         */
    0x02,       /* bNumEndpoints (Two endpoints used)        */
    0xFF,       /* bInterfaceClass (Vendor)                  */
    0x00,       /* bInterfaceSubClass                        */
    0x00,       /* bInterfaceProtocol                        */
    0x00,       /* iInterface                                */
    /* Endpoint 1 Descriptor */
    0x07,       /* bLength (Endpoint Descriptor size)        */
    0x05,       /* bDescriptorType (Endpoint)                */
    0x81,       /* bEndpointAddress (IN | 1)                 */
    0x03,       /* bmAttributes (Interrupt)                  */
    MAX_PACKET_SIZE, 0x00, /* wMaxPacketSize                 */
    0x00,       /* bInterval                                 */
    /* Endpoint 2 Descriptor */
    0x07,       /* bLength (Endpoint Descriptor size)        */
    0x05,       /* bDescriptorType (Endpoint)                */
    0x02,       /* bEndpointAddress (OUT | 2)                */
    0x02,       /* bmAttributes (Bulk)                       */
    MAX_PACKET_SIZE, 0x00, /* wMaxPacketSize                 */
    0x00,       /* bInterval                                 */
};

/** @} */ /* End of group USB_MyDevice_Private_Variables */

/** @defgroup USB_MyDevice_Private_FunctionPrototypes USB MyDevice Private Function Prototypes
  * @{
  */

static USB_Result USB_MyDevice_OnDataSent(USB_EP_TypeDef EPx, uint8_t* Buffer, uint32_t Length);
static USB_Result USB_MyDevice_OnDataReceive(USB_EP_TypeDef EPx, uint8_t* Buffer, uint32_t Length);

/** @} */ /* End of group USB_MyDevice_Private_FunctionPrototypes */

/** @defgroup USB_MyDevice_Private_Functions USB MyDevice Private Functions
  * @{
  */

/**
  * @brief  Sets the (new) buffer for incoming data.
  *
  * @param  ReceiveBuffer: Pointer to memory buffer to place incoming
  *         data to. The buffer length should be equal or greater
  *         then MAX_PACKET_LENGTH and DataPortionLength.
  *
  * @param  DataPortionLength: Amount of data received that triggers
  *         USB_MYDEVICE_HANDLE_DATA_RECEIVE handler to be called. If any
  *         data should be immediately handled, this parameter should
  *         be set to 1.
  *
  * @retval USB_Result.
  */

USB_Result USB_MyDevice_SetReceiveBuffer(uint8_t* ReceiveBuffer, uint32_t DataPortionLength)
{
  USB_MyDeviceContext.MyDevice_ReceiveBuffer = ReceiveBuffer;
  USB_MyDeviceContext.MyDevice_DataPortionLength = DataPortionLength;

  return USB_SUCCESS;
}

/**
  * @brief  Initializes MyDevice context.
  *
  * @param  ReceiveBuffer: Pointer to memory buffer to place incoming
  *         data to. The buffer length should be equal or greater
  *         then MAX_PACKET_LENGTH and DataPortionLength.
  *
  * @param  DataPortionLength: Amount of data received that triggers
  *         USB_MYDEVICE_HANDLE_DATA_RECEIVE handler to be called. If any
  *         data should be immediately handled, this parameter should
  *         be set to 1.
  *
  * @param  StartReceiving: Flag indicating whether the device will accept
  *         incoming data immediately after USB_MyDevice_Reset is called for the
  *         first time.
  *
  * @retval USB_Result.
  */

USB_Result USB_MyDevice_Init(uint8_t* ReceiveBuffer, uint32_t DataPortionLength, FlagStatus StartReceiving)
{
  USB_MyDevice_SetReceiveBuffer(ReceiveBuffer, DataPortionLength);
  USB_MyDeviceContext.MyDevice_SendDataStatus = USB_SUCCESS;
  USB_MyDeviceContext.MyDevice_ReceiveDataStatus = StartReceiving ? USB_SUCCESS : USB_ERR_BUSY;

  return USB_SUCCESS;
}

/**
  * @brief  Starts (or restarts) incoming data receiving.
  *
  * @retval USB_Result.
  *
  */

USB_Result USB_MyDevice_ReceiveStart(void)
{
  USB_MyDeviceContext.MyDevice_ReceiveDataStatus = USB_SUCCESS;
  return USB_EP_doDataOut(USB_MYDEVICE_EP_RECEIVE, USB_MyDeviceContext.MyDevice_ReceiveBuffer,
            USB_MyDeviceContext.MyDevice_DataPortionLength, USB_MyDevice_OnDataReceive);
}

/**
  * @brief  Stops (suspends) incoming data receiving.
  *
  * @retval USB_Result.
  *
  */

USB_Result USB_MyDevice_ReceiveStop(void)
{
  USB_MyDeviceContext.MyDevice_ReceiveDataStatus = USB_ERR_BUSY;
  return USB_EP_Stall(USB_MYDEVICE_EP_RECEIVE, USB_STALL_PROTO);
}

/**
  * @brief  Initiates sending of data portion to the host
  *
  * @param  Buffer: Pointer to the user's buffer containing data
  *         to be sent
  *
  * @param  Length: Length of data
  *
  * @retval USB_Result.
  *
  * @note   The function returns immediately.
  */

USB_Result USB_MyDevice_SendData(uint8_t* Buffer, uint32_t Length)
{
  /* Check if previous data sending is complete */
  USB_Result result = USB_MyDeviceContext.MyDevice_SendDataStatus;

  /* If data can be sent, set "busy" flag and initiate data portion transfer */
  if (result == USB_SUCCESS)
  {
    USB_MyDeviceContext.MyDevice_SendDataStatus = USB_ERR_BUSY;
    result = USB_EP_doDataIn(USB_MYDEVICE_EP_SEND, Buffer, Length, USB_MyDevice_OnDataSent);
  }
  led_usb_flag = 1;
  return result;
}

/** @defgroup USB_MyDevice_Private_Overwritten_Device_Handlers USB MyDevice Overwritten Device Handlers implementation
  * @{
  */
  
/**
  * @brief  Initial MyDevice setup/reset
  *
  * @param  None
  *
  * @retval USB_Result.
  */

USB_Result USB_MyDevice_Reset(void)
{
  USB_Result result;

  /* Inherited handler call */
  result = USB_DeviceReset();

  if (result == USB_SUCCESS)
  {
    /* EP1, EP2 and EP3 initialization. Note: DATA1 bit would be toggled to DATA0 on the first
     * IN transaction */
    USB_EP_Init(USB_MYDEVICE_EP_SEND, USB_SEPx_CTRL_EPEN_Enable | USB_SEPx_CTRL_EPDATASEQ_Data1,
                USB_MYDEVICE_HANDLE_SEND_ERROR);
    USB_EP_Init(USB_MYDEVICE_EP_RECEIVE, USB_SEPx_CTRL_EPEN_Enable, 0);

    /* Start to listen for incoming data, if desired */
    if (USB_MyDeviceContext.MyDevice_ReceiveDataStatus == USB_SUCCESS)
    {
      result = USB_EP_doDataOut(USB_MYDEVICE_EP_RECEIVE, USB_MyDeviceContext.MyDevice_ReceiveBuffer,
                USB_MyDeviceContext.MyDevice_DataPortionLength, USB_MyDevice_OnDataReceive);
    }
  }
  return result;
}

/**
  * @brief  Initial USB_MYDEVICE_EP_RECEIVE setup/reset
  *
  * @param  None
  *
  * @retval USB_Result.
  */

USB_Result USB_MyDevice_Receive_Reset(void)
{
  USB_Result result;
   
  /* Start to listen for incoming data, if desired */
  if (USB_MyDeviceContext.MyDevice_ReceiveDataStatus == USB_SUCCESS)
  {
    result = USB_EP_doDataOut(USB_MYDEVICE_EP_RECEIVE, USB_MyDeviceContext.MyDevice_ReceiveBuffer,
             USB_MyDeviceContext.MyDevice_DataPortionLength, USB_MyDevice_OnDataReceive);
  }
  return result;
}

/**
  * @brief  GET_DESCRIPTOR standard request handler implementation for MyDevice.
  *
  * @param  wVALUE: Descriptor Type and Descriptor Index
  *
  * @param  wINDEX: Not used
  *
  * @param  wLENGTH: Amount of data host is willing to receive.
  *
  * @retval USB_Result.
  *
  * @note   This handler transmits to host an appropriate MyDevice descriptor data.
  */

USB_Result USB_MyDevice_GetDescriptor(uint16_t wVALUE, uint16_t wINDEX, uint16_t wLENGTH)
{ 
  uint8_t* pDescr = 0;
  uint32_t length;
  USB_Result result = USB_SUCCESS;
  /* Only 0 configuration is supported; for device request this field must be 0 */
  if ((wVALUE & 0xFF) != 0)
  {
    result = USB_ERROR;
  }
  else
  {
    switch (wVALUE >> 8)      /* Descriptor type */
    {
      case USB_DEVICE:
        pDescr = Usb_MyDevice_Device_Descriptor;
        length = 0x12;
        break;
      case USB_CONFIGURATION:
        pDescr = Usb_MyDevice_Configuration_Descriptor;
        length = USB_MYDEVICE_CONFIG_DESCR_LENGTH;
        break;
      default:
        result = USB_ERROR;
    }
  }

  if (result == USB_SUCCESS)
  {
    if (length > wLENGTH)
    {
      length = wLENGTH;
    }
    result = USB_EP_doDataIn(USB_EP0, pDescr, length, USB_DeviceDoStatusOutAck);
  }
  return result;
}

/** @} */ /* End of group USB_MyDevice_Private_Overwritten_Device_Handlers */

/**
  * @brief  Handler called when data is sent.
  *
  * @param  EPx: USB EndPoint number.
  *         This parameter can be one of the following values:
  *         USB_EP0, USB_EP1, USB_EP2, USB_EP3.
  *
  * @param  Buffer: Pointer to the user's buffer with portion of data
  *                 sent
  *
  * @param  Length: Length of data portion
  *
  * @retval USB_Result.
  *
  * @note   This function interface reflects EndPoint data transmitting handler
  *         requirements.
  */


static USB_Result USB_MyDevice_OnDataSent(USB_EP_TypeDef EPx, uint8_t* Buffer, uint32_t Length)
{
  /* Release "busy" flag and call user's handler */
  USB_MyDeviceContext.MyDevice_SendDataStatus = USB_SUCCESS;
  return USB_MYDEVICE_HANDLE_DATA_SENT;
}


/**
  * @brief  Handler called when some data is received.
  *
  * @param  EPx: USB EndPoint number.
  *         This parameter can be one of the following values:
  *         USB_EP0, USB_EP1, USB_EP2, USB_EP3.
  *
  * @param  Buffer: Pointer to the user's buffer with received data
  *
  * @param  Length: Length of data
  *
  * @retval USB_Result.
  *
  * @note   This function interface reflects EndPoint data transmitting handler
  *         requirements.
  */

static USB_Result USB_MyDevice_OnDataReceive(USB_EP_TypeDef EPx, uint8_t* Buffer, uint32_t Length)
{
  /* Call user's handler */
  USB_Result result = USB_MYDEVICE_HANDLE_DATA_RECEIVE(Buffer, Length);

  /* If handler returns USB_SUCCESS, wait for another portion. Otherwise, stop to receive
   * incoming data */
  if (result != USB_SUCCESS)
  {
    USB_MyDeviceContext.MyDevice_ReceiveDataStatus = USB_ERR_BUSY;
  }
  if (USB_MyDeviceContext.MyDevice_ReceiveDataStatus == USB_SUCCESS)
  {
    return USB_EP_doDataOut(USB_MYDEVICE_EP_RECEIVE, USB_MyDeviceContext.MyDevice_ReceiveBuffer,
              USB_MyDeviceContext.MyDevice_DataPortionLength, USB_MyDevice_OnDataReceive);
  }
  else
  {
    return USB_SUCCESS;
  }
}

/** @} */ /* End of group USB_CDC_Private_Functions */

/*
*
* END OF FILE MDR32F9Qx_usb_mydevice.c */

