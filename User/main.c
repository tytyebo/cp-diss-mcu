#include "MDR32F9Qx_config.h"
#include "CP_DISS_MCU_Setup.h"
#include "CP_DISS_MCU_employ.h"
#include "MDR32F9Qx_usb_default_handlers.h"
#include "MDR32F9Qx_usb_handlers.h"
#include "MDR32F9Qx_usb.h"
#include "MDR32F9Qx_usb_device.h"
#include "MDR32F9Qx_usb_mydevice.h"
#include "arinc_config.h"
#include "timer_config.h"
#include "eth_config.h"
#include "adc_config.h"
#include "uart_config.h"
#include "MDR32F9Qx_uart.h"

extern USB_RxBuffer_TypeDef USB_RxBuffer;
extern USB_TxBuffer_TypeDef USB_TxBuffer;
uint8_t ArcSd = 0, Enable_WD_STR = 1, State_WS_STR, Enb_SW_RK = 0, MPR_mode = 0, bstate = 0, ADC_flag = 0, adc_channel, LED_flag = 0, arinc_auto_send_config = 0, auto_ctrl_2 = 0;
// Запоминает количество принятых пакетов (для обнаружение новых сообщений)
uint8_t ecount = 0;
uint16_t helper;
uint32_t timer_eth = 0, timer_ari = 0, timer_aro = 0, timer_usb = 0, arinc_course = 0, arinc_roll = 0, arinc_recognize = 0;
uint32_t timer_course = 0, timer_recognize = 0, timer_roll = 0, course_time = 0, recognize_time = 0, roll_time = 0;
uint8_t cragodan[64 - 1];  // см. sizeof(USB_RxBuffer_TypeDef) (by Maksim Kuznetsov)
uint8_t eth_mass[1057];
// Хранит код команды последнего запроса к МПР
uint8_t lastRequestMpr = 0; // инициализация не должена совпадать с кодом запроса
void int_led(void);
void _arinc_data(uint8_t comand_name);
void _arinc_time(uint8_t comand_name);
void _ad_all(uint8_t comand_name, uint8_t buff_number,uint8_t channel_name);
void _at_all(uint8_t comand_name, uint8_t buff_number, uint8_t channel_number);
void SetLEDs (uint8_t enable);
void ARINC_Send_auto(uint8_t control, uint32_t course_time, uint32_t recognize_time, uint32_t roll_time);
// Отправка МПРу сообщение из USB
void toMpr() {
    uint8_t j;
    ecount = ethcount;
    cragodan [0] = USB_RxBuffer.Command;
    lastRequestMpr = USB_RxBuffer.Command;
    for (j = 0; j < USB_RxBuffer.DataLen; j++)
        cragodan [j+1] = USB_RxBuffer.Data[j];
    udp_send(cragodan, USB_RxBuffer.DataLen + 1);
    DoNetworkStuff(MDR_ETHERNET1);
    USB_TxBuffer.Status = USB_CMD_DONE;
    USB_TxBuffer.DataLen = 0;
}
// Отправка данных от МПР или отправка USB_ETH_NOT_RECIVE
void fromMpr() {
    uint16_t j;
    // Проверка на корректность запроса, на получение ответа
    // и на корректность ответа
    if (USB_RxBuffer.DataLen == 0
      && ethcount != ecount
      && eth_data[0] == lastRequestMpr
      && USB_RxBuffer.Command == lastRequestMpr + COUNT_MPR_COMAND) {
        for (j = 2; j < leneth; j++)
            USB_TxBuffer.Data[j - 2] = eth_data[j];
        USB_TxBuffer.DataLen = (leneth - 2);
        *(uint8_t*)(&USB_TxBuffer.Status) = eth_data[1];
    }
    else {
        USB_TxBuffer.DataLen = 0;
        if (USB_RxBuffer.DataLen != 0)
            USB_TxBuffer.Status = USB_CMD_ERROR;
        else if (ethcount == ecount)
            USB_TxBuffer.Status = USB_ETH_NOT_RECIVE;
        else if (eth_data[0] != lastRequestMpr)
            USB_TxBuffer.Status = USB_ETH_ERROR;
        else if (USB_RxBuffer.Command != lastRequestMpr + COUNT_MPR_COMAND)
            USB_TxBuffer.Status = USB_ETH_NO_REQUEST;
    }
}

void DoCommand() {
    uint16_t i1;
    uint32_t helper2;
    if (MIN_ETH_COMAND_REQUEST <= USB_RxBuffer.Command && USB_RxBuffer.Command < MIN_USB_ANSWER && USB_RxBuffer.Command != COEFF_SET) {
        toMpr();
    } else if (MIN_USB_ANSWER <= USB_RxBuffer.Command && USB_RxBuffer.Command < MIN_USB_ANSWER + COUNT_MPR_COMAND) {
        fromMpr();
    } else {
        switch (USB_RxBuffer.Command) {
            case SPEED_TEST:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 1024; i1++)
                        USB_TxBuffer.Data[i1] = 0x37;
                    USB_TxBuffer.DataLen = 1024;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_PWR:
                if (USB_RxBuffer.DataLen == 1) {
                    if(Enb_SW_RK) {
                        Enable_WD_STR = Set_Pwr(USB_RxBuffer.Data[0]);
                    }
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_PWR:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Get_PWR(Enable_WD_STR);
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case CMD_GET_PG:
                if (USB_RxBuffer.DataLen == 0) {
                  USB_TxBuffer.Data[0] = Get_PG();
                  USB_TxBuffer.DataLen = 1;
                  USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_UPR_4RK:
                if (USB_RxBuffer.DataLen == 1) {
                    Set_4RK(USB_RxBuffer.Data[0]);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_UPR_4RK:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Get_UPR_4RK();
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_IND_4RK:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Get_IND_4RK();
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_UPR_9RK:
                if (USB_RxBuffer.DataLen == 2) {
                    if(Enb_SW_RK) {
                        helper = (uint16_t)(USB_RxBuffer.Data[1]<<8) | USB_RxBuffer.Data[0];
                        Set_9RK(helper);
                    }
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_UPR_9RK:
                if (USB_RxBuffer.DataLen == 0) {
                    helper = Get_UPR_9RK();
                    USB_TxBuffer.Data[0] = (uint8_t)helper;
                    USB_TxBuffer.Data[1] = (uint8_t)(helper>>8);
                    USB_TxBuffer.DataLen = 2;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                } 
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_IND_9RK:
                if (USB_RxBuffer.DataLen == 0) {
                    helper = Get_IND_9RK();
                    USB_TxBuffer.Data[0] = (uint8_t)helper;
                    USB_TxBuffer.Data[1] = (uint8_t)(helper>>8);
                    USB_TxBuffer.DataLen = 2;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_UPR_REF_12:
                if (USB_RxBuffer.DataLen == 1) {
                    Set_UPR_REF_12(USB_RxBuffer.Data[0]);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_UPR_REF_12: 
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Get_UPR_REF_12();
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
    ///////////////////////////////////////////////////////////ARINC COMAND///////////////////////////////////////////////
            case SET_ARINC_SPEED:
                if (USB_RxBuffer.DataLen == 1) {
                    helper2 = MDR_TIMER2->CNT;
                    while(MDR_TIMER2->CNT - helper2 < 2000) {}
                    ArcSd = USB_RxBuffer.Data[0];
                    ArincSpeed(ArcSd);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_ARINC_SPEED:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = ArcSd;
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.DataLen = 0;
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                }
                break;
            case SEND_ARINC_CH1:
                if (USB_RxBuffer.DataLen == 4) {
                    helper2 = (uint32_t)(USB_RxBuffer.Data[3]<<24) | (uint32_t)(USB_RxBuffer.Data[2]<<16) | (uint32_t)(USB_RxBuffer.Data[1]<<8) | USB_RxBuffer.Data[0];
                    ArincSendDataCH1(helper2);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case SEND_ARINC_CH2:
                if (USB_RxBuffer.DataLen == 4) {
                    helper2 = (uint32_t)(USB_RxBuffer.Data[3]<<24) | (uint32_t)(USB_RxBuffer.Data[2]<<16) | (uint32_t)(USB_RxBuffer.Data[1]<<8) | USB_RxBuffer.Data[0];
                    ArincSendDataCH2(helper2);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_ARINC_WX:
                _arinc_data(ARINC_WX);
                break;
            case GET_ARINC_WY:
                _arinc_data(ARINC_WY);
                break;
            case GET_ARINC_WZ:
                _arinc_data(ARINC_WZ);
                break;
            case GET_ARINC_MODE:
                _arinc_data(ARINC_MODE);
                break;
            case GET_ARINC_STATUS:
                _arinc_data(ARINC_STATUS);
                break;
            case GET_ARINC_TIME:
                _arinc_data(ARINC_TIME);
                break;
            case GET_ARINC_VERS_MC:
                _arinc_data(ARINC_VERS_MC);
                break;
            case GET_ARINC_CRC_MC_FIRST_PART:
                _arinc_data(ARINC_CRC_MC_FIRST_PART);
                break;
            case GET_ARINC_CRC_MC_SECOND_PART:
                _arinc_data(ARINC_CRC_MC_SECOND_PART);
                break;
            case GET_ARINC_VERS_SHF:
                _arinc_data(ARINC_VERS_SHF);
                break;
            case GET_ARINC_CRC_SHF_FIRST_PART:
                _arinc_data(ARINC_CRC_SHF_FIRST_PART);
                break;
            case GET_ARINC_CRC_SHF_SECOND_PART:
                _arinc_data(ARINC_CRC_SHF_SECOND_PART);
                break;
            case GET_ARINC_VERS_MC3:
                _arinc_data(ARINC_VERS_MC3);
                break;
            case GET_ARINC_CRC_MC3_FIRST_PART:
                _arinc_data(ARINC_CRC_MC3_FIRST_PART);
                break;
            case GET_ARINC_CRC_MC3_SECOND_PART:
                _arinc_data(ARINC_CRC_MC3_SECOND_PART);
                break;
            case SEND_ALL_ARINC:
                if (USB_RxBuffer.DataLen == 0) {
                    _ad_all(ARINC_WX, 0, 1);
                    _ad_all(ARINC_WY, 10, 1);
                    _ad_all(ARINC_WZ, 20, 1);
                    _ad_all(ARINC_MODE, 30, 1);
                    _ad_all(ARINC_STATUS, 40, 1);
                    _ad_all(ARINC_TIME, 50, 1);
                    _ad_all(ARINC_VERS_MC, 60, 1);
                    _ad_all(ARINC_CRC_MC_FIRST_PART, 70, 1);
                    _ad_all(ARINC_CRC_MC_SECOND_PART, 80, 1);
                    _ad_all(ARINC_VERS_SHF, 90, 1);
                    _ad_all(ARINC_CRC_SHF_FIRST_PART, 100, 1);
                    _ad_all(ARINC_CRC_SHF_SECOND_PART, 110, 1);
                    _ad_all(ARINC_WX, 120, 2);
                    _ad_all(ARINC_WY, 130, 2);
                    _ad_all(ARINC_WZ, 140, 2);
                    _ad_all(ARINC_MODE, 150, 2);
                    _ad_all(ARINC_STATUS, 160, 2);
                    _ad_all(ARINC_TIME, 170, 2);
                    _ad_all(ARINC_VERS_MC, 180, 2);
                    _ad_all(ARINC_CRC_MC_FIRST_PART, 190, 2);
                    _ad_all(ARINC_CRC_MC_SECOND_PART, 200, 2);
                    _ad_all(ARINC_VERS_SHF, 210, 2);
                    _ad_all(ARINC_CRC_SHF_FIRST_PART, 220, 2);
                    _ad_all(ARINC_CRC_SHF_SECOND_PART, 230, 2);
                    USB_TxBuffer.DataLen = 240;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_ARINC_ERRORS:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 4; i1++) {
                        USB_TxBuffer.Data[i1] = (uint8_t)(arinc_error_count1 >> (8 * i1));
                        USB_TxBuffer.Data[4 + i1] = (uint8_t)(arinc_error_count2 >> (8 * i1));
                    }
                    USB_TxBuffer.DataLen = 8;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_ARINC_DATA_BUFF:
                if (USB_RxBuffer.DataLen == 12) {
                    arinc_course = (uint32_t)(USB_RxBuffer.Data[3]<<24) | (uint32_t)(USB_RxBuffer.Data[2]<<16) | (uint32_t)(USB_RxBuffer.Data[1]<<8) | USB_RxBuffer.Data[0];
                    arinc_recognize = (uint32_t)(USB_RxBuffer.Data[7]<<24) | (uint32_t)(USB_RxBuffer.Data[6]<<16) | (uint32_t)(USB_RxBuffer.Data[5]<<8) | USB_RxBuffer.Data[4];
                    arinc_roll = (uint32_t)(USB_RxBuffer.Data[11]<<24) | (uint32_t)(USB_RxBuffer.Data[10]<<16) | (uint32_t)(USB_RxBuffer.Data[9]<<8) | USB_RxBuffer.Data[8];
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_ARINC_DATA_BUFF:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 4; i1++) {
                        USB_TxBuffer.Data[i1] = (uint8_t)(arinc_course >> (8 * i1));
                        USB_TxBuffer.Data[4 + i1] = (uint8_t)(arinc_recognize >> (8 * i1));
                        USB_TxBuffer.Data[8 + i1] = (uint8_t)(arinc_recognize >> (8 * i1));
                    }
                    USB_TxBuffer.DataLen = 12;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_ARINC_AUTO_SEND:
                if (USB_RxBuffer.DataLen == 13) {
                    arinc_auto_send_config = USB_RxBuffer.Data[0];
                    course_time = (uint32_t)(USB_RxBuffer.Data[4]<<24) | (uint32_t)(USB_RxBuffer.Data[3]<<16) | (uint32_t)(USB_RxBuffer.Data[2]<<8) | USB_RxBuffer.Data[1];
                    recognize_time = (uint32_t)(USB_RxBuffer.Data[8]<<24) | (uint32_t)(USB_RxBuffer.Data[7]<<16) | (uint32_t)(USB_RxBuffer.Data[6]<<8) | USB_RxBuffer.Data[5];
                    roll_time = (uint32_t)(USB_RxBuffer.Data[12]<<24) | (uint32_t)(USB_RxBuffer.Data[11]<<16) | (uint32_t)(USB_RxBuffer.Data[10]<<8) | USB_RxBuffer.Data[9];
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_ARINC_AUTO_SEND:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = arinc_auto_send_config;
                    for(i1 = 0; i1 < 4; i1++) {
                        USB_TxBuffer.Data[1 + i1] = (uint8_t)(arinc_course >> (8 * i1));
                        USB_TxBuffer.Data[5 + i1] = (uint8_t)(arinc_recognize >> (8 * i1));
                        USB_TxBuffer.Data[9 + i1] = (uint8_t)(arinc_recognize >> (8 * i1));
                    }
                    USB_TxBuffer.DataLen = 13;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
    ///////////////////////////////////////////////////////////////////End ARINC/////////////////////////////////////////////////////
            case SET_LVTTL:
                if (USB_RxBuffer.DataLen == 1) {
                    Set_3LVTTL(USB_RxBuffer.Data[0]);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_LVTTL:
                if (USB_RxBuffer.DataLen == 0) {
                  USB_TxBuffer.Data[0] = Get_3LVTTL();
                  USB_TxBuffer.DataLen = 1;
                  USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_IN_LVTTL:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Get_EN_TTL();
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_BEAM:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 12; i1++) {
                        USB_TxBuffer.Data[i1] = BEAM_state[i1];
                        USB_TxBuffer.Data[12 + i1 * 4] = (uint8_t)BEAM_time[i1];
                        USB_TxBuffer.Data[13 + i1 * 4] = (uint8_t)(BEAM_time[i1]>>8);
                        USB_TxBuffer.Data[14 + i1 * 4] = (uint8_t)(BEAM_time[i1]>>16);
                        USB_TxBuffer.Data[15 + i1 * 4] = (uint8_t)(BEAM_time[i1]>>24);
                    }
                    USB_TxBuffer.DataLen = 60;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_SINHRO_BEAM_MPR:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 24; i1++) {
                        USB_TxBuffer.Data[i1 * 4] = (uint8_t)Sinh_Beam_MPR_time[i1];
                        USB_TxBuffer.Data[1 + i1 * 4] = (uint8_t)(Sinh_Beam_MPR_time[i1]>>8);
                        USB_TxBuffer.Data[2 + i1 * 4] = (uint8_t)(Sinh_Beam_MPR_time[i1]>>16);
                        USB_TxBuffer.Data[3 + i1 * 4] = (uint8_t)(Sinh_Beam_MPR_time[i1]>>24);
                    }
                    USB_TxBuffer.DataLen = 96;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_SINHRO_BEAM_DISS:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 24; i1++) {
                        USB_TxBuffer.Data[i1 * 4] = (uint8_t)Sinh_Beam_DISS_time[i1];
                        USB_TxBuffer.Data[1 + i1 * 4] = (uint8_t)(Sinh_Beam_DISS_time[i1]>>8);
                        USB_TxBuffer.Data[2 + i1 * 4] = (uint8_t)(Sinh_Beam_DISS_time[i1]>>16);
                        USB_TxBuffer.Data[3 + i1 * 4] = (uint8_t)(Sinh_Beam_DISS_time[i1]>>24);
                    }
                    USB_TxBuffer.DataLen = 96;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case MAN_POS:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = !AUTO_MANUAL_POS();//0- авто, 1 - ручной
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_SW:
                if (USB_RxBuffer.DataLen == 1) {
                    Enb_SW_RK = USB_RxBuffer.Data[0];;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_SW:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Enb_SW_RK;
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case WRITE_ETH_MASS:
                if (USB_RxBuffer.DataLen == 33) {
                    for(i1 = 0; i1 < 32; i1++)
                        eth_mass[1 + i1 + USB_RxBuffer.Data[0] * 32] = USB_RxBuffer.Data[i1+1];
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case READ_ETH_MASS:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 1025; i1++) {
                        USB_TxBuffer.Data[i1] = eth_mass[1+i1];
                    }
                    USB_TxBuffer.DataLen = 1025;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case UART_WRITE_MASS:
                if (USB_RxBuffer.DataLen == 28) {
                    for(i1 = 0; i1 < 28; i1++)
                        uart_mass[i1] = USB_RxBuffer.Data[i1];
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case UART_READ_MASS:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 28; i1++) {
                        USB_TxBuffer.Data[i1] = uart_mass[i1];
                    }
                    USB_TxBuffer.DataLen = 28;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_UART_CONTROL:
                if (USB_RxBuffer.DataLen == 1) {
                    uart_ctrl = USB_RxBuffer.Data[0];
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_UART_CONTROL:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = uart_ctrl;
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
    ////////////////////////////////////////////////////////////////////Ethernet Start////////////////////////////////////////////////////
            case SEND_ETH:
                if (USB_RxBuffer.DataLen == 4) {
                    cragodan [0] = USB_RxBuffer.Data[0];
                    cragodan [1] = USB_RxBuffer.Data[1];
                    cragodan [2] = USB_RxBuffer.Data[2];
                    cragodan [3] = USB_RxBuffer.Data[3];
                    udp_send(cragodan,4);
                    DoNetworkStuff(MDR_ETHERNET1);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case READ_ETH:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = irq_adc_count;
                    USB_TxBuffer.Data[1] = eth_data[0];
                    USB_TxBuffer.Data[2] = eth_data[1];
                    USB_TxBuffer.Data[3] = eth_data[2];
                    USB_TxBuffer.Data[4] = eth_data[3];
                    USB_TxBuffer.Data[5] = eth_data[4];
                    USB_TxBuffer.Data[6] = eth_data[5];
                    USB_TxBuffer.Data[7] = eth_data[6];
                    USB_TxBuffer.Data[8] = eth_data[7];
                    USB_TxBuffer.Data[9] = eth_data[8];
                    USB_TxBuffer.Data[10] = eth_data[9];
                    USB_TxBuffer.Data[11] = eth_data[10];
                    USB_TxBuffer.Data[12] = eth_data[11];
                    USB_TxBuffer.Data[13] = eth_data[12];
                    USB_TxBuffer.Data[14] = eth_data[13];
                    USB_TxBuffer.Data[15] = eth_data[14];
                    USB_TxBuffer.Data[16] = eth_data[15];
                    USB_TxBuffer.Data[17] = eth_data[16];
                    USB_TxBuffer.Data[18] = eth_data[17];
                    USB_TxBuffer.Data[19] = eth_data[18];
                    USB_TxBuffer.DataLen = 20;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case COEFF_SET:
                if (USB_RxBuffer.DataLen == 0) {
                    ecount = ethcount;
                    lastRequestMpr = USB_RxBuffer.Command;
                    eth_mass[0] = 0xC2;
                    udp_send(eth_mass,1026);
                    DoNetworkStuff(MDR_ETHERNET1);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            ///////////////////////////////////////////////// Eth End //////////////////////////////////////////////////////
            case ADC_START:
                if (USB_RxBuffer.DataLen == 1) {
                    adc_channel = USB_RxBuffer.Data[0];
                    ADC_flag = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else 
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case ADC_RESULT:
                if (USB_RxBuffer.DataLen == 1) {
                    for(i1 = 0; i1 < 128; i1++) {
                        USB_TxBuffer.Data[i1*2] = adc_mass[128 * USB_RxBuffer.Data[0] + i1];
                        USB_TxBuffer.Data[i1*2+1] = (adc_mass[128 * USB_RxBuffer.Data[0] + i1]>>8);
                    }
                    USB_TxBuffer.DataLen = 256;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_MULT:
                if (USB_RxBuffer.DataLen == 1) {
                    Set_Mult(USB_RxBuffer.Data[0]);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case SET_AUTO_CTRL2:
                if (USB_RxBuffer.DataLen == 1) {
                    auto_ctrl_2 = USB_RxBuffer.Data[0];
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            case GET_AUTO_CTRL2:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = auto_ctrl_2;
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case GET_MULT:
                if (USB_RxBuffer.DataLen == 0) {
                    USB_TxBuffer.Data[0] = Get_Mult();
                    USB_TxBuffer.DataLen = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case RESET_INPUT:
                if (USB_RxBuffer.DataLen == 0) {
                    for(i1 = 0; i1 < 15; i1++) {
                        arinc_data1 [i1] = 0;
                        arinc_data2 [i1] = 0;
                        arinc_data_count1[i1] = 0;
                        arinc_data_count2[i1] = 0;
                        arinc_time1[i1][0] = 0;
                        arinc_time1[i1][1] = 0;
                        arinc_time2[i1][0] = 0;
                        arinc_time2[i1][1] = 0;
                    }
                    for(i1 = 0; i1 < 12; i1++) {
                        BEAM_state[i1] = 0;
                        BEAM_time[i1] = 0;
                        Sinh_Beam_DISS_time[i1] = 0;
                        Sinh_Beam_DISS_time[12 + i1] = 0;
                        Sinh_Beam_MPR_time[i1] = 0;
                        Sinh_Beam_MPR_time[12 + i1] = 0;
                    }
                    ecount = ethcount = leneth = MPR_mode = Enb_SW_RK = ADC_flag = adc_channel = uart_send_is_running = uart_ctrl = bi = si1 = si2 = auto_ctrl_2 = 0;
                    arinc_auto_send_config = course_time = recognize_time = roll_time = 0;
                    ArcSd = 0x00;
                    ArincSpeed(ArcSd);
                    for(i1 = 0; i1 < 1024; i1++) {
                        eth_data[i1] = 0;
                        adc_mass[i1] = 0;
                    }
                    USB_TxBuffer.Status = USB_CMD_DONE;
                    USB_TxBuffer.DataLen = 0;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case RESET_OUTPUT:
                if (USB_RxBuffer.DataLen == 0) {
                    Enable_WD_STR = Set_Pwr(0x08);
                    Set_4RK(0x00);
                    Set_9RK(0x0000);
                    Set_UPR_REF_12(0x00);
                    Set_3LVTTL(0x00);
                    USB_TxBuffer.Status = USB_CMD_DONE;
                    USB_TxBuffer.DataLen = 0;
                }
                else {
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                    USB_TxBuffer.DataLen = 0;
                }
                break;
            case SET_LEDS:
                if (USB_RxBuffer.DataLen == 0) {
                    LED_flag = 1;
                    USB_TxBuffer.Status = USB_CMD_DONE;
                }
                else
                    USB_TxBuffer.Status = USB_CMD_ERROR;
                USB_TxBuffer.DataLen = 0;
                break;
            default:
                USB_TxBuffer.Status = USB_CMD_UNKNOWN;
                USB_TxBuffer.DataLen = 0;
        }
    }
}
int main (void) {
    MCU_Setup();
    Timer_Config();
    ArincSpeed(ArcSd);
    Eth_Config();
    UART_Config();
    while(1) {
        if (USB_RxBuffer.Filled == 1) {
            USB_TxBuffer.Command = USB_RxBuffer.Command;
            if (USB_RxBuffer.Valid == 1)
                DoCommand();
            else {
                USB_TxBuffer.Status = USB_CMD_CHECKSUM_ERROR;
                USB_TxBuffer.DataLen = 0;
            }
            if (USB_TxBuffer.Filled == 0) {
                USB_TxBuffer.Filled = 1;
                USB_RxBuffer.Filled = 0;
            }
        }
        if (USB_TxBuffer.Filled == 1)
            USB_SendTxBuffer(&USB_TxBuffer);
        if(ADC_flag){
            ADC_Start(adc_channel);
            ADC_flag = 0;
        }
        MCU_USB_UpdateConnection();
        State_WS_STR = Change_WS_STR(Enable_WD_STR,State_WS_STR);
        Set_LED(Enb_SW_RK);
        Set_UP_SW(Enb_SW_RK);
        bstate = Monitor_BEAM(bstate);
        int_led();
        Uart_Send(uart_ctrl, Get_EN_TEST(), bstate);
        auto_ctrl(auto_ctrl_2, Get_EN_HFS());
        ARINC_Send_auto(arinc_auto_send_config, course_time, recognize_time, roll_time);
        SetLEDs(LED_flag);
    }
}
void _arinc_data(uint8_t comand_name){
    uint8_t j;
    if (USB_RxBuffer.DataLen == 1) {
        if(USB_RxBuffer.Data[0] == 1) {
            USB_TxBuffer.Data[0] = (uint8_t)(arinc_data_count1[comand_name]);
            USB_TxBuffer.Data[1] = (uint8_t)(arinc_data_count1[comand_name] >> 8);
            for(j = 0; j < 4; j++) {
                USB_TxBuffer.Data[2 + j] = (uint8_t)(arinc_data1[comand_name]>>(8*j));
                USB_TxBuffer.Data[6 + j] = (uint8_t)(arinc_time1[comand_name][1]>>(8 * j));
            }
            USB_TxBuffer.DataLen = 10;
            USB_TxBuffer.Status = USB_CMD_DONE;
        }
        else if(USB_RxBuffer.Data[0] == 2) {
            USB_TxBuffer.Data[0] = (uint8_t)(arinc_data_count2[comand_name]);
            USB_TxBuffer.Data[1] = (uint8_t)(arinc_data_count2[comand_name] >> 8);
            for(j = 0; j < 4; j++) {
                USB_TxBuffer.Data[2 + j] = (uint8_t)(arinc_data2[comand_name]>>(8*j));
                USB_TxBuffer.Data[6 + j] = (uint8_t)(arinc_time2[comand_name][1]>>(8 * j));
            }
            USB_TxBuffer.DataLen = 10;
            USB_TxBuffer.Status = USB_CMD_DONE;
        }
        else {
            USB_TxBuffer.Status = USB_CMD_ERROR;
            USB_TxBuffer.DataLen = 0;
        }
    }
    else {
        USB_TxBuffer.Status = USB_CMD_ERROR;
        USB_TxBuffer.DataLen = 0;
    }
}
void _ad_all(uint8_t comand_name, uint8_t buff_number, uint8_t channel_name){
    uint8_t j;
    if(channel_name == 1) {
        USB_TxBuffer.Data[buff_number] = (uint8_t)(arinc_data_count1[comand_name]);
        USB_TxBuffer.Data[buff_number + 1] = (uint8_t)(arinc_data_count1[comand_name] >> 8);
        for(j = 0; j < 4; j++) {
            USB_TxBuffer.Data[buff_number + j + 2] = (uint8_t)(arinc_data1[comand_name]>>(8*j));
            USB_TxBuffer.Data[buff_number + j + 6] = (uint8_t)(arinc_time1[comand_name][1]>>(8 * j));
        }
    }
    else if(channel_name == 2) {
        USB_TxBuffer.Data[buff_number] = (uint8_t)(arinc_data_count2[comand_name]);
        USB_TxBuffer.Data[buff_number + 1] = (uint8_t)(arinc_data_count2[comand_name] >> 8);
        for(j = 0; j < 4; j++) {
            USB_TxBuffer.Data[buff_number + j + 2] = (uint8_t)(arinc_data2[comand_name]>>(8*j));
            USB_TxBuffer.Data[buff_number + j + 6] = (uint8_t)(arinc_time2[comand_name][1]>>(8 * j));
        }
    }
}
void int_led(void) {
    uint32_t func_timer = MDR_TIMER2->CNT;
    //Eternet Leds
    if(led_eth_flag)
    {
        timer_eth = func_timer;
        Set_Led_Eth();
        led_eth_flag = 0;
    }
    if((long int)func_timer - (long int)timer_eth > 5000 || (long int)timer_eth - (long int)func_timer> 5000)
        Reset_Led_Eth();
    //Arinc Input
    if(led_ari_flag)
    {
        timer_ari = func_timer;
        Set_Led_Ari();
        led_ari_flag = 0;
    }
    if((long int)func_timer - (long int)timer_ari > 10000 || (long int)timer_ari - (long int)func_timer> 10000)
        Reset_Led_Ari();
    //Arinc output
    if(led_aro_flag)
    {
        timer_aro = func_timer;
        Set_Led_Aro();
        led_aro_flag = 0;
    }
    if((long int)func_timer - (long int)timer_aro > 10000 || (long int)timer_aro - (long int)func_timer> 10000)
        Reset_Led_Aro();
    //Usb
    if(led_usb_flag)
    {
        timer_usb = func_timer;
        Set_Led_USB();
        led_usb_flag = 0;
    }
    if((long int)func_timer - (long int)timer_usb > 5000 || (long int)timer_usb - (long int)func_timer> 5000)
        Reset_Led_USB();
}
void SetLEDs (uint8_t enable) {
    if(enable) {
        uint32_t func_timer = MDR_TIMER2->CNT;
        Set_Led_Eth();
        Set_Led_Ari();
        Set_Led_Aro();
        Set_Led_USB();
        PORT_SetBits(MDR_PORTA, PORT_Pin_13);
        PORT_SetBits(MDR_PORTA, PORT_Pin_14);
        PORT_SetBits(MDR_PORTA, PORT_Pin_15);
        PORT_SetBits(MDR_PORTC, PORT_Pin_9);
        PORT_SetBits(MDR_PORTC, PORT_Pin_10);
        PORT_SetBits(MDR_PORTC, PORT_Pin_11);
        PORT_SetBits(MDR_PORTA, PORT_Pin_12);
        while(MDR_TIMER2->CNT - func_timer < 2000000) {
            State_WS_STR = Change_WS_STR(Enable_WD_STR,State_WS_STR);
        }
        Reset_Led_Eth();
        Reset_Led_Ari();
        Reset_Led_Aro();
        Reset_Led_USB();
        PORT_ResetBits(MDR_PORTA, PORT_Pin_13);
        PORT_ResetBits(MDR_PORTA, PORT_Pin_14);
        PORT_ResetBits(MDR_PORTA, PORT_Pin_15);
        PORT_ResetBits(MDR_PORTC, PORT_Pin_9);
        PORT_ResetBits(MDR_PORTC, PORT_Pin_10);
        PORT_ResetBits(MDR_PORTC, PORT_Pin_11);
        PORT_ResetBits(MDR_PORTA, PORT_Pin_12);
        func_timer = MDR_TIMER2->CNT;
        while(MDR_TIMER2->CNT - func_timer < 2000000) {
            State_WS_STR = Change_WS_STR(Enable_WD_STR,State_WS_STR);
        }
        LED_flag = 0;
    }
}
void ARINC_Send_auto(uint8_t control, uint32_t course_time, uint32_t recognize_time, uint32_t roll_time) {
    uint32_t func_timer = MDR_TIMER2->CNT;
    if(control) {
        if((func_timer - timer_course) > course_time) {
            ArincSendDataCH1(arinc_course);
            timer_course = func_timer;
        }
        if((func_timer - timer_recognize) > recognize_time) {
            ArincSendDataCH1(arinc_recognize);
            timer_recognize = func_timer;
        }
        if((func_timer - timer_roll) > roll_time) {
            ArincSendDataCH1(arinc_roll);
            timer_roll = func_timer;
        }
    }
}
