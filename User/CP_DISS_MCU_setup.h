#ifndef __CP_DISS_MCU_SETUP_H
#define __CP_DISS_MCU_SETUP_H

#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_usb_device.h"
typedef enum
{
   USB_MPR_ERROR = 0x33,
   USB_ETH_ERROR = 0x66,
   USB_CMD_DONE = 0x55,
   USB_CMD_UNKNOWN = 0x44,
   USB_CMD_ERROR = 0x40,
   USB_CMD_CHECKSUM_ERROR = 0x30,
   USB_ETH_NOT_RECIVE = 0x22,
   USB_ETH_NO_REQUEST = 0x77,
}USB_CmdStatus_TypeDef;

typedef struct
{
   uint8_t Command;
   uint8_t Data[62]; // Поле данных не может быть больше 62 байт, т.к. физичиский буфер USB имеет размер 64 байта (by Maksim Kuznetsov)
   uint8_t ChkSum;
   uint32_t DataLen;
   uint8_t Filled;
   uint8_t Valid;
}USB_RxBuffer_TypeDef;

typedef struct
{
   uint8_t Command;  
   USB_CmdStatus_TypeDef Status;     
   uint8_t Data[1024+2];
   uint8_t ChkSum;
   uint32_t DataLen;
   uint8_t Filled;
}USB_TxBuffer_TypeDef;

void MCU_Setup(void);
void MCU_HSE_Setup(void);
void MCU_USB_Setup(void);
void USB_RxBuffer_Reset(USB_RxBuffer_TypeDef* USB_RxBuffer);

#define     SET_PWR                             (0x01)
#define     GET_PWR                             (0x02)
#define     CMD_GET_PG                          (0x03)
#define     SET_UPR_4RK                         (0x04)
#define     GET_UPR_4RK                         (0x05)
#define     GET_IND_4RK                         (0x06)
#define     SET_UPR_9RK                         (0x07)
#define     GET_UPR_9RK                         (0x08)
#define     GET_IND_9RK                         (0x09)
#define     SET_UPR_REF_12                      (0x0A)
#define     GET_UPR_REF_12                      (0x0B)

#define     GET_ARINC_WX                        (0x10)
#define     SET_ARINC_DATA_BUFF                 (0x11)
#define     GET_ARINC_WY                        (0x12)
#define     GET_ARINC_DATA_BUFF                 (0x13)
#define     GET_ARINC_WZ                        (0x14)
#define     SET_ARINC_AUTO_SEND                 (0x15)
#define     GET_ARINC_MODE                      (0x16)
#define     GET_ARINC_AUTO_SEND                 (0x17)
#define     GET_ARINC_STATUS                    (0x18)

#define     GET_ARINC_TIME                      (0x1A)
#define     GET_ARINC_VERS_MC                   (0x1B)
#define     SET_ARINC_SPEED                     (0x1C)
#define     SEND_ARINC_CH1                      (0x1D)
#define     SEND_ARINC_CH2                      (0x1E)
#define     SEND_ALL_ARINC                      (0x1F)
#define     GET_BEAM                            (0x20)
#define     SET_LVTTL                           (0x21)
#define     GET_LVTTL                           (0x22)
#define     GET_IN_LVTTL                        (0x23)
#define     GET_ARINC_CRC_MC_FIRST_PART         (0x24)
#define     GET_ARINC_CRC_MC_SECOND_PART        (0x25)
#define     GET_ARINC_VERS_SHF                  (0x26)
#define     GET_ARINC_CRC_SHF_FIRST_PART        (0x27)
#define     GET_ARINC_CRC_SHF_SECOND_PART       (0x28)
#define     GET_ARINC_VERS_MC3                  (0x29)
#define     GET_ARINC_CRC_MC3_FIRST_PART        (0x2A)
#define     GET_ARINC_CRC_MC3_SECOND_PART       (0x2B)
#define     GET_ARINC_SPEED                     (0x2C)
#define     GET_ARINC_ERRORS                    (0x2D)
#define     GET_SINHRO_BEAM_MPR                 (0x2E)
#define     GET_SINHRO_BEAM_DISS                (0x2F)
#define     ADC_START                           (0x30)
#define     ADC_RESULT                          (0x31)
#define     SET_MULT                            (0x32)
#define     GET_MULT                            (0x33)
#define     SET_AUTO_CTRL2                      (0x34)
#define     GET_AUTO_CTRL2                      (0x35)

#define     MAN_POS                             (0x40)                      //позиция галетника авто/ручн
#define     SET_SW                              (0x41)                      //включить управление РК с лицевой панели
#define     GET_SW                              (0x42)                      //отключить управление РК с лицевой панели
#define     RESET_INPUT                         (0x43)
#define     RESET_OUTPUT                        (0x44)
#define     WRITE_ETH_MASS                      (0x45)
#define     READ_ETH_MASS                       (0x46)
#define     UART_WRITE_MASS                     (0x47)
#define     UART_READ_MASS                      (0x48)
#define     SET_UART_CONTROL                    (0x49)
#define     GET_UART_CONTROL                    (0x4A)
#define     SET_LEDS                            (0x4B)

#define     MIN_ETH_COMAND_REQUEST              (0xA0)
#define     COEFF_SET                           (0xC2)                      //Отсылка массива на МПР для записи кэфф. АЧХ
#define     MIN_USB_ANSWER                      (0xD0)

#define     COUNT_MPR_COMAND                    (MIN_USB_ANSWER - MIN_ETH_COMAND_REQUEST)

#define     SPEED_TEST                          (0x71)
#define     SEND_ETH                            (0x80)
#define     READ_ETH                            (0x81)

USB_Result USB_SendTxBuffer(USB_TxBuffer_TypeDef* TxBuffer);
void MCU_USB_UpdateConnection(void);
uint8_t USB_ChkSum(uint8_t* Data, uint32_t Length);
void USB_memcpy(uint8_t* Source, uint8_t* Destination, uint32_t Length);
void USB_LongCmdEnable(void);
void USB_ShortCmdEnable(void);
void USB_ForceShortCmdEnable(void);
void MCU_Port_Setup(void);
void MCU_ADC_Clock_Setup(void);
#endif
