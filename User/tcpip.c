/******************************************************************
 *****                                                        *****
 *****  Name: tcpip.c                                         *****
 *****  Ver.: 1.0                                             *****
 *****  Date: 07/05/2001                                      *****
 *****  Auth: Andreas Dannenberg                              *****
 *****        HTWK Leipzig                                    *****
 *****        university of applied sciences                  *****
 *****        Germany                                         *****
 *****  Func: implements the TCP/IP-stack and provides a      *****
 *****        simple API to the user                          *****
 *****                                                        *****
 ******************************************************************/

/** @addtogroup __MDR32F9Qx_StdPeriph_Examples MDR32F9Qx StdPeriph Examples
  * @{
  */

/** @addtogroup __MDR1986VE1T_EVAL MDR1986VE1T Opora Evaluation Board
  * @{
  */

/** @addtogroup Ethernet_IT Ethernet
  * @{
  */

#include "tcpip.h"

#define DEBUG_PRINTF(...) printf(__VA_ARGS__)

/* Initalizes the LAN-controller, reset flags, starts timer-ISR */
#define DATA_SIZE 8296
#define DATA_LEN (int)DATA_SIZE/6
#define DATA_LEN5 DATA_SIZE - DATA_LEN*5
#define MAX_TCP_DATA_SIZE 1460

/* Global vars and flags */
uint32_t TCPRxDataCount = 0;              // nr. of bytes rec'd
uint32_t TCPTxDataCount = 0;              // nr. of bytes to send
uint16_t Ident = 1;

/* Pointer to output TCP data */
uint8_t *pData = NULL;
uint8_t led_eth_flag = 0;
/* TCP ports */
uint16_t TCPLocalPort = 0;
uint16_t TCPRemotePort = 23;

/* MAC and IP of current TCP-session */
uint16_t RemoteMAC[3] = {0,0,0};
uint16_t RemoteIP[2] = {0,0};

/* The socket status */
uint8_t SocketStatus;

/* Internal variables */
TTCPStateMachine TCPStateMachine;         	// perhaps the most important var at all ;-)
TLastFrameSent LastFrameSent;             	// retransmission type

uint32_t ISNGenHigh = 0;                  	// upper word of our Initial Sequence Number
uint32_t TCPSeqNr = 0;                   	// next sequence number to send
uint32_t TCPUNASeqNr = 0;					// last unacknowledged sequence number
											// incremented AFTER sending data
uint32_t TCPAckNr = 0;                   	// next seq to receive and ack to send
                                            // incremented AFTER receiving data
uint8_t TCPTimer = 0;                   	// inc'd each 262ms
uint8_t RetryCounter = 0;               	// nr. of retransmissions

/* Properties of the just received frame */
uint32_t RecdFrameLength = 0;             	// CS8900 reported frame length
uint16_t RecdFrameMAC[3] = {0,0,0};     	// 48 bit MAC
uint16_t RecdFrameIP[2] = {0,0};        	// 32 bit IP
uint16_t RecdIPFrameLength = 0;         	// 16 bit IP packet length

/* The next 3 buffers must be word-aligned!*/
/* (here the 'RecdIPFrameLength' above does that)*/
uint8_t TxFrame1[ETH_CTRL_FIELD + ETH_HEADER_SIZE + IP_HEADER_SIZE + TCP_HEADER_SIZE + MAX_TCP_TX_DATA_SIZE] IAR_SECTION ("EXECUTABLE_MEMORY_SECTION") __attribute__((section("EXECUTABLE_MEMORY_SECTION"))) __attribute__ ((aligned (4)));
uint8_t TxFrame2[ETH_CTRL_FIELD + ETH_HEADER_SIZE + MAX_ETH_TX_DATA_SIZE] 									 IAR_SECTION ("EXECUTABLE_MEMORY_SECTION") __attribute__((section("EXECUTABLE_MEMORY_SECTION"))) __attribute__ ((aligned (4)));
// space for incoming packet
uint32_t InputFrame[1514/4] 																				 IAR_SECTION ("EXECUTABLE_MEMORY_SECTION") __attribute__((section("EXECUTABLE_MEMORY_SECTION"))) __attribute__ ((aligned (4)));

uint32_t TxFrame1Size = 0;                // bytes to send in TxFrame1
uint32_t TxFrame2Size = 0;                // bytes to send in TxFrame2
uint8_t TransmitControl = 0;
//For udp_send
extern uint16_t ToIP[];
uint8_t eth_data[1024 + 2];
uint16_t leneth;
uint8_t ethcount = 0;

/* Handles an incoming frame  */
pEthernetARP_Answer  EthernetARP_Answer;
pEthernetIP_Frame    EthernetIP_Frame;
pEthernetTCPIP_Frame EthernetTCPIP_Frame;

pEthernetUDP_Frame EthernetUDP_Frame;

pARPHead pARP;
uint8_t TCPFlags;

/* "MYIP1.MYIP2.MYIP3.MYIP4" */
uint16_t MyIP[] =
{
  MYIP_1 + (MYIP_2 << 8),
  MYIP_3 + (MYIP_4 << 8)
};
/* "TOIP1.TOIP2.TOIP3.TOIP4" */
uint16_t ToIP[] =
{
  TOIP_1 + (TOIP_2 << 8),
  TOIP_3 + (TOIP_4 << 8)
};
/* "SUBMASK1.SUBMASK2.SUBMASK3.SUBMASK4" */
const uint16_t SubnetMask[] =
{
  SUBMASK_1 + (SUBMASK_2 << 8),
  SUBMASK_3 + (SUBMASK_4 << 8)
};

/* "GWIP1.GWIP2.GWIP3.GWIP4" */
const uint16_t GatewayIP[] =
{
  GWIP_1 + (GWIP_2 << 8),
  GWIP_3 + (GWIP_4 << 8)
};


/**
  * @brief	Function copy data from received frame to destenation array.
  * @param	dest: pointer to dest array.
  * @param	src: pointer to src array.
  * @param	size: number elements to copt.
  * @retval None
  */
void CopyFromFrame(void * dest , uint16_t * src, uint32_t size){
    uint16_t * pDest	= (uint16_t * )dest;
    uint16_t * psrc 	= (uint16_t * )src;
    uint32_t   i = 0;
    while (size > 1) {
    	pDest[i] = psrc[i];
    	i++;
    	size -= 2;
    }
    if (size)                                      	// check for leftover byte...
    	*(uint8_t *)&pDest[i] =  *(uint8_t *)&psrc[i];  	// the LAN-Controller will return 0
}

/**
  * @brief	Main network function. must be called from user program
  * 		periodically (the often - the better) handles network,
  * 		TCP/IP-stack and user events.
  * @param	ETHERNETx: Slect the ETHERNET peripheral.
  *         This parameter can be one of the following values:
  *         MDR_ETHERNET1, MDR_ETHERNET2
  * @retval	None
  */
void DoNetworkStuff(MDR_ETHERNET_TypeDef * ETHERNETx)
{
    led_eth_flag = 1;
	if (TransmitControl & SEND_FRAME2) {
		ETH_SendFrame(ETHERNETx, (uint32_t *) TxFrame2, *(uint32_t*)&TxFrame2[0]);
		TransmitControl &= ~SEND_FRAME2;
	}

	if (TransmitControl & SEND_FRAME1) {
		//PrepareTCP_DATA_FRAME();
		ETH_SendFrame(ETHERNETx, (uint32_t *) TxFrame1, *(uint32_t*)&TxFrame1[0]);
		TransmitControl &= ~SEND_FRAME1;
	}
}

/**
  * @brief	Handles an incoming broadcast frame.
  * @param	ptr_InputBuffer: pointer to incuming frame.
  * @param 	FrameLen: size of the incuming frame.
  * @retval	None
  */
void ProcessEthBroadcastFrame(uint32_t * ptr_InputBuffer, uint32_t FrameLen  )
{
    uint16_t TargetIP[2];

    pARP = (pARPHead)ptr_InputBuffer;
    CopyFromFrame(&RecdFrameMAC,pARP->SrcAddr, 6);      						// store SA (for our answer)
    /* Check for ARP protocol */
    if (SWAPB(pARP->FrameType) == FRAME_ARP)
        if (SWAPB(pARP->HardwareType) == HARDW_ETH10)
            if (SWAPB(pARP->ProtocolType) == FRAME_IP)
                if (SWAPB(pARP-> HardSizeProtSize) == IP_HLEN_PLEN)
                    if (SWAPB(pARP->Opcode) == OP_ARP_REQUEST){
                        CopyFromFrame(RecdFrameIP, pARP->SenderIPAddr, 4);     	// read sender's protocol address
                        CopyFromFrame(TargetIP,    pARP->TargetIPAddr, 4);      // read target's protocol address
                        if (!memcmp(MyIP, TargetIP, 4)){                      	// is it for us?
                            PrepareARP_ANSWER();                                // yes->create ARP_ANSWER frame
                        }
                    }
}

/**
  * @brief	Handles an incoming unicast frame.
  * @param	ptr_InputBuffer: pointer to incuming frame.
  * @param 	FrameLen: size of the incuming frame.
  * @retval
  */
void ProcessEthIAFrame(uint32_t * ptr_InputBuffer, uint32_t FrameLen  )
{
	uint16_t TargetIP[2];
	uint8_t ProtocolType;

	EthernetARP_Answer = (pEthernetARP_Answer)ptr_InputBuffer;
	EthernetIP_Frame = (pEthernetIP_Frame)ptr_InputBuffer;
	EthernetTCPIP_Frame = (pEthernetTCPIP_Frame)ptr_InputBuffer;
	EthernetUDP_Frame = (pEthernetUDP_Frame)ptr_InputBuffer;	// my UDP
  pARP = (pARPHead)ptr_InputBuffer;
	CopyFromFrame(RecdFrameMAC, EthernetARP_Answer->SrcAddr, 6);            	// store SA (for our answer)

	switch (SWAPB(EthernetARP_Answer->FrameType)){                     			// get frame type
	case FRAME_ARP :                                                      		// check for ARP
			if ((TCPFlags & (TCP_ACTIVE_OPEN | IP_ADDR_RESOLVED)) == TCP_ACTIVE_OPEN){
				if (SWAPB(EthernetARP_Answer->HardwareType) == HARDW_ETH10)
					if (SWAPB(EthernetARP_Answer->ProtocolType) == FRAME_IP)
						if (SWAPB(EthernetARP_Answer->HardSizeProtSize) == IP_HLEN_PLEN)
							if (SWAPB(EthernetARP_Answer->Opcode) == OP_ARP_ANSWER) {
								TCPStopTimer();  // OK, now we've the MAC we wanted ;-)
								CopyFromFrame(RemoteMAC, EthernetARP_Answer->SenderMacAddr, 6);  // extract opponents MAC
								TCPFlags |= IP_ADDR_RESOLVED;
							}
				}
				else{
					if (SWAPB(pARP->HardwareType) == HARDW_ETH10){
						if (SWAPB(pARP->ProtocolType) == FRAME_IP)
							if (SWAPB(pARP-> HardSizeProtSize) == IP_HLEN_PLEN)
								if (SWAPB(pARP->Opcode) == OP_ARP_REQUEST){
									CopyFromFrame(RecdFrameIP, pARP->SenderIPAddr, 4);     	// read sender's protocol address
									CopyFromFrame(TargetIP,    pARP->TargetIPAddr, 4);      // read target's protocol address
									if (!memcmp(MyIP, TargetIP, 4)){                      	// is it for us?
										PrepareARP_ANSWER();                                // yes->create ARP_ANSWER frame
									}
								}
					}
				}
	break;
	case FRAME_IP:                                      						// check for IP-type
		if ((SWAPB(EthernetIP_Frame->IPVerIHL) & 0xFF00) == IP_VER_IHL) {      	// IPv4, IHL=5 (20 Bytes Header)	// ignore Type Of Service
			RecdIPFrameLength = SWAPB(EthernetIP_Frame->FrameLength); 	 	  	// get IP frame's length 			// ignore identification
			if (!(SWAPB(EthernetIP_Frame->Flags_Offset) & (IP_FLAG_MOREFRAG | IP_FRAGOFS_MASK))) {  				// only unfragm. frames
				ProtocolType = SWAPB(EthernetIP_Frame->ProtocolType_TTL);  		// get protocol, ignore TTL		 	// ignore checksum
				CopyFromFrame(RecdFrameIP, EthernetIP_Frame->SenderIPAddr, 4);  // get source IP
				CopyFromFrame(TargetIP, EthernetIP_Frame->TargetIPAddr, 4);     // get destination IP
				if (!memcmp(MyIP, TargetIP, 4))           						// is it for us?
					switch (ProtocolType) {
						case PROT_ICMP:
							if(ProcessICMPFrame())
							break;
						case PROT_UDP:
							udp_proceed();
							break;                           						// not implemented!
					}
			}
		}
		break;
	}
}

/**
  * @brief	We've just rec'd an ICMP-frame (Internet Control Message Protocol)
  * 		check what to do and branch to the appropriate sub-function.
  * @param	None
  * @retval	None
  */
uint32_t ProcessICMPFrame ( void )
{

	unsigned short ICMPTypeAndCode;
	ICMPTypeAndCode = SWAPB(EthernetIP_Frame->Type_and_Code);  // get Message Type and Code
															   // ignore ICMP checksum
	switch (ICMPTypeAndCode >> 8) {                // check type
		case ICMP_ECHO:                             // is echo request?
			PrepareICMP_ECHO_REPLY();               // echo as much as we can...
			return (1);
//		break;
	}
	return (0);
}

/**
  * @brief	Prepares the TxFrame2-buffer to send an ARP-request.
  * @param	None
  * @retval	None
  */
void PrepareARP_REQUEST(void)
{
	uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame2[4];

	// Ethernet
	*(uint32_t *) &ptr_OutFrame[0] = (((((uint8_t) (RecdFrameMAC[1] >> 8) & 0xFF) << 24)
			                       | (((uint8_t) RecdFrameMAC[1] & 0xFF) << 16)
			                       | (((uint8_t) (RecdFrameMAC[0] >> 8) & 0xFF) << 8)
			                       | ((uint8_t) RecdFrameMAC[0] & 0xFF)));

	*(uint16_t *) &ptr_OutFrame[4] = ((((uint8_t) (RecdFrameMAC[2] >> 8) & 0xFF) << 8)
							       | ((uint8_t) RecdFrameMAC[2] & 0xFF));
	/* Set our MAC address */
	ptr_OutFrame[6] = MAC_5;
	ptr_OutFrame[7] = MAC_4;
	ptr_OutFrame[8] = MAC_3;
	ptr_OutFrame[9] = MAC_2;
	ptr_OutFrame[10] = MAC_1;
	ptr_OutFrame[11] = MAC_0;

	// ARP
	*(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]		= SWAPB(FRAME_ARP);
	*(uint16_t *) &TxFrame2[ARP_HARDW_OFS] 			= SWAPB(HARDW_ETH10);
	*(uint16_t *) &TxFrame2[ARP_PROT_OFS] 			= SWAPB(FRAME_IP);
	*(uint16_t *) &TxFrame2[ARP_HLEN_PLEN_OFS] 		= SWAPB(IP_HLEN_PLEN);
	*(uint16_t *) &TxFrame2[ARP_OPCODE_OFS] 		= SWAPB(OP_ARP_REQUEST);
	*(uint8_t *) &TxFrame2[ARP_SENDER_HA_OFS + 0] 	= MAC_5;
	*(uint8_t *) &TxFrame2[ARP_SENDER_HA_OFS + 1] 	= MAC_4;
	*(uint8_t *) &TxFrame2[ARP_SENDER_HA_OFS + 2] 	= MAC_3;
	*(uint8_t *) &TxFrame2[ARP_SENDER_HA_OFS + 3] 	= MAC_2;
	*(uint8_t *) &TxFrame2[ARP_SENDER_HA_OFS + 4] 	= MAC_1;
	*(uint8_t *) &TxFrame2[ARP_SENDER_HA_OFS + 5] 	= MAC_0;

	memcpy(&TxFrame2[ARP_SENDER_IP_OFS], MyIP, 4);
	memset(&TxFrame2[ARP_TARGET_HA_OFS], 0x00, 6);  // we don't know opposites MAC!

	if (((RemoteIP[0] ^ MyIP[0]) & SubnetMask[0]) || ((RemoteIP[1] ^ MyIP[1]) & SubnetMask[1]))
		memcpy(&TxFrame2[ARP_TARGET_IP_OFS], GatewayIP, 4);  // IP not in subnet, use gateway
	else
		memcpy(&TxFrame2[ARP_TARGET_IP_OFS], RemoteIP, 4);  // other IP is next to us...

	/* Set the length of the send frame */
	*(uint32_t *)&TxFrame2[0] = (ARP_FRAME_SIZE + ETH_HEADER_SIZE);
	TransmitControl |= SEND_FRAME2;
}

/**
  * @brief	Prepares the TxFrame2-buffer to send an ARP-answer (reply).
  * @param	None
  * @retval	None
  */
void PrepareARP_ANSWER(void)
{
	uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame2[4];
	/* Ethernet */
	/* Set destanation MAC address */
	*(uint32_t *) &ptr_OutFrame[0] = (((((uint8_t) (RecdFrameMAC[1] >> 8) & 0xFF) << 24)
			                       | (((uint8_t) RecdFrameMAC[1] & 0xFF) << 16)
			                       | (((uint8_t) (RecdFrameMAC[0] >> 8) & 0xFF) << 8)
			                       | ((uint8_t) RecdFrameMAC[0] & 0xFF)));

	*(uint16_t *) &ptr_OutFrame[4] = ((((uint8_t) (RecdFrameMAC[2] >> 8) & 0xFF) << 8)
							       | ((uint8_t) RecdFrameMAC[2] & 0xFF));

	/* Set our MAC address */
	ptr_OutFrame[6] = MAC_5;
	ptr_OutFrame[7] = MAC_4;
	ptr_OutFrame[8] = MAC_3;
	ptr_OutFrame[9] = MAC_2;
	ptr_OutFrame[10] = MAC_1;
	ptr_OutFrame[11] = MAC_0;

	// ARP
	*(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]			= SWAPB(FRAME_ARP);
	*(uint16_t *)&ptr_OutFrame[ARP_HARDW_OFS] 			= SWAPB(HARDW_ETH10);
	*(uint16_t *)&ptr_OutFrame[ARP_PROT_OFS] 			= SWAPB(FRAME_IP);
	*(uint16_t *)&ptr_OutFrame[ARP_HLEN_PLEN_OFS] 		= SWAPB(IP_HLEN_PLEN);
	*(uint16_t *)&ptr_OutFrame[ARP_OPCODE_OFS] 			= SWAPB(OP_ARP_ANSWER);
	*(uint8_t *) &ptr_OutFrame[ARP_SENDER_HA_OFS + 0] 	= MAC_5;
	*(uint8_t *) &ptr_OutFrame[ARP_SENDER_HA_OFS + 1] 	= MAC_4;
	*(uint8_t *) &ptr_OutFrame[ARP_SENDER_HA_OFS + 2] 	= MAC_3;
	*(uint8_t *) &ptr_OutFrame[ARP_SENDER_HA_OFS + 3] 	= MAC_2;
	*(uint8_t *) &ptr_OutFrame[ARP_SENDER_HA_OFS + 4] 	= MAC_1;
	*(uint8_t *) &ptr_OutFrame[ARP_SENDER_HA_OFS + 5] 	= MAC_0;

	/* IP */
	memcpy(&ptr_OutFrame[ARP_SENDER_IP_OFS], &MyIP, 4);
	memcpy(&ptr_OutFrame[ARP_TARGET_HA_OFS], &RecdFrameMAC, 6);
	memcpy(&ptr_OutFrame[ARP_TARGET_IP_OFS], &RecdFrameIP, 4);

	/* Set the length of the send frame */
	*(uint32_t *)&TxFrame2[0] = (ARP_FRAME_SIZE + ETH_HEADER_SIZE);
	TransmitControl |= SEND_FRAME2;
}

/**
  * @brief	Prepares the TxFrame2-buffer to send an ICMP-echo-reply
  * @param	None
  * @retval	None
  */
void PrepareICMP_ECHO_REPLY(void)
{
	uint16_t ICMPDataCount;
	uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame2[4];

	if (RecdIPFrameLength > MAX_ETH_TX_DATA_SIZE)    // don't overload TX-buffer
		ICMPDataCount = MAX_ETH_TX_DATA_SIZE - IP_HEADER_SIZE - ICMP_HEADER_SIZE;
	else
		ICMPDataCount = RecdIPFrameLength - IP_HEADER_SIZE - ICMP_HEADER_SIZE;

	TxFrame2Size = IP_HEADER_SIZE + ICMP_HEADER_SIZE + ICMPDataCount;

	// Ethernet
	/* Set destanation MAC address */
	*(uint32_t *) &ptr_OutFrame[0] = (((((uint8_t) (RecdFrameMAC[1] >> 8) & 0xFF) << 24)
			                       | (((uint8_t) RecdFrameMAC[1] & 0xFF) << 16)
			                       | (((uint8_t) (RecdFrameMAC[0] >> 8) & 0xFF) << 8)
			                       | ((uint8_t) RecdFrameMAC[0] & 0xFF)));

	*(uint16_t *) &ptr_OutFrame[4] = ((((uint8_t) (RecdFrameMAC[2] >> 8) & 0xFF) << 8)
							       | ((uint8_t) RecdFrameMAC[2] & 0xFF));

	/* Set our MAC address */
	ptr_OutFrame[6] = MAC_5;
	ptr_OutFrame[7] = MAC_4;
	ptr_OutFrame[8] = MAC_3;
	ptr_OutFrame[9] = MAC_2;
	ptr_OutFrame[10] = MAC_1;
	ptr_OutFrame[11] = MAC_0;

	// IP
	*(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]			= SWAPB(FRAME_IP);
	*(uint16_t *)&ptr_OutFrame[IP_VER_IHL_TOS_OFS] 		= SWAPB(IP_VER_IHL);
	WriteWBE(&ptr_OutFrame[IP_TOTAL_LENGTH_OFS], IP_HEADER_SIZE + ICMP_HEADER_SIZE + ICMPDataCount);
	*(uint16_t *)&ptr_OutFrame[IP_IDENT_OFS] 			= EthernetIP_Frame->Identification;//PROT_ICMP;
	*(uint16_t *)&ptr_OutFrame[IP_FLAGS_FRAG_OFS] 		= 0x00;
	*(uint16_t *)&ptr_OutFrame[IP_TTL_PROT_OFS] 		= SWAPB((DEFAULT_TTL << 8) | PROT_ICMP);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;
	memcpy(&ptr_OutFrame[IP_SOURCE_OFS], MyIP, 4);
	memcpy(&ptr_OutFrame[IP_DESTINATION_OFS], RecdFrameIP, 4);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= CalcChecksum(&ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE, 0);

	// ICMP
	*(uint16_t *)&ptr_OutFrame[ICMP_TYPE_CODE_OFS]		= SWAPB(ICMP_ECHO_REPLY << 8);
	*(uint16_t *)&ptr_OutFrame[ICMP_CHKSUM_OFS] 		= 0;                   // initialize checksum field
	*(uint16_t *)&ptr_OutFrame[ICMP_ID]			 		= EthernetIP_Frame->Identifier;
	*(uint16_t *)&ptr_OutFrame[ICMP_SEQ_NUM] 			= EthernetIP_Frame->Sequence_number;

	/* Compute chech sum */
	CopyFromFrame(&ptr_OutFrame[ICMP_DATA_OFS],&EthernetIP_Frame->Data, ICMPDataCount);        // get data to echo...
	*(uint16_t *)&ptr_OutFrame[ICMP_CHKSUM_OFS] = standard_chksum_opt(&ptr_OutFrame[IP_DATA_OFS], ICMPDataCount + ICMP_HEADER_SIZE, 0);

	/* Set the length of the send frame */
	*(uint32_t*)&TxFrame2[0] = ETH_HEADER_SIZE + IP_HEADER_SIZE + ICMP_HEADER_SIZE + ICMPDataCount;
	TransmitControl |= SEND_FRAME2;
}

/**
  * @brief	Calculates the TCP/IP checksum. if 'IsTCP != 0', the TCP pseudo-header
  * 		will be included.
  * @param	Start: pointer to input array.
  * @param	Count: number of the half-word in the input array.
  * @parma	IsTCP: a sign, which requires TCP checksum.
  * @retval	Checksum.
  */
uint16_t CalcChecksum(void * Start, uint16_t Count, uint16_t IsTCP)
{
  unsigned long Sum = 0;
  unsigned short * piStart;

  if (IsTCP) {                                   // if we've a TCP frame...
    Sum += MyIP[0];                              // ...include TCP pseudo-header
    Sum += MyIP[1];
    Sum += RemoteIP[0];
    Sum += RemoteIP[1];
    Sum += SwapBytes(Count);                     // TCP header length plus data length
    Sum += SWAPB(PROT_TCP);
  }

  piStart = Start;
  while (Count > 1) {                            // sum words
    Sum += *piStart++;
    Count -= 2;
  }

  if (Count)                                     // add left-over byte, if any
    Sum += *(unsigned char *)piStart;

  while (Sum >> 16)                              // fold 32-bit sum to 16 bits
    Sum = (Sum & 0xFFFF) + (Sum >> 16);

  return (~Sum);
}

#define FOLD_U32T(u)          (((u) >> 16) + ((u) & 0x0000ffffUL))
#define SWAP_BYTES_IN_WORD(w) (((w) & 0xff) << 8) | (((w) & 0xff00) >> 8)
/**
  * @brief	An optimized checksum routine. Basically, it uses loop-unrolling on
 * 			the checksum loop, treating the head and tail bytes specially, whereas
 * 			the inner loop acts on 8 bytes at a time.
  * @param	dataptr: start of buffer to be checksummed. May be an odd byte address.
  * @param  len: number of bytes in the buffer to be checksummed.
  * @parma	IsTCP: a sign, which requires TCP checksum.
  * @retval Checksum.
  */
uint16_t standard_chksum_opt(void *dataptr,  uint32_t len, uint8_t IsTCP)
{
	uint8_t *pb = (uint8_t *) dataptr;
	uint16_t *ps, t = 0;
	uint32_t *pl;
	uint32_t sum = 0, tmp;
	/* starts at odd byte address? */
	int32_t odd = ((uint32_t) pb & 1);

	if (IsTCP) {                                   // if we've a TCP frame...
		sum += MyIP[0];                          // ...include TCP pseudo-header
		sum += MyIP[1];
		sum += RemoteIP[0];
		sum += RemoteIP[1];
		sum += SwapBytes(len);            		 // TCP header length plus data length
		sum += SWAPB(PROT_TCP);
	}

	if (odd && len > 0) {
		((uint8_t *) &t)[1] = *pb++;
		len--;
	}

	ps = (uint16_t *) pb;

	if (((uint32_t) ps & 3) && len > 1) {
		sum += *ps++;
		len -= 2;
	}

	pl = (uint32_t *) ps;

	while (len > 7) {
		tmp = sum + *pl++; /* ping */
		if (tmp < sum) {
			tmp++; /* add back carry */
		}

		sum = tmp + *pl++; /* pong */
		if (sum < tmp) {
			sum++; /* add back carry */
		}

		len -= 8;
	}

	/* make room in upper bits */
	sum = FOLD_U32T(sum);

	ps = (uint16_t *) pl;

	/* 16-bit aligned word remaining? */
	while (len > 1) {
		sum += *ps++;
		len -= 2;
	}

	/* dangling tail byte remaining? */
	if (len > 0) { /* include odd byte */
		((uint8_t *) &t)[0] = *(uint8_t *) ps;
	}

	sum += t; /* add end bytes */

	/* Fold 32-bit sum to 16 bits
	 calling this twice is propably faster than if statements... */
	sum = FOLD_U32T(sum);
	sum = FOLD_U32T(sum);

	if (odd) {
		sum = SWAP_BYTES_IN_WORD(sum);
	}

	return ((uint16_t) ~sum);
}

/**
  * @brief	Stopps the timer.
  * @param	None
  * @retval	None
  */
void TCPStopTimer(void)
{
  TCPFlags &= ~TCP_TIMER_RUNNING;
}

/**
  * @brief	Help function to write a WORD in big-endian byte-order to MCU-memory.
  * @param	Add: pointer to memory.
  * @param	Data: input value.
  * @retval	None
  */
void WriteWBE(uint8_t * Add, uint16_t Data)
{
  *Add++ = Data >> 8;
  *Add = Data;
}

/**
  * @brief	help function to write a DWORD in big-endian byte-order
  * 		to MCU-memory.
  * @param	Add: pointer to memory.
  * @param	Data: input value.
  * @retval
  */
void WriteDWBE(uint8_t *Add, uint32_t Data)
{
  *Add++ = Data >> 24;
  *Add++ = Data >> 16;
  *Add++ = Data >> 8;
  *Add = Data;
}

/**
  * @brief	Help function to swap the byte order of a WORD�
  * @param	Data: input value.
  * @retval	swaped data.
  */
uint16_t SwapBytes(uint16_t Data)
{
  return ((Data >> 8) | (Data << 8));
}

/** @} */ /* End of group Ethernet_1T */

/** @} */ /* End of group __MDR1986VE1T_EVAL MDR1986VE1T Opora Evaluation Board */

/** @} */ /* End of group __MDR32F9Qx_StdPeriph_Examples */

///--------------------------------------------------------------------------
///--------------- Protocol UDP Functions -----------------------------------
///--------------------------------------------------------------------------

extern uint8_t fl_telemetry_mode;
uint8_t *udpData;
uint16_t udpDataLen = 0;
uint8_t status[] = {0x0, 0x0, 0x0, 0x0};
uint8_t eth_rx_comand;
//----------------------------------------------------------------------------
// UDP FRAME
// process UDP packet

uint32_t PreCheckSum (uint32_t PreValue, void* Buf, uint16_t Size)
{
  uint16_t Num = Size/2;
  uint16_t i;
  uint32_t cs = PreValue;
  for(i=0; i< 2*Num; i+=2 ) {
    cs += (((uint8_t*)Buf)[i] ) + ((((uint8_t*)Buf)[i+1])<< 8);
  }
  if ( Size > 2*Num ) {
    cs+= ((uint8_t*)Buf)[i];
  }
  return cs;
}

uint16_t CheckSum (uint32_t PreValue, void* Buf, uint16_t Size)
{
  uint16_t Num = Size/2;
  uint16_t i;
  uint32_t cs = PreValue;
  for(i=0; i< 2*Num; i+=2 ) {
    cs+= (((uint8_t*)Buf)[i])  + ((((uint8_t*)Buf)[i+1])<< 8);
  }
  if ( Size > 2*Num ) {
    cs+= ((uint8_t*)Buf)[i];
  }
  cs=(cs>>16)+(cs&0xFFFF);
  return (uint16_t)(~cs);
}

void udp_reply(uint8_t * ptr_UdpData, uint16_t UDPDataCount)
{
  // Prepare UDP-packet for reply
  uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame1[4];
  UDP_Pseudo_Header tmpUDPPseudoHeader;  
  // Ethernet
  /* Set destanation MAC address */
  *(uint32_t *) &ptr_OutFrame[0] = (((((uint8_t) (RecdFrameMAC[1] >> 8) & 0xFF) << 24)
                             | (((uint8_t) RecdFrameMAC[1] & 0xFF) << 16)
                             | (((uint8_t) (RecdFrameMAC[0] >> 8) & 0xFF) << 8)
                             | ((uint8_t) RecdFrameMAC[0] & 0xFF)));

  *(uint16_t *) &ptr_OutFrame[4] = ((((uint8_t) (RecdFrameMAC[2] >> 8) & 0xFF) << 8)
                     | ((uint8_t) RecdFrameMAC[2] & 0xFF));
  /* Set our MAC address */
  ptr_OutFrame[6] = MAC_5;
  ptr_OutFrame[7] = MAC_4;
  ptr_OutFrame[8] = MAC_3;
  ptr_OutFrame[9] = MAC_2;
  ptr_OutFrame[10] = MAC_1;
  ptr_OutFrame[11] = MAC_0;  
  *(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]      = SWAPB(FRAME_IP);
  // IP
  *(uint16_t *)&ptr_OutFrame[IP_VER_IHL_TOS_OFS]     = SWAPB(IP_VER_IHL);
  WriteWBE(&ptr_OutFrame[IP_TOTAL_LENGTH_OFS], IP_HEADER_SIZE + UDP_HEADER_SIZE + UDPDataCount);
  *(uint16_t *)&ptr_OutFrame[IP_IDENT_OFS]       = 0;//EthernetIP_Frame->Identification;//PROT_ICMP;
  *(uint16_t *)&ptr_OutFrame[IP_FLAGS_FRAG_OFS]     = 0x00;
  *(uint16_t *)&ptr_OutFrame[IP_TTL_PROT_OFS]     = SWAPB((128 << 8) | PROT_UDP);//SWAPB((DEFAULT_TTL << 8) | PROT_UDP);
  *(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS]     = 0;
  memcpy(&ptr_OutFrame[IP_SOURCE_OFS], MyIP, 4);
  memcpy(&ptr_OutFrame[IP_DESTINATION_OFS], RecdFrameIP, 4);
  *(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS]     = 0;//CalcChecksum(&ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE, 0);
  *(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] = CheckSum (0, &ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE);
  // UDP  
  *(uint16_t *)&ptr_OutFrame[UDP_SRC_PORT]  =  0x8EB1;
  *(uint16_t *)&ptr_OutFrame[UDP_DST_PORT]  =  0x8EB1;
  *(uint16_t *)&ptr_OutFrame[UDP_LEN_DGRM]  = SWAPB(UDPDataCount + UDP_HEADER_SIZE);
  *(uint16_t *)&ptr_OutFrame[UDP_CKSUM]    = 0x0;
  memcpy(&ptr_OutFrame[UDP_DATA], ptr_UdpData, UDPDataCount);
  memcpy((void*) &(tmpUDPPseudoHeader.SrcIP), MyIP, 4);
  memcpy((void*) &(tmpUDPPseudoHeader.DstIP), RecdFrameIP, 4);
  tmpUDPPseudoHeader.Zero = 0x00;
  tmpUDPPseudoHeader.Protocol = PROT_UDP;
  tmpUDPPseudoHeader.UDPLength = SWAPB(UDPDataCount + UDP_HEADER_SIZE);
  *(uint16_t *)&ptr_OutFrame[UDP_CKSUM] = 0;  // CS
  /* Set the length of the send frame */
  *(uint32_t*)&TxFrame1[0] = ETH_HEADER_SIZE + IP_HEADER_SIZE + UDPDataCount + UDP_HEADER_SIZE;
    
  TransmitControl |= SEND_FRAME1;
}


//-----------------------------------------------------------------------
//----------------- UDP technilogic mode

void udp_echo2()
{
    uint16_t ii;
    leneth = SWAPB(EthernetUDP_Frame->len) - UDP_HEADER_SIZE;
    for(ii = 0; ii < leneth ; ii++)
        eth_data [ii] = EthernetUDP_Frame->data[ii];        
    ethcount++;
}

void udp_proceed()
{
    udp_echo2();
}

void udp_send(uint8_t * ptr_UdpData, uint16_t UDPDataCount)
{
	// Prepare UDP-packet for send
	uint8_t * ptr_OutFrame = (uint8_t * )&TxFrame1[4];
	UDP_Pseudo_Header tmpUDPPseudoHeader;	
	// Ethernet
	/* Set destination MAC address */
	ptr_OutFrame[0] = MAC2_5;
	ptr_OutFrame[1] = MAC2_4;
	ptr_OutFrame[2] = MAC2_3;
	ptr_OutFrame[3] = MAC2_2;
	ptr_OutFrame[4] = MAC2_1;
	ptr_OutFrame[5] = MAC2_0;
	/* Set our MAC address */
	ptr_OutFrame[6] = MAC_5;
	ptr_OutFrame[7] = MAC_4;
	ptr_OutFrame[8] = MAC_3;
	ptr_OutFrame[9] = MAC_2;
	ptr_OutFrame[10] = MAC_1;
	ptr_OutFrame[11] = MAC_0;
	*(uint16_t *)&ptr_OutFrame[ETH_TYPE_OFS]			= SWAPB(FRAME_IP);
	// IP
	*(uint16_t *)&ptr_OutFrame[IP_VER_IHL_TOS_OFS] 		= SWAPB(IP_VER_IHL);
	WriteWBE(&ptr_OutFrame[IP_TOTAL_LENGTH_OFS], IP_HEADER_SIZE + UDP_HEADER_SIZE + UDPDataCount);
	*(uint16_t *)&ptr_OutFrame[IP_IDENT_OFS] 			= 0;//EthernetIP_Frame->Identification;//PROT_ICMP;
	*(uint16_t *)&ptr_OutFrame[IP_FLAGS_FRAG_OFS] 		= 0x00;
	*(uint16_t *)&ptr_OutFrame[IP_TTL_PROT_OFS] 		= SWAPB((128 << 8) | PROT_UDP);//SWAPB((DEFAULT_TTL << 8) | PROT_UDP);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;
	memcpy(&ptr_OutFrame[IP_SOURCE_OFS], MyIP, 4);
	memcpy(&ptr_OutFrame[IP_DESTINATION_OFS], ToIP, 4);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] 		= 0;//CalcChecksum(&ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE, 0);
	*(uint16_t *)&ptr_OutFrame[IP_HEAD_CHKSUM_OFS] = CheckSum (0, &ptr_OutFrame[IP_VER_IHL_TOS_OFS], IP_HEADER_SIZE);
	// UDP
    *(uint16_t *)&ptr_OutFrame[UDP_SRC_PORT]	=	0x8EB1;
    *(uint16_t *)&ptr_OutFrame[UDP_DST_PORT]	=	0x8EB1;
	*(uint16_t *)&ptr_OutFrame[UDP_LEN_DGRM]	= SWAPB(UDPDataCount + UDP_HEADER_SIZE);
	*(uint16_t *)&ptr_OutFrame[UDP_CKSUM]		= 0x0;			
	memcpy(&ptr_OutFrame[UDP_DATA], ptr_UdpData, UDPDataCount);
	memcpy ( (void*) &(tmpUDPPseudoHeader.SrcIP), MyIP, 4);
	memcpy ( (void*) &(tmpUDPPseudoHeader.DstIP), ToIP, 4);
	tmpUDPPseudoHeader.Zero = 0x00;
	tmpUDPPseudoHeader.Protocol = PROT_UDP;
	tmpUDPPseudoHeader.UDPLength = SWAPB(UDPDataCount + UDP_HEADER_SIZE);
	*(uint16_t *)&ptr_OutFrame[UDP_CKSUM] = 0;	
	/* Set the length of the send frame */
	*(uint32_t*)&TxFrame1[0] = ETH_HEADER_SIZE + IP_HEADER_SIZE + UDPDataCount + UDP_HEADER_SIZE;
		
	TransmitControl |= SEND_FRAME1;
}
///--------------------------------------------------------------------------
