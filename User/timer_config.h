#ifndef __TIMER_CONFIG_H__
	#define __TIMER_CONFIG_H__
	#include "MDR32F9Qx_config.h"           // Milandr::Device:Startup
	#include "MDR32F9Qx_rst_clk.h"          // Milandr::Drivers:RST_CLK
	#include "MDR32F9Qx_timer.h"            // Milandr::Drivers:TIMER
	
	#define INIT_TIME					{0, 0, 0, 0, 0}		// начальнее время которое будет записан в первую ячейку буфера времени наработки
	
	typedef struct {
		uint8_t count : 7;
		uint8_t fl : 1;
	} fl_count;
	
	typedef struct
	{
		uint8_t fl_time;
		uint8_t _40ms;
		uint8_t	Seconds;
		uint8_t	Minutes;
		uint16_t	Hours;
	}	DataTimeStructTypeDef;
	
	void Timer_Config(void);						// настройки Timer
	void TIMER2_IRQHandler(void);				// прерывание
    void TIMER4_IRQHandler(void);
	//uint8_t DeltaTime(DataTimeStructTypeDef last_time);		// для небольшой разницы
	extern DataTimeStructTypeDef	DataTime;
    extern uint8_t flag;
    extern uint8_t tryout;
#endif 	//__TIMER_CONFIG_H__
