#include "CP_DISS_MCU_Employ.h"
#include "timer_config.h"
#include "uart_config.h"
#include "MDR32F9Qx_uart.h"

DataTimeStructTypeDef	DataTime = INIT_TIME;
uint8_t uart_count = 0;

void Timer_Config(void)
{
    // Таймер 1 занят ethernet`ом
    TIMER_CntInitTypeDef TIMER_CntInitStruct;
    RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER2, ENABLE);//Используется для отсчета времени принимаемых сообщений по ARINC, мигание диодами и др.
    RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER3, ENABLE);//Используется для оцифровывания 25 кГц
    RST_CLK_PCLKcmd(RST_CLK_PCLK_TIMER4, ENABLE);//Используется для UART
    
    TIMER_BRGInit(MDR_TIMER2, TIMER_HCLKdiv1);
    TIMER_BRGInit(MDR_TIMER3, TIMER_HCLKdiv2);
    TIMER_BRGInit(MDR_TIMER4, TIMER_HCLKdiv2);

    TIMER_DeInit(MDR_TIMER2);

    TIMER_CntStructInit (&TIMER_CntInitStruct);

    TIMER_CntInitStruct.TIMER_IniCounter = 0;                       // CNT начальное значение счетчика
    TIMER_CntInitStruct.TIMER_Prescaler  = 0x0077;                  // PSG пределитель
    TIMER_CntInitStruct.TIMER_Period     = 0xFFFFFFFF;              // ARR основание счета	// 40 ms

    TIMER_CntInit(MDR_TIMER2, &TIMER_CntInitStruct);
    TIMER_ITConfig(MDR_TIMER2, TIMER_STATUS_CNT_ARR, ENABLE);       // настройка прерывания
    NVIC_EnableIRQ(TIMER2_IRQn);                                    // разрешение прерываний
    TIMER_Cmd(MDR_TIMER2, ENABLE);                                  // включение таймера
    
    TIMER_DeInit(MDR_TIMER4);
    TIMER_CntStructInit (&TIMER_CntInitStruct);
    TIMER_CntInitStruct.TIMER_IniCounter = 0;                       // CNT начальное значение счетчика
    TIMER_CntInitStruct.TIMER_Prescaler  = 0x0077;                  // 1 мкс
    TIMER_CntInitStruct.TIMER_Period     = 0x0000000A;              // 10 мкс
    TIMER_CntInit(MDR_TIMER4, &TIMER_CntInitStruct);
    TIMER_ITConfig(MDR_TIMER4, TIMER_STATUS_CNT_ARR, ENABLE);       // настройка прерывания
    NVIC_EnableIRQ(TIMER4_IRQn);                                    // разрешение прерываний
    //TIMER_Cmd(MDR_TIMER4, ENABLE);                                  // включение таймера
}
//Прерывание второго таймера
void TIMER2_IRQHandler(void)
{
    TIMER_ClearFlag(MDR_TIMER2, TIMER_STATUS_CNT_ARR);
}
//Прерывание четвертого таймера
void TIMER4_IRQHandler(void)
{
    if(UART_GetFlagStatus(MDR_UART2, UART_FLAG_TXFE ) == SET) {
        UART_SendData(MDR_UART2, uart_mass[uart_count]);
        uart_count++;
    }
    if(uart_ctrl & 0x02) {
        if(uart_count >= 28) {
            uart_count = 0;
            uart_send_is_running = 0;
            TIMER_Cmd(MDR_TIMER4, DISABLE);
        }
    }
    else {
        if(uart_count >= 18) {
            uart_count = 0;
            uart_send_is_running = 0;
            TIMER_Cmd(MDR_TIMER4, DISABLE);
        }
    }
    TIMER_ClearFlag(MDR_TIMER4, TIMER_STATUS_CNT_ARR);
}
