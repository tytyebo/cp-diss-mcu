#ifndef __ARINC_CONFIG_H__
	#define __ARINC_CONFIG_H__
	#include "MDR32F9Qx_config.h"           // Milandr::Device:Startup
	#include "MDR32F9Qx_rst_clk.h"          // Milandr::Drivers:RST_CLK
	#include "MDR32F9Qx_port.h"             // Milandr::Drivers:PORT
	#include "MDR32F9Qx_arinc429r.h"        // Milandr::Drivers:ARINC429R
	#include "MDR32F9Qx_arinc429t.h"        // Milandr::Drivers:ARINC429T
    #include "adc_config.h"

    #define ARINC_TX_TIME(time)		(time.Minutes << 24 | time.Seconds << 16 | time._40ms << 8)
    // RX
    #define ARINC_ADDRESS_W_X                   (0xB0)
    #define ARINC_ADDRESS_W_Y                   (0xB1)
    #define ARINC_ADDRESS_W_Z                   (0xB2)
    #define ARINC_ADDRESS_MODE                  (0xBA)
    #define ARINC_ADDRESS_STATUS                (0xEE)
    #define ARINC_ADDRESS_TIME                  (0x06)
    #define ARINC_ADDRESS_VERS_MC               (0x07)
    #define ARINC_ADDRESS_CRC_MC_FIRST_PART     (0x08)
    #define ARINC_ADDRESS_CRC_MC_SECOND_PART    (0x09)
    #define ARINC_ADDRESS_VERS_SHF              (0x0A)
    #define ARINC_ADDRESS_CRC_SHF_FIRST_PART    (0x0B)
    #define ARINC_ADDRESS_CRC_SHF_SECOND_PART   (0x0C)
    #define ARINC_ADDRESS_VERS_MC3              (0x0D)
    #define ARINC_ADDRESS_CRC_MC3_FIRST_PART    (0x0E)
    #define ARINC_ADDRESS_CRC_MC3_SECOND_PART   (0x0F)
    
    #define ARINC_RX_ARRAY_SIZE                 (64)

    #define ARINC_WX                            (0x00)
    #define ARINC_WY                            (0x01)
    #define ARINC_WZ                            (0x02)
    #define ARINC_MODE                          (0x03)
    #define ARINC_STATUS                        (0x04)
    #define ARINC_TIME                          (0x05)
    #define ARINC_VERS_MC                       (0x06)
    #define ARINC_CRC_MC_FIRST_PART             (0x07)
    #define ARINC_CRC_MC_SECOND_PART            (0x08)
    #define ARINC_VERS_SHF                      (0x09)
    #define ARINC_CRC_SHF_FIRST_PART            (0x0A)
    #define ARINC_CRC_SHF_SECOND_PART           (0x0B)
    #define ARINC_VERS_SHF                      (0x09)
    #define ARINC_CRC_SHF_FIRST_PART            (0x0A)
    #define ARINC_CRC_SHF_SECOND_PART           (0x0B)
    #define ARINC_VERS_MC3                      (0x0C)
    #define ARINC_CRC_MC3_FIRST_PART            (0x0D)
    #define ARINC_CRC_MC3_SECOND_PART           (0x0E)

    extern uint8_t arinc_rx_size1;
    extern uint8_t arinc_rx_size2;
    extern uint8_t led_ari_flag;
    extern uint8_t led_aro_flag;
    extern uint32_t arinc_rx_array1[ARINC_RX_ARRAY_SIZE];
    extern uint32_t arinc_rx_array2[ARINC_RX_ARRAY_SIZE];
    extern uint32_t arinc_time1[15][2];
    extern uint32_t arinc_data1[15];
    extern uint32_t arinc_time2[15][2];
    extern uint32_t arinc_data2[15];
    extern uint16_t arinc_data_count1[15];
    extern uint16_t arinc_data_count2[15];
    extern uint32_t arinc_error_count1;
    extern uint32_t arinc_error_count2;

    void Arinc_Config(void);									// настройка arinc
    void ArincSpeed(uint8_t speed);	// 0bit - OUT1, 1bit - OUT2, 3bit - IN2, 4bit - IN2
    void ARINC429R_IRQHandler(void);					// прерывание arinc
    void ArincGetData(void);
    void ArincSendDataCH1(uint32_t tx_data);			// отправка посылки с подсчетом бита паритета
    void ArincSendDataCH2(uint32_t tx_data);
#endif 	//__ARINC_CONFIG_H__
