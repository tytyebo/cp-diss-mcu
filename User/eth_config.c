#include "eth_config.h"

static ETH_InitTypeDef  ETH_InitStruct;
uint8_t eth_rx_flag = 0;

void Eth_Config()
{
	/* Reset ehernet clock settings */
	ETH_ClockDeInit();

	RST_CLK_PCLKcmd(RST_CLK_PCLK_DMA, ENABLE);

	/* Enable HSE2 oscillator */
	RST_CLK_HSE2config(RST_CLK_HSE2_Bypass);
	if(RST_CLK_HSE2status() == ERROR)
		while(1); /* Infinite loop */

	/* Config PHY clock */
	ETH_PHY_ClockConfig(ETH_PHY_CLOCK_SOURCE_HSE2, ETH_PHY_HCLKdiv1);
	/* Init the BRG ETHERNET */
	ETH_BRGInit(ETH_HCLKdiv1);

	/* Enable the ETHERNET clock */
	ETH_ClockCMD(ETH_CLK1, ENABLE);

	/* Reset to default ethernet settings */
	ETH_DeInit(MDR_ETHERNET1);

	/* Init ETH_InitStruct members with its default value */
	ETH_StructInit((ETH_InitTypeDef * ) &ETH_InitStruct);
	/* Set the speed of the chennel */
	ETH_InitStruct.ETH_PHY_Mode = ETH_PHY_MODE_AutoNegotiation;
	ETH_InitStruct.ETH_Transmitter_RST = SET;
	ETH_InitStruct.ETH_Receiver_RST = SET;
	/* Set the buffer mode */
//	ETH_InitStruct.ETH_Buffer_Mode = ETH_BUFFER_MODE_AUTOMATIC_CHANGE_POINTERS;
	ETH_InitStruct.ETH_Buffer_Mode = ETH_BUFFER_MODE_LINEAR;

	ETH_InitStruct.ETH_Source_Addr_HASH_Filter = DISABLE;

	/* Set the MAC address. */
	ETH_InitStruct.ETH_MAC_Address[2] = (MAC_0<<8)|MAC_1;
	ETH_InitStruct.ETH_MAC_Address[1] = (MAC_2<<8)|MAC_3;
	ETH_InitStruct.ETH_MAC_Address[0] = (MAC_4<<8)|MAC_5;

	/* Set the buffer size of transmitter and receiver */
	ETH_InitStruct.ETH_Dilimiter = 0x1000;

	/* Init the ETHERNET 1 */
	ETH_Init(MDR_ETHERNET1, (ETH_InitTypeDef *) &ETH_InitStruct);

	/* Enable PHY module */
	ETH_PHYCmd(MDR_ETHERNET1, ENABLE);

	//TCPLowLevelInit();
	ETH_MACITConfig(MDR_ETHERNET1, ETH_MAC_IT_RF_OK, ENABLE);					// Индикатор успешно принятого пакета
	/* Start the ETHERNET1 */
	ETH_Start(MDR_ETHERNET1);
	
	NVIC_EnableIRQ (ETHERNET_IRQn);		// разрешение прерываний 
}

void ETH_InputPachetHandler(MDR_ETHERNET_TypeDef * ETHERNETx)
{
	uint16_t status_reg;
	ETH_StatusPacketReceptionTypeDef ETH_StatusPacketReceptionStruct;

	/* Check that the packet is received */
	status_reg = ETH_GetMACITStatusRegister(ETHERNETx);

	if(ETHERNETx->ETH_R_Head != ETHERNETx->ETH_R_Tail){
		if(status_reg & ETH_MAC_IT_RF_OK ){
			ETH_StatusPacketReceptionStruct.Status = ETH_ReceivedFrame(ETHERNETx, InputFrame);

			if(ETH_StatusPacketReceptionStruct.Fields.UCA)
			{
				ProcessEthIAFrame(InputFrame, ETH_StatusPacketReceptionStruct.Fields.Length);	// udp тут
				eth_rx_flag++;
			}
			if(ETH_StatusPacketReceptionStruct.Fields.BCA)
				 ProcessEthBroadcastFrame(InputFrame, ETH_StatusPacketReceptionStruct.Fields.Length);
		}
	}
}

void ETHERNET_IRQHandler()
{
    led_eth_flag = 1;
	ETH_InputPachetHandler(MDR_ETHERNET1);
	DoNetworkStuff(MDR_ETHERNET1);
}
