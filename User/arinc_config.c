#include "arinc_config.h"

uint8_t arinc_rx_size1, arinc_rx_size2, led_ari_flag = 0, led_aro_flag = 0;

uint32_t arinc_time_massive1[ARINC_RX_ARRAY_SIZE];
uint32_t arinc_time_massive2[ARINC_RX_ARRAY_SIZE];
uint32_t arinc_rx_array1[ARINC_RX_ARRAY_SIZE];
uint32_t arinc_rx_array2[ARINC_RX_ARRAY_SIZE];

uint32_t last_time_ar1 = 0, last_time_ar2 = 0;
uint32_t arinc_time1[15][2];
uint32_t arinc_data1[15];
uint32_t arinc_time2[15][2];
uint32_t arinc_data2[15];
uint16_t arinc_data_count1[15] = {0, 0, 0, 0, 0, 0, 0};
uint16_t arinc_data_count2[15] = {0, 0, 0, 0, 0, 0, 0};
uint32_t arinc_error_count1 = 0, arinc_error_count2 = 0;

void ArincSpeed(uint8_t speed) {
    ARINC429R_InitChannelTypeDef ARINC429R_InitChannelStruct;
    ARINC429T_InitChannelTypeDef ARINC429T_InitChannelStruct;
    RST_CLK_PCLKcmd(RST_CLK_PCLK_ARINC429R | RST_CLK_PCLK_ARINC429T, ENABLE);

    ARINC429R_DeInit();
    ARINC429R_BRG_Init(120);
    ARINC429R_InitChannelStruct.ARINC429R_SD = DISABLE;
    ARINC429R_InitChannelStruct.ARINC429R_LB = DISABLE;
    ARINC429R_InitChannelStruct.ARINC429R_SDI1 = RESET;
    ARINC429R_InitChannelStruct.ARINC429R_SDI2 = RESET;
    ARINC429R_InitChannelStruct.ARINC429R_DIV = 0;
    if((speed >> 2) & 1)                                                        // IN1. "1" это 100
        ARINC429R_InitChannelStruct.ARINC429R_CLK = ARINC429R_CLK_100_KHz;
    else                                                                        // IN1. "0" это 12.5
        ARINC429R_InitChannelStruct.ARINC429R_CLK = ARINC429R_CLK_12_5_KHz;
    ARINC429R_ChannelInit(ARINC429R_CHANNEL1, &ARINC429R_InitChannelStruct);    // Инициализация первого входного канала
    if((speed >> 3) & 1)                                                        // IN2. "1" это 100
        ARINC429R_InitChannelStruct.ARINC429R_CLK = ARINC429R_CLK_100_KHz;
    else                                                                        // IN2. "0" это 12.5
        ARINC429R_InitChannelStruct.ARINC429R_CLK = ARINC429R_CLK_12_5_KHz;
    ARINC429R_ChannelInit(ARINC429R_CHANNEL2, &ARINC429R_InitChannelStruct);    // Инициализация второго входного канала
    ARINC429R_ITConfig((ARINC429R_IT_INT_DR | ARINC429R_IT_INT_ER), ENABLE);
    NVIC_EnableIRQ(ARINC429R_IRQn);
    ARINC429R_ChannelCmd(ARINC429R_CHANNEL1, ENABLE);
    ARINC429R_ChannelCmd(ARINC429R_CHANNEL2, ENABLE);

    ARINC429T_DeInit();
    ARINC429T_BRG_Init(120);
    ARINC429T_InitChannelStruct.ARINC429T_DIV = 0;
    ARINC429T_InitChannelStruct.ARINC429T_EN_PAR = ENABLE;
    ARINC429T_InitChannelStruct.ARINC429T_ODD = ARINC429T_ODD_ADD_TO_ODD;
    if(speed & 1) {                                                             // OUT1. "1" это 12.5
        ARINC429T_InitChannelStruct.ARINC429T_CLK = ARINC429T_CLK_12_5_KHz;
        PORT_ResetBits(MDR_PORTD, PORT_Pin_12);                                 // SLP1 = 0
    }
    else {                                                                      // OUT1. "0" это 100
        ARINC429T_InitChannelStruct.ARINC429T_CLK = ARINC429T_CLK_100_KHz;
        PORT_SetBits(MDR_PORTD, PORT_Pin_12);                                   // SLP1 = 1
    }
    ARINC429T_ChannelInit(ARINC429T_CHANNEL1, &ARINC429T_InitChannelStruct);
    if((speed >> 1) & 1) {                                                      // OUT2. "1" это 100
        ARINC429T_InitChannelStruct.ARINC429T_CLK = ARINC429T_CLK_100_KHz;
        PORT_SetBits(MDR_PORTD, PORT_Pin_2);                                    // SLP2 = 1
    }
    else {                                                                      // OUT2. "0" это 12.5
        ARINC429T_InitChannelStruct.ARINC429T_CLK = ARINC429T_CLK_12_5_KHz;
        PORT_ResetBits(MDR_PORTD, PORT_Pin_2);                                  // SLP2 = 0
    }
    ARINC429T_ChannelInit(ARINC429T_CHANNEL3, &ARINC429T_InitChannelStruct);
    ARINC429T_ChannelCmd(ARINC429T_CHANNEL1, ENABLE);
    ARINC429T_ChannelCmd(ARINC429T_CHANNEL3, ENABLE);
    //======= Recive CS turn on	// лучше в порт перевести, как узнаю что это
    PORT_ResetBits(MDR_PORTD, PORT_Pin_0);
    PORT_ResetBits(MDR_PORTD, PORT_Pin_1);
}

void ARINC429R_IRQHandler(void) {
    uint32_t timer = MDR_TIMER2->CNT;
    // Первый канал
    if(ARINC429R_GetFlagStatus(ARINC429R_CHANNEL1, ARINC429R_FLAG_ERR) == SET){         // Ошибка приема данных
        ARINC429R_ITConfig(ARINC429R_IT_INT_ER, DISABLE);
        ARINC429R_ChannelCmd(ARINC429R_CHANNEL1, DISABLE);
        ARINC429R_ITConfig((ARINC429R_IT_INT_DR | ARINC429R_IT_INT_ER), ENABLE);
        ARINC429R_ChannelCmd(ARINC429R_CHANNEL1, ENABLE);
        arinc_error_count1++;
    }
    else{
        led_ari_flag = 1;
        ARINC429R_SetChannel(ARINC429R_CHANNEL1);
        while(ARINC429R_GetFlagStatus(ARINC429R_CHANNEL1, ARINC429R_FLAG_DR) == SET)
        {
            if((long int)timer - (long int)last_time_ar1 > 200 || (long int)last_time_ar1 - (long int)timer > 200) {
                arinc_rx_array1[arinc_rx_size1] = ARINC429R_ReceiveData();
                arinc_time_massive1[arinc_rx_size1] = last_time_ar1 = timer;
                if(arinc_rx_size1 < ARINC_RX_ARRAY_SIZE-1)
                    arinc_rx_size1++;
            }
        }
    }
    // Второй канал
    if(ARINC429R_GetFlagStatus(ARINC429R_CHANNEL2, ARINC429R_FLAG_ERR) == SET){         // Ошибка приема данных
        ARINC429R_ITConfig(ARINC429R_IT_INT_ER, DISABLE);
        ARINC429R_ChannelCmd(ARINC429R_CHANNEL2, DISABLE);
        ARINC429R_ITConfig((ARINC429R_IT_INT_DR | ARINC429R_IT_INT_ER), ENABLE);
        ARINC429R_ChannelCmd(ARINC429R_CHANNEL2, ENABLE);
        arinc_error_count2++;
    }
    else{
        led_ari_flag = 1;
        ARINC429R_SetChannel(ARINC429R_CHANNEL2);
        while(ARINC429R_GetFlagStatus(ARINC429R_CHANNEL2, ARINC429R_FLAG_DR) == SET)
        {
            if((long int)timer - (long int)last_time_ar2 > 200 || (long int)last_time_ar2 - (long int)timer > 200) {
                arinc_rx_array2[arinc_rx_size2] = ARINC429R_ReceiveData();
                arinc_time_massive2[arinc_rx_size2] = last_time_ar2 = timer;
                if(arinc_rx_size2 < ARINC_RX_ARRAY_SIZE-1)
                    arinc_rx_size2++;
            }
        }
    }
    ArincGetData();
}
void ArincSendDataCH1(uint32_t tx_data) {
    led_aro_flag = 1;
    ARINC429T_SendData(ARINC429T_CHANNEL1, tx_data);
    while(ARINC429T_GetFlagStatus(ARINC429T_CHANNEL1, ARINC429T_FLAG_TX_R) != 1);
}
void ArincSendDataCH2(uint32_t tx_data) {
    led_aro_flag = 1;
    ARINC429T_SendData(ARINC429T_CHANNEL3, tx_data);
    while(ARINC429T_GetFlagStatus(ARINC429T_CHANNEL3, ARINC429T_FLAG_TX_R) != 1);
}
void ArincGetData(void) {
    uint8_t i;
    for(i = 0; i < arinc_rx_size1; i++) {
        switch(arinc_rx_array1[i] & 0xFF) {
            case ARINC_ADDRESS_W_X:
                arinc_data1[0] = arinc_rx_array1[i];
                arinc_time1[0][0] = arinc_time1[0][1];
                arinc_time1[0][1] = arinc_time_massive1[i];
                arinc_data_count1[0]++;
                break;
            case ARINC_ADDRESS_W_Y:
                arinc_data1[1] = arinc_rx_array1[i];
                arinc_time1[1][0] = arinc_time1[1][1];
                arinc_time1[1][1] = arinc_time_massive1[i];
                arinc_data_count1[1]++;
                break;
            case ARINC_ADDRESS_W_Z:
                arinc_data1[2] = arinc_rx_array1[i];
                arinc_time1[2][0] = arinc_time1[2][1];
                arinc_time1[2][1] = arinc_time_massive1[i];
                arinc_data_count1[2]++;
                break;
            case ARINC_ADDRESS_MODE:
                arinc_data1[3] = arinc_rx_array1[i];
                arinc_time1[3][0] = arinc_time1[3][1];
                arinc_time1[3][1] = arinc_time_massive1[i];
                arinc_data_count1[3]++;
                break;
            case ARINC_ADDRESS_STATUS:
                arinc_data1[4] = arinc_rx_array1[i];
                arinc_time1[4][0] = arinc_time1[4][1];
                arinc_time1[4][1] = arinc_time_massive1[i];
                arinc_data_count1[4]++;
                break;
            case ARINC_ADDRESS_TIME:
                arinc_data1[5] = arinc_rx_array1[i];
                arinc_time1[5][1] = arinc_time_massive1[i];
                arinc_data_count1[5]++;
                break;
            case ARINC_ADDRESS_VERS_MC:
                arinc_data1[6] = arinc_rx_array1[i];
                arinc_time1[6][1] = arinc_time_massive1[i];
                arinc_data_count1[6]++;
                break;
            case ARINC_ADDRESS_CRC_MC_FIRST_PART:
                arinc_data1[7] = arinc_rx_array1[i];
                arinc_time1[7][1] = arinc_time_massive1[i];
                arinc_data_count1[7]++;
                break;
            case ARINC_ADDRESS_CRC_MC_SECOND_PART:
                arinc_data1[8] = arinc_rx_array1[i];
                arinc_time1[8][1] = arinc_time_massive1[i];
                arinc_data_count1[8]++;
                break;
            case ARINC_ADDRESS_VERS_SHF:
                arinc_data1[9] = arinc_rx_array1[i];
                arinc_time1[9][1] = arinc_time_massive1[i];
                arinc_data_count1[9]++;
                break;
            case ARINC_ADDRESS_CRC_SHF_FIRST_PART:
                arinc_data1[10] = arinc_rx_array1[i];
                arinc_time1[10][1] = arinc_time_massive1[i];
                arinc_data_count1[10]++;
                break;
            case ARINC_ADDRESS_CRC_SHF_SECOND_PART:
                arinc_data1[11] = arinc_rx_array1[i];
                arinc_time1[11][1] = arinc_time_massive1[i];
                arinc_data_count1[11]++;
                break;
            case ARINC_ADDRESS_VERS_MC3:
                arinc_data1[12] = arinc_rx_array1[i];
                arinc_time1[12][1] = arinc_time_massive1[i];
                arinc_data_count1[12]++;
                break;
            case ARINC_ADDRESS_CRC_MC3_FIRST_PART:
                arinc_data1[13] = arinc_rx_array1[i];
                arinc_time1[13][1] = arinc_time_massive1[i];
                arinc_data_count1[13]++;
                break;
            case ARINC_ADDRESS_CRC_MC3_SECOND_PART:
                arinc_data1[14] = arinc_rx_array1[i];
                arinc_time1[14][1] = arinc_time_massive1[i];
                arinc_data_count1[14]++;
                break;
        }
    }
    for(i = 0; i < arinc_rx_size2; i++) {
        switch(arinc_rx_array2[i] & 0xFF) {
            case ARINC_ADDRESS_W_X:
                arinc_data2[0] = arinc_rx_array2[i];
                arinc_time2[0][0] = arinc_time2[0][1];
                arinc_time2[0][1] = arinc_time_massive2[i];
                arinc_data_count2[0]++;
                break;
            case ARINC_ADDRESS_W_Y:
                arinc_data2[1] = arinc_rx_array2[i];
                arinc_time2[1][0] = arinc_time2[1][1];
                arinc_time2[1][1] = arinc_time_massive2[i];
                arinc_data_count2[1]++;
                break;
            case ARINC_ADDRESS_W_Z:
                arinc_data2[2] = arinc_rx_array2[i];
                arinc_time2[2][0] = arinc_time2[2][1];
                arinc_time2[2][1] = arinc_time_massive2[i];
                arinc_data_count2[2]++;
                break;
            case ARINC_ADDRESS_MODE:
                arinc_data2[3] = arinc_rx_array2[i];
                arinc_time2[3][0] = arinc_time2[3][1];
                arinc_time2[3][1] = arinc_time_massive2[i];
                arinc_data_count2[3]++;
                break;
            case ARINC_ADDRESS_STATUS:
                arinc_data2[4] = arinc_rx_array2[i];
                arinc_time2[4][0] = arinc_time2[4][1];
                arinc_time2[4][1] = arinc_time_massive2[i];
                arinc_data_count2[4]++;
                break;
            case ARINC_ADDRESS_TIME:
                arinc_data2[5] = arinc_rx_array2[i];
                arinc_time2[5][1] = arinc_time_massive2[i];
                arinc_data_count2[5]++;
                break;
            case ARINC_ADDRESS_VERS_MC:
                arinc_data2[6] = arinc_rx_array2[i];
                arinc_time2[6][1] = arinc_time_massive2[i];
                arinc_data_count2[6]++;
                break;
            case ARINC_ADDRESS_CRC_MC_FIRST_PART:
                arinc_data2[7] = arinc_rx_array2[i];
                arinc_time2[7][1] = arinc_time_massive2[i];
                arinc_data_count2[7]++;
                break;
            case ARINC_ADDRESS_CRC_MC_SECOND_PART:
                arinc_data2[8] = arinc_rx_array2[i];
                arinc_time2[8][1] = arinc_time_massive2[i];
                arinc_data_count2[8]++;
                break;
            case ARINC_ADDRESS_VERS_SHF:
                arinc_data2[9] = arinc_rx_array2[i];
                arinc_time2[9][1] = arinc_time_massive2[i];
                arinc_data_count2[9]++;
                break;
            case ARINC_ADDRESS_CRC_SHF_FIRST_PART:
                arinc_data2[10] = arinc_rx_array2[i];
                arinc_time2[10][1] = arinc_time_massive2[i];
                arinc_data_count2[10]++;
                break;
            case ARINC_ADDRESS_CRC_SHF_SECOND_PART:
                arinc_data2[11] = arinc_rx_array2[i];
                arinc_time2[11][1] = arinc_time_massive2[i];
                arinc_data_count2[11]++;
                break;
            case ARINC_ADDRESS_VERS_MC3:
                arinc_data2[12] = arinc_rx_array2[i];
                arinc_time2[12][1] = arinc_time_massive2[i];
                arinc_data_count2[12]++;
                break;
            case ARINC_ADDRESS_CRC_MC3_FIRST_PART:
                arinc_data2[13] = arinc_rx_array2[i];
                arinc_time2[13][1] = arinc_time_massive2[i];
                arinc_data_count2[13]++;
                break;
            case ARINC_ADDRESS_CRC_MC3_SECOND_PART:
                arinc_data2[14] = arinc_rx_array2[i];
                arinc_time2[14][1] = arinc_time_massive2[i];
                arinc_data_count2[14]++;
                break;
            }
    }
    arinc_rx_size1 = arinc_rx_size2 = 0;
}
