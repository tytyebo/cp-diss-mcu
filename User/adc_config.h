#ifndef __ADC_CONFIG_H__
	#define __ADC_CONFIG_H__
	#include "MDR32F9Qx_config.h"
	#include "MDR32F9Qx_rst_clk.h"
	#include "MDR32F9Qx_adc.h"
    #include "timer_config.h"
    
    void ADC_Config(uint32_t chanel);
    void ADC_Start(uint32_t chanel);
    extern uint16_t adc_mass[1024];
    void ADC_IRQHandler(void);
    extern uint8_t irq_adc_count;
#endif 	//__ADC_CONFIG_H__
