#ifndef __ETH_CONFIG_H__
	#define __ETH_CONFIG_H__
	#include "MDR32F9Qx_config.h"           // Milandr::Device:Startup
	#include "MDR32F9Qx_rst_clk.h"          // Milandr::Drivers:RST_CLK
	#include "MDR32F9Qx_dma.h"              // Milandr::Drivers:DMA
	#include "MDR32F9Qx_eth.h"              // Milandr::Drivers:Ethernet
	#include "MDR32F9Qx_timer.h"            // Milandr::Drivers:TIMER
	#include "tcpip.h"
	#include "stdio.h"
	void Eth_Config(void);
	void ETH_InputPachetHandler(MDR_ETHERNET_TypeDef * ETHERNETx);
	void ETHERNET_IRQHandler(void);		// прерывания
	extern uint8_t eth_rx_flag;
#endif 	//__ETH_CONFIG_H__
