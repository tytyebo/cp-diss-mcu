#ifndef __UART_CONFIG_H
#define __UART_CONFIG_H

#include "MDR32F9Qx_config.h"
#include "MDR32F9Qx_timer.h"

void UART_Config (void);
void Uart_Send (uint8_t control, uint8_t test, uint8_t beams);
extern uint8_t uart_mass[28];
extern uint8_t uart_ctrl;
extern uint8_t uart_send_is_running;
#endif
