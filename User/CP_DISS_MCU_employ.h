#ifndef __CP_DISS_MCU_EMPLOY_H
#define __CP_DISS_MCU_EMPLOY_H

#include "MDR32F9Qx_config.h"
typedef enum
{
  CMD_SUCCESS     = 0x0,
  CMD_ERROR       = 0x1,

}Cmd_Result;

void Set_UPR_5V_15V(void);
void Reset_UPR_5V_15V(void);
void Set_UPR_27V_1(void);
void Reset_UPR_27V_1(void);
void Set_UPR_27V_2(void);
void Reset_UPR_27V_2(void);
void Set_WD_STR(void);
void Reset_WD_STR(void);
uint8_t Get_PG_1_MCU(void);
uint8_t Get_PG_2_MCU(void);
uint8_t Get_PG_3_MCU(void);
uint8_t Get_PG_4_MCU(void);
uint8_t Get_WD_PG(void);
uint8_t Get_IND_27V_15V(void);
uint8_t Get_PG(void);
uint8_t Set_Pwr(uint8_t data);
uint8_t Get_PWR(uint8_t param);
uint8_t Change_WS_STR(uint8_t param, uint8_t pre);
uint8_t Get_IND_4RK(void);
void Set_4RK(uint8_t data);
uint8_t Get_UPR_4RK(void);
uint16_t Get_IND_9RK(void);
uint16_t Get_IND_9RK(void);
void Set_9RK(uint16_t data);
uint16_t Get_UPR_9RK(void);
void Set_UPR_RK_A_9(void);
void Set_9RK2(uint8_t data1);
void Set_UPR_REF_12(uint8_t data);
uint8_t Get_UPR_REF_12(void);
void Set_UPR_RK_A_1(void);
void Reset_UPR_RK_A_1(void);
uint8_t Monitor_BEAM(uint8_t bstate);
uint8_t Get_EN_HFS(void);
uint8_t Get_EN_TEST(void);
uint8_t Get_EN_TTL(void);
uint8_t Get_3LVTTL(void);
void Set_3LVTTL(uint8_t data1);
uint8_t AUTO_MANUAL_POS(void);
void Set_LED(uint8_t PC_RK);
void Set_UP_SW(uint8_t enable);
void Set_Led_Eth(void);
void Reset_Led_Eth(void);
void Set_Led_Ari(void);
void Reset_Led_Ari(void);
void Set_Led_Aro(void);
void Reset_Led_Aro(void);
void Set_Led_USB(void);
void Reset_Led_USB(void);
void Set_Mult(uint8_t data);
uint8_t Get_Mult(void);
void auto_ctrl(uint8_t control, uint8_t hfs);

extern uint32_t BEAM_time[12];
extern uint8_t BEAM_state[12];
extern uint8_t bi;
extern uint32_t Sinh_Beam_DISS_time[24];
extern uint32_t Sinh_Beam_MPR_time[24];
extern uint8_t si1;
extern uint8_t si2;
#endif
